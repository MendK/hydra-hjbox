//
//  SampleExecutor.h
//  Sample03_UsingAsyncTask
//
//  Created by Tae Hyun, Na on 2015. 2. 20..
//  Copyright (c) 2015년 TeamP9. All rights reserved.
//
//  Licensed under the MIT license.

#import <UIKit/UIKit.h>
#import <Hydra/Hydra.h>

#define		SampleExecutorName		@"sampleExecutorName"

#define		SampleExecutorParameterKeyCloseQueryCall	@"sampleExecutorParameterKeyCloseQueryCall"
#define		SampleExecutorParameterKeyUrlString			@"sampleExecutorParameterKeyUrlString"
#define		SampleExecutorParameterKeyImage				@"sampleExecutorParameterKeyImage"

@interface SampleExecutor : HYExecuter

@end
