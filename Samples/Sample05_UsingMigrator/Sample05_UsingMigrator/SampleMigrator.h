//
//  SampleMigrator.h
//  Sample05_UsingMigrator
//
//  Created by Tae Hyun, Na on 2015. 3. 11..
//  Copyright (c) 2015년 TeamP9. All rights reserved.
//
//  Licensed under the MIT license.

#import <Foundation/Foundation.h>
#import <Hydra/Hydra.h>

@interface SampleMigrator : HYMigrator

@end
