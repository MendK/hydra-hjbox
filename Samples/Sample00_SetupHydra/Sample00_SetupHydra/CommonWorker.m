//
//  CommonWorker.m
//  Sample00_SetupHydra
//
//  Created by Tae Hyun, Na on 2015. 2. 17..
//  Copyright (c) 2015년 TeamP9. All rights reserved.
//
//  Licensed under the MIT license.

#import "CommonWorker.h"

@implementation CommonWorker

- (NSString *)name
{
	return CommonWorkerName;
}

- (NSString *)brief
{
	return @"common worker, I'll do anything for you.";
}

@end
