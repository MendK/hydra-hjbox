//
//  PlaygroundView.h
//  Sample02_UsingManager
//
//  Created by Tae Hyun, Na on 2015. 2. 17..
//  Copyright (c) 2015년 TeamP9. All rights reserved.
//
//  Licensed under the MIT license.

#import <UIKit/UIKit.h>

@interface PlaygroundView : UIView

@property (nonatomic, readonly) UIImageView *imageView;
@property (nonatomic, readonly) UIButton *doButton;

@end
