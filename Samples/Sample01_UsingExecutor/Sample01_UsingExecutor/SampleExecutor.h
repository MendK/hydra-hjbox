//
//  SampleExecutor.h
//  Sample01_UsingExecutor
//
//  Created by Tae Hyun, Na on 2015. 2. 17..
//  Copyright (c) 2015년 TeamP9. All rights reserved.
//
//  Licensed under the MIT license.

#import <Foundation/Foundation.h>
#import <Hydra/Hydra.h>

#define		SampleExecutorName		@"sampleExecutorName"

#define		SampleExecutorParameterKeyInputNumber		@"sampleExecutorParameterKeyInputNumber"
#define		SampleExecutorParameterKeyOutputNumber		@"sampleExecutorParameterKeyOutputNumber"
#define		SampleExecutorParameterKeyErrorMessage		@"sampleExecutorParameterKeyErrorMessage"

@interface SampleExecutor : HYExecuter

@end
