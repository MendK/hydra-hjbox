//
//  PlaygroundView.h
//  Sample04_UsingTrackingResultSet
//
//  Created by Tae Hyun, Na on 2015. 3. 10..
//  Copyright (c) 2015년 TeamP9. All rights reserved.
//
//  Licensed under the MIT license.

#import <UIKit/UIKit.h>

@interface PlaygroundView : UIView

@property (nonatomic, readonly) UITextView *logView;
@property (nonatomic, readonly) UIButton *booButton;
@property (nonatomic, readonly) UIButton *fooButton;

@end
