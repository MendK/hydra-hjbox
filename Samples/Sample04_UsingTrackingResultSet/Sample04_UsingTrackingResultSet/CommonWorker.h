//
//  CommonWorker.h
//  Sample04_UsingTrackingResultSet
//
//  Created by Tae Hyun, Na on 2015. 3. 10..
//  Copyright (c) 2015년 TeamP9. All rights reserved.
//
//  Licensed under the MIT license.

#import <Foundation/Foundation.h>
#import <Hydra/Hydra.h>

#define		CommonWorkerName	@"CommonWorker"

@interface CommonWorker : HYWorker

@end
