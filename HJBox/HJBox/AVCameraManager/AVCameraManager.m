//
//  AVCameraManager.m
//  HJBox
//
//  Created by Na Tae Hyun on 13. 11. 4..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import "AVCameraManager.h"


AVCameraManager	*g_defaultAVCameraManager;


@interface AVCameraManager( AVCameraManagerPrivate )

- (BOOL) toggleCameraToPosition: (AVCaptureDevicePosition)position;
- (void) postNotifyWithStatus: (AVCameraManagerStatus)status;
- (void) postNotifyStillImageCapturedWithImage: (UIImage *)image devicePosition: (AVCaptureDevicePosition)position;
- (void) postNotifyWithParamDict: (NSDictionary *)paramDict;
- (AVCaptureDevice *) captureDeviceForPosition: (AVCaptureDevicePosition)position;

@end


@implementation AVCameraManager

@synthesize status = _status;

- (id) init
{
	if( (self = [super init]) != nil ) {
		
		_status = AVCameraManagerStatusIdle;
		
		if( (_lock = [[NSLock alloc] init]) == nil ) {
			return nil;
		}
		
	}
	
	return self;
}

+ (AVCameraManager *) defaultManager
{
	@synchronized( self ) {
		if( g_defaultAVCameraManager == nil ) {
			g_defaultAVCameraManager = [[AVCameraManager alloc] init];
		}
	}
	
	return g_defaultAVCameraManager;
}

- (NSInteger) countOfCamera
{
	return [[AVCaptureDevice devicesWithMediaType: AVMediaTypeVideo] count];
}

- (BOOL) startWithPreviewView: (UIView *)previewView preset: (NSString *)preset
{
	AVCaptureDevice				*captureDevice;
	NSError						*error;
	
	if( previewView == nil ) {
		return NO;
	}
	
	[_lock lock];
	
	if( _status != AVCameraManagerStatusIdle ) {
		goto START_WITH_PREVIEW_FAILED;
	}
	
	if( (_session = [[AVCaptureSession alloc] init]) == nil ) {
		goto START_WITH_PREVIEW_FAILED;
	}
	
	if( [preset length] > 0 ) {
		_session.sessionPreset = preset;
	}
	
	if( (captureDevice = [AVCaptureDevice defaultDeviceWithMediaType: AVMediaTypeVideo]) == nil ) {
		goto START_WITH_PREVIEW_FAILED;
	}
	
	if( (_videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession: _session]) == nil ) {
		goto START_WITH_PREVIEW_FAILED;
	}
	_videoPreviewLayer.frame = previewView.bounds;
	[previewView.layer addSublayer: _videoPreviewLayer];
	
	error = nil;
	
	if( (_deviceInput = [AVCaptureDeviceInput deviceInputWithDevice: captureDevice error: &error]) == nil ) {
		goto START_WITH_PREVIEW_FAILED;
	}
	
	if( [_session canAddInput: _deviceInput] == NO ) {
		goto START_WITH_PREVIEW_FAILED;
	}
	[_session addInput: _deviceInput];
	
	if( (_stillImageOutput = [[AVCaptureStillImageOutput alloc] init]) == nil ) {
		goto START_WITH_PREVIEW_FAILED;
	}
	[_stillImageOutput setOutputSettings: @{ AVVideoCodecJPEG:AVVideoCodecKey }];
	
	if( [_session canAddOutput: _stillImageOutput] == NO ) {
		goto START_WITH_PREVIEW_FAILED;
	}
	[_session addOutput: _stillImageOutput];
	
	[_session startRunning];
	
	_status = AVCameraManagerStatusRunning;
	[self postNotifyWithStatus: AVCameraManagerStatusRunning];
	
	[_lock unlock];
	
	return YES;
	
START_WITH_PREVIEW_FAILED:
	
	[_lock unlock];
	
	if( _session != nil ) {
		_session = nil;
	}
	if( _videoPreviewLayer != nil ) {
		[_videoPreviewLayer removeFromSuperlayer];
		_videoPreviewLayer = nil;
	}
	if( _deviceInput != nil ) {
		_deviceInput = nil;
	}
	if( _stillImageOutput != nil ) {
		_stillImageOutput = nil;
	}
	
	_status = AVCameraManagerStatusAccessDenied;
	[self postNotifyWithStatus: AVCameraManagerStatusAccessDenied];
	
	return NO;
}

- (void) stop
{
	[_lock lock];
	
	if( _status == AVCameraManagerStatusRunning ) {
		[_session stopRunning];
		_session = nil;
		[_videoPreviewLayer removeFromSuperlayer];
		_videoPreviewLayer = nil;
		_deviceInput = nil;
		_stillImageOutput = nil;
		_status = AVCameraManagerStatusIdle;
		[self postNotifyWithStatus: AVCameraManagerStatusIdle];
	}
	
	[_lock unlock];
}

- (BOOL) toggleCamera
{
	AVCaptureDevicePosition		currentPosition;
	AVCaptureDevicePosition		togglePosition;
	BOOL						result;
	
	[_lock lock];
	
	if( _status != AVCameraManagerStatusRunning ) {
		[_lock unlock];
		return NO;
	}
	
	currentPosition = [[_deviceInput device] position];
	
	if( currentPosition == AVCaptureDevicePositionFront ) {
		togglePosition = AVCaptureDevicePositionBack;
	} else {
		togglePosition = AVCaptureDevicePositionFront;
	}
	
	result = [self toggleCameraToPosition: togglePosition];
	
	[_lock unlock];
	
	return result;
}

- (BOOL) toggleCameraToFront
{
	BOOL						result;
	
	[_lock lock];
	
	if( _status != AVCameraManagerStatusRunning ) {
		[_lock unlock];
		return NO;
	}
	
	result = [self toggleCameraToPosition: AVCaptureDevicePositionFront];
	
	[_lock unlock];
	
	return result;
}

- (BOOL) toggleCameraToBack
{
	BOOL						result;
	
	[_lock lock];
	
	if( _status != AVCameraManagerStatusRunning ) {
		[_lock unlock];
		return NO;
	}
	
	result = [self toggleCameraToPosition: AVCaptureDevicePositionBack];
	
	[_lock unlock];
	
	return result;
}

- (BOOL) captureStillImage
{
	AVCaptureConnection		*connection;
	AVCaptureInputPort		*inputPort;
	AVCaptureConnection		*videoConnection;
	
	[_lock lock];
	
	if( _status != AVCameraManagerStatusRunning ) {
		[_lock unlock];
		return NO;
	}
	
	videoConnection = nil;
	
	for( connection in _stillImageOutput.connections ) {
		for( inputPort in [connection inputPorts] ) {
			if( [[inputPort mediaType] isEqualToString: AVMediaTypeVideo] == YES ) {
				videoConnection = connection;
				break;
			}
		}
		if( videoConnection != nil ) {
			break;
		}
	}
	
	if( videoConnection == nil ) {
		[_lock unlock];
		return NO;
	}
	
	[_stillImageOutput captureStillImageAsynchronouslyFromConnection: videoConnection completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error)
	 {
		 CFDictionaryRef			exifAttachments;
		 NSData						*data;
		 UIImage					*image;
		 AVCaptureDevicePosition	position;
		 
		 image = nil;
		 
		 if( (exifAttachments = CMGetAttachment( imageSampleBuffer, kCGImagePropertyExifDictionary, NULL)) != NULL ) {
			 if( (data = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation: imageSampleBuffer]) != nil ) {
				image = [[UIImage alloc] initWithData: data];
			 }
		 }
		 
		 if( image != nil ) {
			 position = [[_deviceInput device] position];
			 [self postNotifyStillImageCapturedWithImage: image devicePosition: position];
		 } else {
			 [self postNotifyWithStatus: AVCameraManagerStatusStillImageCaptureFailed];
		 }
	 }];
	
	[_lock unlock];
	
	return YES;
}

- (BOOL) toggleCameraToPosition: (AVCaptureDevicePosition)position
{
	AVCaptureDevicePosition		currentPosition;
	AVCaptureDevice				*captureDevice;
	AVCaptureDeviceInput		*captureDeviceInput;
	NSError						*error;
		
	if( _status != AVCameraManagerStatusRunning ) {
		return NO;
	}
	
	currentPosition = [[_deviceInput device] position];
	
	if( currentPosition == position ) {
		return YES;
	}
	
	if( (captureDevice = [self captureDeviceForPosition: position]) == nil ) {
		return NO;
	}
	
	if( (captureDeviceInput = [[AVCaptureDeviceInput alloc] initWithDevice: captureDevice error: &error]) == nil ) {
		return NO;
	}
	
	[_session beginConfiguration];
	[_session removeInput: _deviceInput];
	if( [_session canAddInput: captureDeviceInput] == NO ) {
		[_session addInput: _deviceInput];
		return NO;
	}
	_deviceInput = captureDeviceInput;
	[_session addInput: _deviceInput];
	[_session commitConfiguration];
		
	return YES;
}

- (void) postNotifyWithStatus: (AVCameraManagerStatus)status
{
	NSDictionary	*paramDict;
	
	paramDict = @{
				  AVCameraManagerNotifyParameterKeyStatus:[NSNumber numberWithInteger:(NSInteger)status]
				  };
		
	[self performSelectorOnMainThread: @selector(postNotifyWithParamDict:) withObject: paramDict waitUntilDone: NO];
}

- (void) postNotifyStillImageCapturedWithImage: (UIImage *)image devicePosition: (AVCaptureDevicePosition)position
{
	NSDictionary	*paramDict;
	NSString		*frontCamera;
	
	frontCamera = (position == AVCaptureDevicePositionFront) ? @"Y" : @"N";
	
	paramDict = @{
					AVCameraManagerNotifyParameterKeyStatus:[NSNumber numberWithInteger:(NSInteger)AVCameraManagerStatusStillImageCaptured],
					AVCameraManagerNotifyParameterKeyStillImage:image,
					AVCameraManagerNotifyParameterKeyFrontCamera:frontCamera
				};
	
	[self performSelectorOnMainThread: @selector(postNotifyWithParamDict:) withObject: paramDict waitUntilDone: NO];
}

- (void) postNotifyWithParamDict: (NSDictionary *)paramDict
{
	[[NSNotificationCenter defaultCenter] postNotificationName: AVCameraManagerNotification object: self userInfo: paramDict];
}

- (AVCaptureDevice *) captureDeviceForPosition: (AVCaptureDevicePosition)position;
{
	NSArray				*devices;
	AVCaptureDevice		*device;
	
	devices = [AVCaptureDevice devicesWithMediaType: AVMediaTypeVideo];
	
	for( device in devices ) {
		if( [device position] == position ) {
			return device;
		}
	}
	
	return nil;
}

@end
