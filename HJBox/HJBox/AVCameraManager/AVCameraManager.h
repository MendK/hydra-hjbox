//
//  AVCameraManager.h
//  HJBox
//
//  Created by Na Tae Hyun on 13. 11. 4..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <ImageIO/CGImageProperties.h>


#define		AVCameraManagerNotification							@"avCameraManagerNotification"

#define		AVCameraManagerNotifyParameterKeyStatus				@"avCameraManagerNotifyParameterKeyStatus"
#define		AVCameraManagerNotifyParameterKeyStillImage			@"avCameraManagerNotifyParameterKeyStillImage"
#define		AVCameraManagerNotifyParameterKeyFrontCamera		@"avCameraManagerNotifyParameterKeyFrontCamera"


typedef enum _AVCameraManagerStatus_
{
	AVCameraManagerStatusDummy,
	AVCameraManagerStatusIdle,
	AVCameraManagerStatusRunning,
	AVCameraManagerStatusStillImageCaptured,
	AVCameraManagerStatusStillImageCaptureFailed,
	AVCameraManagerStatusAccessDenied,
	AVCameraManagerStatusUnknownError,
	kCountOfAVCameraManagerStatus
	
} AVCameraManagerStatus;


@interface AVCameraManager : NSObject
{
	AVCameraManagerStatus		_status;
	AVCaptureSession			*_session;
	AVCaptureVideoPreviewLayer	*_videoPreviewLayer;
	AVCaptureDeviceInput		*_deviceInput;
	AVCaptureStillImageOutput	*_stillImageOutput;
	NSLock						*_lock;
}

+ (AVCameraManager *) defaultManager;

- (NSInteger) countOfCamera;

- (BOOL) startWithPreviewView: (UIView *)previewView preset: (NSString *)preset;
- (void) stop;

- (BOOL) toggleCamera;
- (BOOL) toggleCameraToFront;
- (BOOL) toggleCameraToBack;

- (BOOL) captureStillImage;

@property (nonatomic, readonly) AVCameraManagerStatus status;

@end
