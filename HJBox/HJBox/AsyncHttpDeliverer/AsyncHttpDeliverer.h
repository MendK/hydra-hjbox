//
//  AsyncHttpDeliverer.h
//  HJBox
//
//  Created by Na Tae Hyun on 13. 4. 4..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import <Foundation/Foundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <Hydra/Hydra.h>


#define		AsyncHttpDelivererNotification							@"asyncHttpDelivererNotifification"

#define		AsyncHttpDelivererParameterKeyFailed					@"asyncHttpDelivererParameterKeyFailed"
#define		AsyncHttpDelivererParameterKeyCanceled					@"asyncHttpDelivererParameterKeyCanceled"
#define		AsyncHttpDelivererParameterKeyUrlString					@"asyncHttpDelivererParameterKeyUrlString"
#define		AsyncHttpDelivererParameterKeyResponse					@"asyncHttpDelivererParameterKeyResponse"
#define		AsyncHttpDelivererParameterKeyBody						@"asyncHttpDelivererParameterKeyBody"
#define		AsyncHttpDelivererParameterKeyIssuedId					@"asyncHttpDelivererParameterKeyIssuedId"
#define		AsyncHttpDelivererParameterKeyStatus					@"asyncHttpDelivererParameterKeyStatus"
#define		AsyncHttpDelivererParameterKeyContentLength				@"asyncHttpDelivererParameterKeyContentLength"
#define		AsyncHttpDelivererParameterKeyAmountTransferedLength	@"asyncHttpDelivererParameterKeyAmountTransferedLength"
#define		AsyncHttpDelivererParameterKeyExpectedTransferedLength	@"AsyncHttpDelivererParameterKeyExpectedTransferedLength"
#define		AsyncHttpDelivererParameterKeyCurrentTransferedLength	@"asyncHttpDelivererParameterKeyCurrentTransferedLength"
#define		AsyncHttpDelivererParameterKeyFilePath					@"asyncHttpDelivererParameterKeyTransferedFilePath"
#define		AsyncHttpDelivererParameterKeyWorkingTimeByMilisecond	@"asyncHttpDelivererParameterKeyWorkingTimeByMilisecond"


typedef enum _AsyncHttpDelivererStatus_
{
	AsyncHttpDelivererStatusStart,
	AsyncHttpDelivererstatusConnected,
	AsyncHttpDelivererStatusTransfering,
	AsyncHttpDelivererStatusDone,
	AsyncHttpDelivererStatusCanceled,
	AsyncHttpDelivererStatusFailed,
	kCountOfAsyncHttpDelivererStatus
	
} AsyncHttpDelivererStatus;

typedef enum _AsyncHttpDelivererPostContentType_
{
	AsyncHttpDelivererPostContentTypeMultipart,
	AsyncHttpDelivererPostContentTypeUrlEncoded,
	AsyncHttpDelivererPostContentTypeApplicationJson,
	kCountOfAsyncHttpDelivererPostContentType
	
} AsyncHttpDelivererPostContentType;


@interface AsyncHttpDeliverer : HYAsyncTask <NSStreamDelegate>
{
	NSMutableURLRequest		*_request;
	NSURLConnection			*_connection;
	NSURLResponse			*_response;
	NSMutableData			*_sendData;
	NSMutableData			*_receivedData;
	BOOL					_notifyStatus;
	NSString				*_urlString;
	NSMutableDictionary		*_queryStringFieldDict;
	NSMutableDictionary		*_headerFieldDict;
	NSMutableDictionary		*_formDataFieldDict;
	NSMutableDictionary		*_formDataFileNameDict;
	NSMutableDictionary		*_formDataContentTypeDict;
	NSString				*_uploadFileFormDataField;
	NSString				*_uploadFileName;
	NSString				*_uploadFileContentType;
	NSString				*_uploadFilePath;
	NSString				*_downloadFilePath;
	NSFileHandle			*_fileHandle;
	NSString				*_multipartBoundaryString;
	NSArray					*_trustedHosts;
	NSNumber				*_lastUploadContentLengthNumber;
	NSInteger				_transferBufferSize;
	BOOL					_playWithLimitPool;
	uint8_t					*_buffer;
	NSUInteger				_bufferSize;
	NSUInteger				_filledSize;
	NSUInteger				_lookingIndex;
	NSOutputStream			*_producerStream;
	NSInputStream			*_fileStream;
	NSMutableDictionary		*_sharedDict;
}

- (BOOL) setGetWithUrlString: (NSString *)urlString;
- (BOOL) setGetWithUrlString: (NSString *)urlString queryStringDict: (NSDictionary *)queryStringDict;
- (BOOL) setGetWithUrlString: (NSString *)urlString toFilePath: (NSString *)filePath;
- (BOOL) setGetWithUrlString: (NSString *)urlString queryStringDict: (NSDictionary *)queryStringDict toFilePath: (NSString *)filePath;
- (BOOL) setPostWithUrlString: (NSString *)urlString formDataDict: (NSDictionary *)dict contentType: (AsyncHttpDelivererPostContentType)contentType;
- (BOOL) setPostUploadWithUrlString: (NSString *)urlString formDataField: (NSString *)fieldName fileName: (NSString *)fileName fileContentType: (NSString *)fileContentType data: (NSData *)data;
- (BOOL) setPostUploadWithUrlString: (NSString *)urlString formDataField: (NSString *)fieldName fileName: (NSString *)fileName fileContentType: (NSString *)fileContentType filePath: (NSString *)filePath;

- (BOOL) setValue: (NSString *)value forQueryStringField: (NSString *)fieldName;
- (BOOL) setValuesFromQueryStringDict: (NSDictionary *)dict;
- (void) removeValueForQueryStringField: (NSString *)fieldName;
- (void) clearAllQueryStringFields;

- (BOOL) setValue: (NSString *)value forHeaderField: (NSString *)fieldName;
- (BOOL) setValuesFromHeaderFieldDict: (NSDictionary *)dict;
- (void) removeValueForHeaderField: (NSString *)fieldName;
- (void) clearAllHeaderFields;

- (BOOL) setValue: (NSString *)value forFormDataField: (NSString *)fieldName;
- (BOOL) setData: (NSData *)data forFormDataField: (NSString *)fieldName fileName: (NSString *)fileName contentType: (NSString *)contentType;
- (BOOL) setFileForStreammingUpload: (NSString *)filePath forFormDataField: (NSString *)fieldName fileName: (NSString *)fileName contentType: (NSString *)contentType;
- (BOOL) setValuesFromFormDataDict: (NSDictionary *)dict;
- (void) removeValueForFormDataField: (NSString *)fieldName;
- (void) clearAllFormDataFields;

- (BOOL) setBodyData: (NSData *)bodyData;

@property (nonatomic, assign) BOOL notifyStatus;
@property (nonatomic, assign) NSURLRequestCachePolicy cachePolicy;
@property (nonatomic, assign) NSTimeInterval timeoutInterval;
@property (nonatomic, strong) NSString *urlString;
@property (nonatomic, strong) NSString *method;
@property (nonatomic, strong) NSString *multipartBoundaryString;
@property (nonatomic, strong) NSArray *trustedHosts;
@property (nonatomic, assign) NSInteger transferBufferSize;

@end
