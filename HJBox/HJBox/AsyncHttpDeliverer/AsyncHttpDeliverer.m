//
//  AsyncHttpDeliverer.m
//  HJBox
//
//  Created by Na Tae Hyun on 13. 4. 4..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import "AsyncHttpDeliverer.h"


#define		kMultipartBoundaryString	@"----------i0am1default2boundary3for4asynchttpdeliverer5"
#define		kTransferBufferSize			8192


@interface AsyncHttpDeliverer( AsyncHttpDelivererPrivate )

- (NSString *) stringForUrlEncoded: (NSString *)string;
- (NSString *) stringForUrlEncodedFromDict: (NSMutableDictionary *)dict;
- (void) resetTransfer;
- (void) closeProducerStream;
- (NSString *) suggestFileNameForFilePath: (NSString *)filePath;
- (NSString *) suggestMimeTypeForFilePath: (NSString *)filePath;
- (BOOL) fillSendDataWithFormData;
- (BOOL) bindConnection;
- (NSInputStream *) makeBoundInputStreamWithBufferSize: (NSUInteger)bufferSize;
- (void) doneWithError;
- (void) postNotifyStatus: (NSDictionary *)paramDict;
- (void) pushNotifyStatusToMainThread: (NSDictionary *)paramDict;

@end


@implementation AsyncHttpDeliverer

@synthesize notifyStatus = _notifyStatus;
@dynamic cachePolicy;
@dynamic timeoutInterval;
@dynamic urlString;
@dynamic method;
@synthesize multipartBoundaryString = _multipartBoundaryString;
@synthesize trustedHosts = _trustedHosts;
@dynamic transferBufferSize;

- (id) initWithCloseQuery: (id)anQuery
{
	if( (self = [super initWithCloseQuery: anQuery]) != nil ) {
		if( (_request = [[NSMutableURLRequest alloc] init]) == nil ) {
			return nil;
		}
		if( (_queryStringFieldDict = [[NSMutableDictionary alloc] init]) == nil ) {
			return nil;
		}
		if( (_headerFieldDict = [[NSMutableDictionary alloc] init]) == nil ) {
			return nil;
		}
		if( (_formDataFieldDict = [[NSMutableDictionary alloc] init]) == nil ) {
			return nil;
		}
		if( (_formDataFileNameDict = [[NSMutableDictionary alloc] init]) == nil ) {
			return nil;
		}
		if( (_formDataContentTypeDict = [[NSMutableDictionary alloc] init]) == nil ) {
			return nil;
		}
		if( (_sharedDict = [[NSMutableDictionary alloc] init]) == nil ) {
			return nil;
		}
		if( (_sendData = [[NSMutableData alloc] init]) == nil ) {
			return nil;
		}
		_multipartBoundaryString = kMultipartBoundaryString;
		_transferBufferSize = kTransferBufferSize;
	}
	
	return self;
}

- (void) dealloc
{
	[self resetTransfer];
}

- (NSString *) stringForUrlEncoded: (NSString *)string
{
	return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes( kCFAllocatorDefault, (CFStringRef)string, NULL, CFSTR("!*'();:@&=+$,/?%#[]"), kCFStringEncodingUTF8 ));
}

- (NSString *) stringForUrlEncodedFromDict: (NSMutableDictionary *)dict
{
	NSString		*key, *encodedKey;
	NSString		*value, *encodedValue;
	id				anObject;
	NSMutableString	*string;
	
	if( [dict count] <= 0 ) {
		return nil;
	}
	
	if( (string = [[NSMutableString alloc] init]) == nil ) {
		return nil;
	}
	
	for( key in dict ) {
		anObject = [dict objectForKey: key];
		if( [anObject isKindOfClass: [NSString class]] == YES ) {
			value = (NSString *)anObject;
		} else if( [anObject isKindOfClass: [NSData class]] == YES ) {
			value = [[NSString alloc] initWithData: anObject encoding: NSUTF8StringEncoding];
		} else {
			continue;
		}
		encodedKey = [self stringForUrlEncoded: key];
		encodedValue = [self stringForUrlEncoded: value];
		if( [string length] <= 0 ) {
			[string appendFormat: @"%@=%@", encodedKey, encodedValue];
		} else {
			[string appendFormat: @"&%@=%@", encodedKey, encodedValue];
		}
	}
	
	return string;
}

- (void) resetTransfer
{
	if( _fileHandle != nil ) {
		[_fileHandle closeFile];
		_fileHandle = nil;
	}
	
	if( _connection != nil ) {
		[_connection cancel];
		_connection = nil;
	}
	
	if( _producerStream != nil ) {
		_producerStream.delegate = nil;
		[_producerStream removeFromRunLoop: [NSRunLoop currentRunLoop] forMode: NSDefaultRunLoopMode];
		[_producerStream close];
		_producerStream = nil;
	}
	
	if( _fileStream != nil ) {
		[_fileStream close];
		_fileStream = nil;
	}
	
	if( _response != nil ) {
		_response = nil;
	}
	_receivedData = nil;

	if( _buffer != NULL ) {
		free( _buffer );
		_buffer = NULL;
	}
	_bufferSize = 0;
}

- (void) closeProducerStream
{
	if( _producerStream != nil ) {
		_producerStream.delegate = nil;
		[_producerStream removeFromRunLoop: [NSRunLoop currentRunLoop] forMode: NSDefaultRunLoopMode];
		[_producerStream close];
		_producerStream = nil;
	}
}

- (NSString *) suggestFileNameForFilePath: (NSString *)filePath
{
	return [filePath lastPathComponent];
}

- (NSString *) suggestMimeTypeForFilePath: (NSString *)filePath
{
	NSString	*pathExtention;
	CFStringRef	uti;
	CFStringRef	mimeType;
	
	if( (pathExtention = [filePath pathExtension]) == nil ) {
		return nil;
	}
	
	uti = UTTypeCreatePreferredIdentifierForTag( kUTTagClassFilenameExtension, (__bridge CFStringRef)pathExtention, NULL );
	mimeType = UTTypeCopyPreferredTagWithClass( uti, kUTTagClassMIMEType );
	CFRelease( uti );
	
	return CFBridgingRelease( mimeType );
}

- (BOOL) fillSendDataWithFormData
{
	NSData		*boundaryData;
	NSString	*fieldName;
	id			anObject;
	NSString	*value;
	NSData		*data;
	NSString	*fileName;
	NSString	*fileContentType;
	NSError		*error;
	
	[_sendData setLength: 0];
	
	boundaryData = [[NSString stringWithFormat: @"--%@\r\n", _multipartBoundaryString] dataUsingEncoding: NSUTF8StringEncoding];
	
	if( [_formDataFieldDict count] > 0 ) {
		if( [[_headerFieldDict objectForKey: @"Content-Type"] isEqualToString: @"application/x-www-form-urlencoded"] == YES ) {
			if( (value = [self stringForUrlEncodedFromDict: _formDataFieldDict]) != nil ) {
				[_sendData appendData: [value dataUsingEncoding: NSUTF8StringEncoding]];
			}
		} else if( [[_headerFieldDict objectForKey: @"Content-Type"] rangeOfString: @"multipart/form-data"].location != NSNotFound ) {
			for( fieldName in _formDataFieldDict ) {
				anObject = [_formDataFieldDict objectForKey: fieldName];
				if( [anObject isKindOfClass: [NSString class]] == YES ) {
					value = (NSString *)anObject;
					[_sendData appendData: boundaryData];
					[_sendData appendData: [[NSString stringWithFormat: @"Content-Disposition: form-data; name=\"%@\"\r\n\r\n%@\r\n", fieldName, value] dataUsingEncoding: NSUTF8StringEncoding]];
				} else if( [anObject isKindOfClass: [NSData class]] == YES ) {
					data = (NSData *)anObject;
					fileName = [_formDataFileNameDict objectForKey: fieldName];
					fileContentType = [_formDataContentTypeDict objectForKey: fieldName];
					[_sendData appendData: boundaryData];
					if( ([fileName length] > 0) && ([fileContentType length] > 0) ) {
						[_sendData appendData: [[NSString stringWithFormat: @"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\nContent-Type: %@\r\n\r\n", fieldName, fileName, fileContentType] dataUsingEncoding: NSUTF8StringEncoding]];
					} else {
						[_sendData appendData: [[NSString stringWithFormat: @"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", fieldName] dataUsingEncoding: NSUTF8StringEncoding]];
					}
					[_sendData appendData: data];
					[_sendData appendData: [[NSString stringWithFormat: @"\r\n"] dataUsingEncoding: NSUTF8StringEncoding]];
				}
			}
			[_sendData appendData: [[NSString stringWithFormat: @"--%@--\r\n", _multipartBoundaryString] dataUsingEncoding: NSUTF8StringEncoding]];
		} else if( [[_headerFieldDict objectForKey: @"Content-Type"] isEqualToString: @"application/json"] == YES ) {
			if( [NSJSONSerialization isValidJSONObject: _formDataFieldDict] == YES ) {
				if( (data = [NSJSONSerialization dataWithJSONObject: _formDataFieldDict options: NSJSONWritingPrettyPrinted error: &error]) != nil ) {
					[_sendData appendData: data];
				}
			}
		}
	}
	
	if( [_uploadFilePath length] > 0 ) {
		[_sendData appendData: boundaryData];
		[_sendData appendData: [[NSString stringWithFormat: @"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\nContent-Type: %@\r\n\r\n", _uploadFileFormDataField, _uploadFileName, _uploadFileContentType] dataUsingEncoding: NSUTF8StringEncoding]];
		_bufferSize = ([_sendData length] > _transferBufferSize) ? [_sendData length] : _transferBufferSize;
		if( (_buffer = (uint8_t *)malloc( (size_t)_bufferSize )) == NULL ) {
			return NO;
		}
		_filledSize = [_sendData length];
		_lookingIndex = 0;
		memcpy( _buffer, [_sendData bytes], _filledSize );
	}
	
	return YES;
}

- (BOOL) bindConnection
{
	NSUInteger			contentLength;
	NSInputStream		*inputStream;
	
	if( [_uploadFilePath length] > 0 ) {
			
		if( (_fileStream = [[NSInputStream alloc] initWithFileAtPath: _uploadFilePath]) == nil ) {
			return NO;
		}
		[_fileStream open];
		
		if( (inputStream = [self makeBoundInputStreamWithBufferSize: _transferBufferSize]) == nil ) {
			return NO;
		}
		_producerStream.delegate = self;
		[_producerStream scheduleInRunLoop: [NSRunLoop currentRunLoop] forMode: NSDefaultRunLoopMode];
		[_producerStream open];
		
		[_sendData appendData: [[NSString stringWithFormat: @"\r\n--%@--\r\n", _multipartBoundaryString] dataUsingEncoding: NSUTF8StringEncoding]];
		
		contentLength = [_sendData length];
		contentLength += [(NSNumber *)[[[NSFileManager defaultManager] attributesOfItemAtPath: _uploadFilePath error: NULL] objectForKey: NSFileSize] unsignedIntegerValue];
		
		[_request setHTTPBodyStream: inputStream];
		
	} else {
		
		if( (contentLength = [_sendData length]) > 0 ) {
			[_request setHTTPBody: _sendData];
		}
		
	}
	
	if( contentLength <= 0 ) {
		contentLength = [[_request HTTPBody] length];
	}
	
	_lastUploadContentLengthNumber = [NSNumber numberWithUnsignedInteger: contentLength];
	[_request setValue: [[NSNumber numberWithUnsignedInteger: contentLength] stringValue] forHTTPHeaderField: @"Content-Length"];
	
	HYTRACE_BLOCK
	(
		HYTRACE( @"- AsyncHttpDeliverer [%d] request start", _issuedId );
		HYTRACE( @"- url    [%@]", [_request URL] );
		HYTRACE( @"- method [%@]", [_request HTTPMethod] );
		for( NSString *key in [_request allHTTPHeaderFields] ) {
			HYTRACE( @"- header [%@][%@]", key, [[_request allHTTPHeaderFields] objectForKey: key] );
		}
		if( [_uploadFilePath length] > 0 ) {
			HYTRACE( @"- body    STREAMMING" );
		} else {
			HYTRACE( @"- body    [%@]", [[NSString alloc] initWithData: [_request HTTPBody] encoding: NSUTF8StringEncoding] );
		}
	)
	
	if( (_connection = [[NSURLConnection alloc] initWithRequest: _request delegate: self]) == nil ) {
		return NO;
	}
	
	return YES;
}

- (NSInputStream *) makeBoundInputStreamWithBufferSize: (NSUInteger)bufferSize
{
	CFReadStreamRef			readStream;
	CFWriteStreamRef		writeStream;
	
	if( bufferSize == 0 ) {
		return nil;
	}
	
	readStream = NULL;
	writeStream = NULL;
	
	CFStreamCreateBoundPair( NULL, &readStream, &writeStream, (CFIndex)bufferSize );
	
	_producerStream = CFBridgingRelease( writeStream );
	
	return CFBridgingRelease( readStream );
}

- (void) doneWithError
{
	[_closeQuery setParameter: @"Y" forKey: AsyncHttpDelivererParameterKeyFailed];
	
	if( _notifyStatus == YES ) {
		[self pushNotifyStatusToMainThread: [NSDictionary dictionaryWithObjectsAndKeys:
											 [NSNumber numberWithUnsignedInteger:(NSUInteger)self.issuedId], AsyncHttpDelivererParameterKeyIssuedId,
											 _urlString, AsyncHttpDelivererParameterKeyUrlString,
											 [NSNumber numberWithInteger:(NSInteger)AsyncHttpDelivererStatusFailed], AsyncHttpDelivererParameterKeyStatus,
											 nil]
		 ];
	}
	
	[self done];
}

- (void) postNotifyStatus: (NSDictionary *)paramDict
{
	[[NSNotificationCenter defaultCenter] postNotificationName: AsyncHttpDelivererNotification object: self userInfo: paramDict];
}

- (void) pushNotifyStatusToMainThread: (NSDictionary *)paramDict
{
	[self performSelectorOnMainThread: @selector(postNotifyStatus:) withObject: paramDict waitUntilDone: NO];
}

- (BOOL) setGetWithUrlString: (NSString *)urlString
{
	if( [urlString length] <= 0 ) {
		return NO;
	}
	
	self.urlString = urlString;
	[self setMethod: @"GET"];
	
	return YES;
}

- (BOOL) setGetWithUrlString: (NSString *)urlString queryStringDict: (NSDictionary *)queryStringDict
{
	if( [self setGetWithUrlString: urlString] == NO ) {
		return NO;
	}
	
	[self setValuesFromQueryStringDict: queryStringDict];
	
	return YES;
}

- (BOOL) setGetWithUrlString: (NSString *)urlString toFilePath: (NSString *)filePath
{
	if( [filePath length] <= 0 ) {
		return NO;
	}
	
	if( [self setGetWithUrlString: urlString] == NO ) {
		return NO;
	}
	
	_downloadFilePath = filePath;
	
	return YES;
}

- (BOOL) setGetWithUrlString: (NSString *)urlString queryStringDict: (NSDictionary *)queryStringDict toFilePath: (NSString *)filePath
{
	if( [self setGetWithUrlString: urlString queryStringDict: queryStringDict] == NO ) {
		return NO;
	}
	
	[self setValuesFromQueryStringDict: queryStringDict];
	
	return YES;
}

- (BOOL) setPostWithUrlString: (NSString *)urlString formDataDict: (NSDictionary *)dict contentType: (AsyncHttpDelivererPostContentType)contentType
{
	if( [urlString length] <= 0 ) {
		return NO;
	}
		
	switch( contentType ) {
		case AsyncHttpDelivererPostContentTypeMultipart :
			[self setValue: [NSString stringWithFormat :@"multipart/form-data; boundary=%@", _multipartBoundaryString] forHeaderField: @"Content-Type"];
			break;
		case AsyncHttpDelivererPostContentTypeUrlEncoded :
			[self setValue: @"application/x-www-form-urlencoded" forHeaderField: @"Content-Type"];
			break;
		case AsyncHttpDelivererPostContentTypeApplicationJson :
			[self setValue: @"application/json" forHeaderField: @"Content-Type"];
			[self setValue: @"application/json" forHeaderField: @"Accept"];
			break;
		default :
			return NO;
	}
	
	self.urlString = urlString;
	[self setMethod: @"POST"];
	
	[self setValuesFromFormDataDict: dict];
	
	return YES;
}

- (BOOL) setPostUploadWithUrlString: (NSString *)urlString formDataField: (NSString *)fieldName fileName: (NSString *)fileName fileContentType: (NSString *)fileContentType data: (NSData *)data
{
	if( ([urlString length] <= 0) || ([fieldName length] <= 0) || ([fileName length] <= 0) || ([fileContentType length] <=0) || ([data length] <= 0) ) {
		return NO;
	}
	
	[self setValue: [NSString stringWithFormat :@"multipart/form-data; boundary=%@", _multipartBoundaryString] forHeaderField: @"Content-Type"];
	self.urlString = urlString;
	[self setMethod: @"POST"];
	
	[self setData: data forFormDataField: fieldName fileName: fileName contentType: fileContentType];
	
	return YES;
}

- (BOOL) setPostUploadWithUrlString: (NSString *)urlString formDataField: (NSString *)fieldName fileName: (NSString *)fileName fileContentType: (NSString *)fileContentType filePath: (NSString *)filePath
{
	if( [urlString length] <= 0 ) {
		return NO;
	}
	
	if( [self setFileForStreammingUpload: filePath forFormDataField: fieldName fileName: fileName contentType: fileContentType] == NO ) {
		return NO;
	}
	
	[self setValue: [NSString stringWithFormat :@"multipart/form-data; boundary=%@", _multipartBoundaryString] forHeaderField: @"Content-Type"];
	self.urlString = urlString;
	[self setMethod: @"POST"];
	
	return YES;
}

- (BOOL) setValue: (NSString *)value forQueryStringField: (NSString *)fieldName
{
	if( ([value length] <= 0) || ([fieldName length] <= 0) ) {
		return NO;
	}
	
	[_queryStringFieldDict setObject: value forKey: fieldName];
	
	return YES;
}

- (BOOL) setValuesFromQueryStringDict: (NSDictionary *)dict
{
	if( [dict count] <= 0 ) {
		return YES;
	}
	
	[_queryStringFieldDict setValuesForKeysWithDictionary: dict];
	
	return YES;
}

- (void) removeValueForQueryStringField: (NSString *)fieldName
{
	if( [fieldName length] <= 0 ) {
		return;
	}
	
	[_queryStringFieldDict removeObjectForKey: fieldName];
}

- (void) clearAllQueryStringFields
{
	[_queryStringFieldDict removeAllObjects];
}

- (BOOL) setValue: (NSString *)value forHeaderField: (NSString *)fieldName
{
	if( ([value length] <= 0) || ([fieldName length] <= 0) ) {
		return NO;
	}
	
	[_headerFieldDict setObject: value forKey: fieldName];
	
	return YES;
}

- (BOOL) setValuesFromHeaderFieldDict: (NSDictionary *)dict
{
	if( [dict count] <= 0 ) {
		return YES;
	}
	
	[_headerFieldDict setValuesForKeysWithDictionary: dict];
	
	return YES;
}

- (void) removeValueForHeaderField: (NSString *)fieldName
{
	if( [fieldName length] <= 0 ) {
		return;
	}
	
	[_headerFieldDict removeObjectForKey: fieldName];
}

- (void) clearAllHeaderFields
{
	[_headerFieldDict removeAllObjects];
}

- (BOOL) setValue: (NSString *)value forFormDataField: (NSString *)fieldName
{
	if( ([value length] <= 0 ) || ([fieldName length] <= 0) ) {
		return NO;
	}
		
	[_formDataFieldDict setObject: value forKey: fieldName];
	
	return YES;
}

- (BOOL) setData: (NSData *)data forFormDataField: (NSString *)fieldName fileName: (NSString *)fileName contentType: (NSString *)contentType
{
	if( ([data length] <= 0) || ([fieldName length] <= 0) || ([fileName length] <= 0) || ([contentType length] <= 0) ) {
		return NO;
	}
	
	[_formDataFieldDict setObject: data forKey: fieldName];
	[_formDataFileNameDict setObject: fileName forKey: fieldName];
	[_formDataContentTypeDict setObject: contentType forKey: fieldName];
	
	return YES;
}

- (BOOL) setFileForStreammingUpload: (NSString *)filePath forFormDataField: (NSString *)fieldName fileName: (NSString *)fileName contentType: (NSString *)contentType
{
	BOOL		isDirectory;
	
	if( ([filePath length] <= 0) || ([fieldName length] <= 0) || ([fileName length] <= 0) || ([contentType length] <= 0) ) {
		return NO;
	}
	if( [[NSFileManager defaultManager] fileExistsAtPath: filePath isDirectory: &isDirectory] == NO ) {
		return NO;
	}
	if( isDirectory == YES ) {
		return NO;
	}
		
	_uploadFileFormDataField = fieldName;
	_uploadFileContentType = contentType;
	_uploadFileName = fileName;
	_uploadFilePath = filePath;
	
	if( [_uploadFileName length] <= 0 ) {
		_uploadFileName = [self suggestFileNameForFilePath: _uploadFilePath];
	}
	if( [_uploadFileContentType length] <= 0 ) {
		_uploadFileContentType = [self suggestMimeTypeForFilePath: _uploadFileName];
	}
	
	if( ([_uploadFileName length] <= 0) || ([_uploadFileContentType length] <= 0) ) {
		_uploadFileFormDataField = nil;
		_uploadFileContentType = nil;
		_uploadFileName = nil;
		_uploadFilePath = nil;
		return NO;
	}
	
	return YES;
}

- (BOOL) setValuesFromFormDataDict: (NSDictionary *)dict
{
	if( [dict count] <= 0 ) {
		return YES;
	}
	
	[_formDataFieldDict setValuesForKeysWithDictionary: dict];
	
	return YES;
}

- (void) removeValueForFormDataField: (NSString *)fieldName
{
	if( [fieldName length] <= 0 ) {
		return;
	}
	
	[_formDataFieldDict removeObjectForKey: fieldName];
}

- (void) clearAllFormDataFields
{
	[_formDataFieldDict removeAllObjects];
}

- (BOOL) setBodyData: (NSData *)bodyData
{
	if( [bodyData length] <= 0 ) {
		return NO;
	}
	
	[_request setHTTPBody: bodyData];
	
	return YES;
}

- (BOOL) didBind
{
	NSString		*baseDirectory;
	BOOL			isDirecotry;
	BOOL			fileCreated;
	NSString		*value;
	NSString		*urlStringWithQueries;
	NSURL			*url;
	
	[self resetTransfer];
	
	if( _notifyStatus == YES ) {
		[self pushNotifyStatusToMainThread: [NSDictionary dictionaryWithObjectsAndKeys:
											 [NSNumber numberWithUnsignedInteger:(NSUInteger)self.issuedId], AsyncHttpDelivererParameterKeyIssuedId,
											 _urlString, AsyncHttpDelivererParameterKeyUrlString,
											 [NSNumber numberWithInteger:(NSInteger)AsyncHttpDelivererStatusStart], AsyncHttpDelivererParameterKeyStatus,
											 nil]
		 ];
	}
	
	if( (value = [self stringForUrlEncodedFromDict: _queryStringFieldDict]) == nil ) {
		urlStringWithQueries = _urlString;
	} else {
		urlStringWithQueries = [NSString stringWithFormat: @"%@?%@", _urlString, value];
	}
	
	if( (url = [NSURL URLWithString: urlStringWithQueries]) == nil ) {
		url = [NSURL URLWithString: [urlStringWithQueries stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
	}
	[_request setURL: url];
	
	if( [_headerFieldDict count] > 0 ) {
		[_request setAllHTTPHeaderFields: _headerFieldDict];
	}
	
	if( ([[_request HTTPMethod] isEqualToString: @"GET"] == YES) ) {
		
		_fileHandle = nil;
		_connection = nil;
		fileCreated = NO;
		
		if( [_downloadFilePath length] > 0 ) {
			if( (baseDirectory = [_downloadFilePath stringByDeletingLastPathComponent]) != nil ) {
				if( [[NSFileManager defaultManager] fileExistsAtPath: baseDirectory isDirectory: &isDirecotry] == NO ) {
					[[NSFileManager defaultManager] createDirectoryAtPath: baseDirectory withIntermediateDirectories: YES attributes: nil error: nil];
				} else {
					if( isDirecotry == NO ) {
						[[NSFileManager defaultManager] removeItemAtPath: baseDirectory error: nil];
						[[NSFileManager defaultManager] createDirectoryAtPath: baseDirectory withIntermediateDirectories: YES attributes: nil error: nil];
					}
				}
				fileCreated = [[NSFileManager defaultManager] createFileAtPath: _downloadFilePath contents: nil attributes: nil];
			}
			_fileHandle = [NSFileHandle fileHandleForWritingAtPath: _downloadFilePath];
			if( _fileHandle != nil ) {
				_connection = [[NSURLConnection alloc] initWithRequest: _request delegate: self];
			}
		} else {
			_connection = [[NSURLConnection alloc] initWithRequest: _request delegate: self];
		}
		if( _connection == nil ) {
			[_closeQuery setParameter: @"Y" forKey: AsyncHttpDelivererParameterKeyFailed];
			if( fileCreated == YES ) {
				[[NSFileManager defaultManager] removeItemAtPath: _downloadFilePath error: nil];
			}
			return NO;
		}
		
		HYTRACE_BLOCK
		(
			if( _connection != nil ) {
				HYTRACE( @"- AsyncHttpDeliverer [%d] request start", _issuedId );
				HYTRACE( @"- url    [%@]", [_request URL] );
				HYTRACE( @"- method [%@]", [_request HTTPMethod] );
				for( NSString *key in [_request allHTTPHeaderFields] ) {
					HYTRACE( @"- header [%@][%@]", key, [[_request allHTTPHeaderFields] objectForKey: key] );
				}
				HYTRACE( @"- body    [%@]", [[NSString alloc] initWithData: [_request HTTPBody] encoding: NSUTF8StringEncoding] );
			}
		)
		
	} else if( ([[_request HTTPMethod] isEqualToString: @"POST"] == YES) || ([[_request HTTPMethod] isEqualToString: @"PUT"] == YES) ) {
		
		dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
			
			if( [self fillSendDataWithFormData] == NO ) {
				[self resetTransfer];
				[self doneWithError];
			}
			
			dispatch_async( dispatch_get_main_queue(), ^{
				
				if( [[_closeQuery parameterForKey: AsyncHttpDelivererParameterKeyFailed] boolValue] == NO ) {
					if( [self bindConnection] == NO ) {
						[self resetTransfer];
						[self doneWithError];
					}
				}
				
			});
			
		});
		
	} else { // ([[_request HTTPMethod] isEqualToString: @"DELETE"] == YES)
		
		if( (_connection = [[NSURLConnection alloc] initWithRequest: _request delegate: self]) == nil ) {
			[_closeQuery setParameter: @"Y" forKey: AsyncHttpDelivererParameterKeyFailed];
			return NO;
		}
		
	}
	
	return YES;
}

- (void) willDone
{
	[_closeQuery setParameter: [NSNumber numberWithUnsignedInteger:(NSUInteger)self.issuedId] forKey: AsyncHttpDelivererParameterKeyIssuedId];
}

- (void) willCancel
{
	[_closeQuery setParameter: [NSNumber numberWithUnsignedInteger:(NSUInteger)self.issuedId] forKey: AsyncHttpDelivererParameterKeyIssuedId];
	[_closeQuery setParameter: @"Y" forKey: AsyncHttpDelivererParameterKeyCanceled];
	[_closeQuery setParameter: [NSNumber numberWithUnsignedInt: [self passedMilisecondFromBind]] forKey: AsyncHttpDelivererParameterKeyWorkingTimeByMilisecond];
	
	if( _notifyStatus == YES ) {
		[self pushNotifyStatusToMainThread: [NSDictionary dictionaryWithObjectsAndKeys:
											 [NSNumber numberWithUnsignedInteger:(NSUInteger)self.issuedId], AsyncHttpDelivererParameterKeyIssuedId,
											 _urlString, AsyncHttpDelivererParameterKeyUrlString,
											 [NSNumber numberWithInteger:(NSInteger)AsyncHttpDelivererStatusCanceled], AsyncHttpDelivererParameterKeyStatus,
											 nil]
		 ];
	}
}

- (void) willUnbind
{
	[self resetTransfer];
}

- (NSString *) brief
{
	return @"AsyncHttpDeliverer";
}

- (NSString *) customDataDescription
{
	NSString	*description;
	
	description = [NSString stringWithFormat: @"<request url=\"%@\" method=\"%@\"/>", _request.URL, _request.HTTPMethod];
	if( [_limiterName length] > 0 ) {
		description = [description stringByAppendingFormat: @"<limiter name=\"%@\" count=\"%ld\"/>", _limiterName, (long)_limiterCount];
	}
	if( [_uploadFileName length] > 0 ) {
		description = [description stringByAppendingFormat: @"<upload_file_name=\"%@\"", _uploadFileName];
	}
	if( [_uploadFilePath length] > 0 ) {
		description = [description stringByAppendingFormat: @"<upload_file_path=\"%@\"", _uploadFilePath];
	}
	   
	return description;
}

- (NSURLRequestCachePolicy) cachePolicy
{
	return _request.cachePolicy;
}

- (void) setCachePolicy: (NSURLRequestCachePolicy)cachePolicy
{
	[_request setCachePolicy: cachePolicy];
}

- (NSTimeInterval) timeoutInterval
{
	return _request.timeoutInterval;
}

- (void) setTimeoutInterval: (NSTimeInterval)timeoutInterval
{
	[_request setTimeoutInterval: timeoutInterval];
}

- (NSString *) urlString
{
	return _urlString;
}

- (void) setUrlString: (NSString *)urlString
{
	_urlString = urlString;
}

- (NSString *) method
{
	return _request.HTTPMethod;
}

- (void) setMethod: (NSString *)method
{
	[_request setHTTPMethod: method];
}

- (NSInteger) transferBufferSize
{
	return _transferBufferSize;
}

- (void) setTransferBufferSize: (NSInteger)transferBufferSize
{
	if( transferBufferSize <= 0 ) {
		return;
	}
	
	_transferBufferSize = transferBufferSize;
}

#pragma mark -
#pragma mark NSStreamDelegate

- (void) stream: (NSStream *)theStream handleEvent: (NSStreamEvent)streamEvent
{
	NSUInteger		leftSize;
	NSInteger		writeBytes;
	NSNumber		*fileSizeNumber;
	NSData			*boundaryData;
	
	switch( streamEvent ) {
		case NSStreamEventOpenCompleted :
			if( _notifyStatus == YES ) {
				fileSizeNumber = (NSNumber *)[[[NSFileManager defaultManager] attributesOfItemAtPath: _uploadFilePath error: NULL] objectForKey: NSFileSize];
				[self pushNotifyStatusToMainThread: [NSDictionary dictionaryWithObjectsAndKeys:
													 [NSNumber numberWithUnsignedInteger:(NSUInteger)self.issuedId], AsyncHttpDelivererParameterKeyIssuedId,
													 _urlString, AsyncHttpDelivererParameterKeyUrlString,
													 [NSNumber numberWithInteger:(NSInteger)AsyncHttpDelivererstatusConnected], AsyncHttpDelivererParameterKeyStatus,
													 fileSizeNumber, AsyncHttpDelivererParameterKeyContentLength,
													 nil]
				 ];
			}
			break;
		case NSStreamEventHasSpaceAvailable :
			//usleep( 500000 );
			if( (leftSize = (_filledSize - _lookingIndex)) <= 0 ) {
				if( _fileStream != nil ) {
					if( (_filledSize = [_fileStream read:(uint8_t *)_buffer maxLength: _transferBufferSize]) == 0 ) {
						[_fileStream close];
						_fileStream = nil;
						boundaryData = [[NSString stringWithFormat: @"\r\n--%@--\r\n", _multipartBoundaryString] dataUsingEncoding: NSUTF8StringEncoding];
						_filledSize = [boundaryData length];
						memcpy( _buffer, [boundaryData bytes], _filledSize );
					}
					_lookingIndex = 0;
				}
			}
			if( (leftSize = (_filledSize - _lookingIndex)) > 0 ) {
				if( (writeBytes = [_producerStream write: (const uint8_t *)(_buffer+_lookingIndex) maxLength: leftSize]) > 0 ) {
					_lookingIndex += writeBytes;
				} else {
					[self resetTransfer];
					[self doneWithError];
				}
			} else {
				if( _fileStream == nil ) {
					if( _notifyStatus == YES ) {
						[self pushNotifyStatusToMainThread: [NSDictionary dictionaryWithObjectsAndKeys:
															 [NSNumber numberWithUnsignedInteger:(NSUInteger)self.issuedId], AsyncHttpDelivererParameterKeyIssuedId,
															 _urlString, AsyncHttpDelivererParameterKeyUrlString,
															 [NSNumber numberWithInteger:(NSInteger)AsyncHttpDelivererStatusDone], AsyncHttpDelivererParameterKeyStatus,
															 nil]
						 ];
					}
					[self closeProducerStream];
				}
			}
			break;
		case NSStreamEventErrorOccurred :
			[self resetTransfer];
			[self doneWithError];
			break;
		default :
			break;
	}
}

#pragma mark -
#pragma mark NSURLConnection methods

- (void) connection: (NSURLConnection *)connection didReceiveResponse: (NSURLResponse *)response
{
	_response = response;
	
	HYTRACE( @"- AsyncHttpDeliverer [%d] http status code [%ld]", _issuedId, (long)((NSHTTPURLResponse *)response).statusCode );
	
	if( _notifyStatus == YES ) {
		[self pushNotifyStatusToMainThread: [NSDictionary dictionaryWithObjectsAndKeys:
											 [NSNumber numberWithUnsignedInteger:(NSUInteger)self.issuedId], AsyncHttpDelivererParameterKeyIssuedId,
											 _urlString, AsyncHttpDelivererParameterKeyUrlString,
											 [NSNumber numberWithInteger:(NSInteger)AsyncHttpDelivererstatusConnected], AsyncHttpDelivererParameterKeyStatus,
											 [NSNumber numberWithLongLong: [response expectedContentLength]], AsyncHttpDelivererParameterKeyContentLength,
											 nil]
		 ];
	}
}

- (BOOL) connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace: (NSURLProtectionSpace *)protectionSpace
{
	return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void) connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge: (NSURLAuthenticationChallenge *)challenge
{
	if( [challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust] == YES ) {
		if( [_trustedHosts containsObject:challenge.protectionSpace.host] == YES ) {
			[challenge.sender useCredential: [NSURLCredential credentialForTrust: challenge.protectionSpace.serverTrust] forAuthenticationChallenge: challenge];
		}
	}
	
	[challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

- (void) connection:( NSURLConnection *)connection didReceiveData: (NSData *)data
{
	NSUInteger		transferLength;
	
	if( _fileHandle != nil ) {
		[_fileHandle writeData: data];
		transferLength = (NSUInteger)[_fileHandle offsetInFile];
	} else {
		if( _receivedData == nil ) {
			_receivedData = [[NSMutableData alloc] init];
		}
		[_receivedData appendData: data];
		transferLength = [_receivedData length];
	}
	
	if( _notifyStatus == YES ) {
		[self pushNotifyStatusToMainThread: [NSDictionary dictionaryWithObjectsAndKeys:
											 [NSNumber numberWithUnsignedInteger:(NSUInteger)self.issuedId], AsyncHttpDelivererParameterKeyIssuedId,
											 _urlString, AsyncHttpDelivererParameterKeyUrlString,
											 [NSNumber numberWithInteger:(NSInteger)AsyncHttpDelivererStatusTransfering], AsyncHttpDelivererParameterKeyStatus,
											 [NSNumber numberWithLongLong: transferLength], AsyncHttpDelivererParameterKeyAmountTransferedLength,
											 [NSNumber numberWithLongLong: [data length]], AsyncHttpDelivererParameterKeyCurrentTransferedLength,
											 nil]
		 ];
	}
}

- (void) connection: (NSURLConnection *)connection didSendBodyData: (NSInteger)bytesWritten totalBytesWritten: (NSInteger)totalBytesWritten totalBytesExpectedToWrite: (NSInteger)totalBytesExpectedToWrite
{	
	if( _notifyStatus == YES ) {
		[self pushNotifyStatusToMainThread: [NSDictionary dictionaryWithObjectsAndKeys:
											 [NSNumber numberWithUnsignedInteger:(NSUInteger)self.issuedId], AsyncHttpDelivererParameterKeyIssuedId,
											 _urlString, AsyncHttpDelivererParameterKeyUrlString,
											 [NSNumber numberWithInteger:(NSInteger)AsyncHttpDelivererStatusTransfering], AsyncHttpDelivererParameterKeyStatus,
											 _lastUploadContentLengthNumber, AsyncHttpDelivererParameterKeyContentLength,
											 [NSNumber numberWithInteger:totalBytesWritten], AsyncHttpDelivererParameterKeyAmountTransferedLength,
                                             [NSNumber numberWithInteger:totalBytesExpectedToWrite], AsyncHttpDelivererParameterKeyExpectedTransferedLength,
											 [NSNumber numberWithInteger:bytesWritten], AsyncHttpDelivererParameterKeyCurrentTransferedLength,
											 nil]
		 ];
	}
}

- (void) connection: (NSURLConnection *)connection didFailWithError: (NSError *)error
{
	[_closeQuery setParameter: [NSNumber numberWithUnsignedInt: [self passedMilisecondFromBind]] forKey: AsyncHttpDelivererParameterKeyWorkingTimeByMilisecond];
	if( _response != nil ) {
		[_closeQuery setParameter: _response forKey: AsyncHttpDelivererParameterKeyResponse];
	}
	
	HYTRACE_BLOCK
	(
		HYTRACE( @"- AsyncHttpDeliverer [%d] request failed", _issuedId );
		HYTRACE( @"- url    [%@]", [_request URL] );
		HYTRACE( @"- method [%@]", [_request HTTPMethod] );
		for( NSString *key in [_request allHTTPHeaderFields] ) {
			HYTRACE( @"- header [%@][%@]", key, [[_request allHTTPHeaderFields] objectForKey: key] );
		}
		HYTRACE( @"- body    [%@]", [[NSString alloc] initWithData: [_request HTTPBody] encoding: NSUTF8StringEncoding] );
	)
	
	[self resetTransfer];
	[self doneWithError];
	
	if( [_downloadFilePath length] > 0 ) {
		[[NSFileManager defaultManager] removeItemAtPath: _downloadFilePath error: nil];
		_downloadFilePath = nil;
	}
}

- (void) connectionDidFinishLoading: (NSURLConnection *)connection
{
	[_closeQuery setParameter: [NSNumber numberWithUnsignedInt: [self passedMilisecondFromBind]] forKey: AsyncHttpDelivererParameterKeyWorkingTimeByMilisecond];
	if( _response != nil ) {
		[_closeQuery setParameter: _response forKey: AsyncHttpDelivererParameterKeyResponse];
	}
	
	HYTRACE( @"- AsyncHttpDeliverer [%d] request end", _issuedId );
	
	if( _receivedData != nil ) {
		[_closeQuery setParameter: _receivedData forKey: AsyncHttpDelivererParameterKeyBody];
	}
	
	HYTRACE_BLOCK
	(
		if( _receivedData != nil ) {
			HYTRACE( @"- receivced data : [%@]", [[NSString alloc] initWithData: _receivedData encoding: NSUTF8StringEncoding] );
		} else {
			if( ([_downloadFilePath length] > 0) && (_fileHandle != nil) ) {
				HYTRACE( @"- received data : [%lld]", [_fileHandle offsetInFile] );
			} else {
				HYTRACE( @"- receivced data : EMPTY" );
			}
		}
	)
	
	if( _notifyStatus == YES ) {
		[self pushNotifyStatusToMainThread: [NSDictionary dictionaryWithObjectsAndKeys:
											 [NSNumber numberWithUnsignedInteger:(NSUInteger)self.issuedId], AsyncHttpDelivererParameterKeyIssuedId,
											 _urlString, AsyncHttpDelivererParameterKeyUrlString,
											 [NSNumber numberWithInteger:(NSInteger)AsyncHttpDelivererStatusDone], AsyncHttpDelivererParameterKeyStatus,
											 nil]
		 ];
	}
	
	[self resetTransfer];
	[self done];
}

@end
