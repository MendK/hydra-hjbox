//
//  CoreLocationManager.m
//  HJBox
//
//  Created by Na Tae Hyun on 13. 11. 4..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import "CoreLocationManager.h"


CoreLocationManager	*g_defaultCoreLocationManager;


@interface CoreLocationManager( CoreLocationManagerPrivate )

- (void) postNotifyWithStatus: (CoreLocationManagerStatus)status;
- (void) postNotifyWithParamDict: (NSDictionary *)paramDict;

@end


@implementation CoreLocationManager

@synthesize status = _status;
@synthesize currentLocation = _currentLocation;
@dynamic currentLatitude;
@dynamic currentLogitude;
@synthesize manager = _manager;
@synthesize authorizatinoStatus = _authorizatinoStatus;
@dynamic desiredAccuracy;

- (id) init
{
	if( (self = [super init]) != nil ) {
		
		_status = CoreLocationManagerStatusIdle;
		
		if( (_manager = [[CLLocationManager alloc] init]) == nil ) {
			return nil;
		}
		_manager.desiredAccuracy = kCLLocationAccuracyBest;
		_manager.delegate = self;
		
		_authorizationStatus = [CLLocationManager authorizationStatus];
		
		if( (_lock = [[NSLock alloc] init]) == nil ) {
			return nil;
		}
		
	}
	
	return self;
}

+ (CoreLocationManager *) defaultManager
{
	@synchronized( self ) {
		if( g_defaultCoreLocationManager == nil ) {
			g_defaultCoreLocationManager = [[CoreLocationManager alloc] init];
		}
	}
	
	return g_defaultCoreLocationManager;
}

- (BOOL) updateCurrentLocation
{
	BOOL		inLocating;
	
	[_lock lock];
	
	inLocating = (_status == CoreLocationManagerStatusLocating);
	if( inLocating == NO ) {
		_status = CoreLocationManagerStatusLocating;
	}
	
	[_lock unlock];
	
	if( inLocating == YES ) {
		return YES;
	}
	
	[self postNotifyWithStatus: CoreLocationManagerStatusLocating];
	
	if( [[NSBundle mainBundle] objectForInfoDictionaryKey: @"NSLocationAlwaysUsageDescription"] != nil ) {
		if ([_manager respondsToSelector: @selector(requestAlwaysAuthorization)] == YES ) {
			[_manager requestAlwaysAuthorization];
		}
	} else {
		if ([_manager respondsToSelector: @selector(requestWhenInUseAuthorization)] == YES ) {
			[_manager requestWhenInUseAuthorization];
		}
	}
	[_manager startUpdatingLocation];
	
	return YES;
}

- (double) currentLatitude
{
	if( _currentLocation == nil ) {
		return 0.0f;
	}
	
	return _currentLocation.coordinate.latitude;
}

- (double) currentLogitude
{
	if( _currentLocation == nil ) {
		return 0.0f;
	}
	
	return _currentLocation.coordinate.longitude;
}

- (CLLocationAccuracy) desiredAccuracy
{
	return _manager.desiredAccuracy;
}

- (void) setDesiredAccuracy: (CLLocationAccuracy)desiredAccuracy
{
	_manager.desiredAccuracy = desiredAccuracy;
}

- (void) postNotifyWithStatus: (CoreLocationManagerStatus)status
{
	[self performSelectorOnMainThread: @selector(postNotifyWithParamDict:) withObject: @{ CoreLocationManagerNotifyParameterKeyStatus:[NSNumber numberWithInteger: (NSInteger)status], CoreLocatoinManagerNotifyParameterKeyAuthorizationStatus:[NSNumber numberWithInteger: (NSInteger)_authorizationStatus] } waitUntilDone: NO];
}

- (void) postNotifyWithParamDict: (NSDictionary *)paramDict
{
	[[NSNotificationCenter defaultCenter] postNotificationName: CoreLocationManagerNotification object: self userInfo: paramDict];
}

- (void) locationManager: (CLLocationManager *)manager didUpdateToLocation: (CLLocation *)newLocation fromLocation: (CLLocation *)oldLocation
{
	_currentLocation = newLocation;
	
	[_manager stopUpdatingLocation];
	
	[_lock lock];
	_status = CoreLocationManagerStatusIdle;
	[_lock unlock];
	
	if( _currentLocation == nil ) {
		[self postNotifyWithStatus: CoreLocationManagerStatusUnknownError];
	} else {
		[self postNotifyWithStatus: CoreLocationManagerStatusLocateUpdated];
	}
}

- (void) locationManager: (CLLocationManager *)manager didFailWithError: (NSError *)error
{
	[_lock lock];
	_status = CoreLocationManagerStatusIdle;
	[_lock unlock];
	
	[self postNotifyWithStatus: CoreLocationManagerStatusAccessDenied];
}

- (void) locationManager: (CLLocationManager *)manager didChangeAuthorizationStatus: (CLAuthorizationStatus)status
{
	_authorizationStatus = status;
}

@end
