//
//  CoreLocationManager.h
//  HJBox
//
//  Created by Na Tae Hyun on 13. 11. 4..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>


#define		CoreLocationManagerNotification								@"coreLocationManagerNotification"

#define		CoreLocationManagerNotifyParameterKeyStatus					@"coreLocationManagerNotifyParameterKeyStatus"
#define		CoreLocatoinManagerNotifyParameterKeyAuthorizationStatus	@"coreLocationManagerNotifyParameterKeyAuthorizationStatus"


typedef enum _CoreLocationManagerStatus_
{
	CoreLocationManagerStatusDummy,
	CoreLocationManagerStatusIdle,
	CoreLocationManagerStatusLocating,
	CoreLocationManagerStatusLocateUpdated,
	CoreLocationManagerStatusAccessDenied,
	CoreLocationManagerStatusUnknownError,
	kCountOfCoreLocationManagerStatus
	
} CoreLocationManagerStatus;


@interface CoreLocationManager : NSObject <CLLocationManagerDelegate>
{
	CoreLocationManagerStatus			_status;
	CLLocationManager					*_manager;
	CLAuthorizationStatus				_authorizationStatus;
	CLLocation							*_currentLocation;
	NSLock								*_lock;
}

+ (CoreLocationManager *) defaultManager;

- (BOOL) updateCurrentLocation;

@property (nonatomic, readonly) CoreLocationManagerStatus status;
@property (nonatomic, readonly) CLLocation *currentLocation;
@property (nonatomic, readonly) double currentLatitude;
@property (nonatomic, readonly) double currentLogitude;
@property (nonatomic, readonly) CLLocationManager *manager;
@property (nonatomic, readonly) CLAuthorizationStatus authorizatinoStatus;
@property (nonatomic, assign) CLLocationAccuracy desiredAccuracy;

@end
