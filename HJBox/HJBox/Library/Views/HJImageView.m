//
//  HJImageView.m
//	HJBox
//
//  Created by Na Tae Hyun on 13. 11. 5..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import "AsyncHttpDeliverer.h"
#import "ResourceManager.h"
#import "HJImageView.h"


@interface HJImageView( ImageViewPrivate )

- (void) asyncHttpDelivererReport: (NSNotification *)notification;
- (void) resourceManagerReport: (NSNotification *)notification;

@end


@implementation HJImageView

@dynamic image;
@synthesize asyncDelivererIssuedId = _asyncDelivererIssuedId;

- (id) initWithFrame: (CGRect)frame
{
	if( (self = [super initWithFrame: frame]) != nil ) {
		
		self.backgroundColor = [UIColor clearColor];
		
		if( (_imageView = [[UIImageView alloc] init]) == nil ) {
			return nil;
		}
		
		[self addSubview: _imageView];
		
		[[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(resourceManagerReport:) name: ResourceManagerNotification object: nil];
		
	}
	
	return self;
}

- (void) dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver: self];
}

- (void) layoutSubviews
{
	_imageView.frame = self.bounds;
	
	if( (_progressViewAsDisplayView == YES) && ([_progressView isKindOfClass: [UIView class]] == YES) ) {
		[_progressView setFrame: self.bounds];
	}
}

- (void) asyncHttpDelivererReport: (NSNotification *)notification
{
	NSDictionary				*userInfo;
	AsyncHttpDelivererStatus	status;
	NSUInteger					issuedId;
	long long					tranferLength;
	double						rate;
	
	if( (_asyncDelivererIssuedId <= 0) || (_progressView == nil) ) {
		return;
	}
	
	userInfo = [notification userInfo];
	
	status = (AsyncHttpDelivererStatus)[[userInfo objectForKey: AsyncHttpDelivererParameterKeyStatus] integerValue];
	issuedId = (NSUInteger)[[userInfo objectForKey: AsyncHttpDelivererParameterKeyIssuedId] unsignedIntegerValue];
	
	if( _asyncDelivererIssuedId != issuedId ) {
		return;
	}
	
	switch( status ) {
		case AsyncHttpDelivererstatusConnected :
			_contentLength = (long long)[[userInfo objectForKey: AsyncHttpDelivererParameterKeyContentLength] longLongValue];
			if( [_progressView respondsToSelector: @selector(hjImageViewConnected:contentLength:)] == YES ) {
				[_progressView hjImageViewConnected: self contentLength: _contentLength];
			}
			break;
		case AsyncHttpDelivererStatusTransfering :
			tranferLength = (long long)[[userInfo objectForKey: AsyncHttpDelivererParameterKeyAmountTransferedLength] longLongValue];
			if( _contentLength <= 0 ) {
				_contentLength = (long long)[[userInfo objectForKey: AsyncHttpDelivererParameterKeyContentLength] longLongValue];
			}
			if( _contentLength <= 0 ) {
				rate = 0.0f;
			} else {
				rate = (double)tranferLength/(double)_contentLength;
			}
			if( [_progressView respondsToSelector: @selector(hjImageViewTransfering:rate:)] == YES ) {
				[_progressView hjImageViewTransfering: self rate: rate];
			}
			break;
		case AsyncHttpDelivererStatusDone :
			if( [_progressView respondsToSelector: @selector(hjImageViewDone:)] == YES ) {
				[_progressView hjImageViewDone: self];
			}
			break;
		case AsyncHttpDelivererStatusFailed :
			if( [_progressView respondsToSelector: @selector(hjImageViewFailed:)] == YES ) {
				[_progressView hjImageViewFailed: self];
			}
			break;
		case AsyncHttpDelivererStatusCanceled :
			if( [_progressView respondsToSelector: @selector(hjImageViewCanceled:)] == YES ) {
				[_progressView hjImageViewCanceled: self];
			}
			break;
		default :
			break;
	}
}

- (void) resourceManagerReport: (NSNotification *)notification
{
	NSDictionary					*userInfo;
	ResourceManagerRequestStatus	status;
	NSString						*urlString;
	NSString						*remakerName;
	id								remakerParameter;
	NSString						*identifierForRemakerParameter;
	ResourceManagerDataType			dataType;
	UIImage							*image;
	
	if( [_imageUrlString length] <= 0 ) {
		return;
	}
	
	userInfo = [notification userInfo];
	
	urlString = [userInfo objectForKey: ResourceManagerParameterKeyUrlString];
	if( [urlString isEqualToString: _imageUrlString] == NO ) {
		return;
	}
	remakerName = [userInfo objectForKey: ResourceManagerParameterKeyRemakerName];
	if( _remakerName != nil ) {
		if( [remakerName isEqualToString: _remakerName] == NO ) {
			return;
		}
	} else {
		if( [remakerName isEqualToString: ResourceManagerOriginalRemakerName] == NO ) {
			return;
		}
	}
	remakerParameter = [userInfo objectForKey: ResourceManagerParameterKeyRemakerParameter];
	if( _identifierForRemakerParameter != nil ) {
		identifierForRemakerParameter = [[ResourceManager defaultManager] remakeIdentifierWithParameter: remakerParameter forName: remakerName];
		if( [identifierForRemakerParameter isEqualToString: _identifierForRemakerParameter] == NO ) {
			return;
		}
	} else {
		if( remakerParameter != nil ) {
			return;
		}
	}
	
	status = (ResourceManagerRequestStatus)[[userInfo objectForKey: ResourceManagerParameterKeyStatus] integerValue];
	
	switch( status ) {
		case ResourceManagerRequestStatusDownloadingStart :
			_asyncDelivererIssuedId = (NSUInteger)[[userInfo objectForKey: ResourceManagerParameterKeyAsyncHttpDelivererIssuedId] unsignedIntegerValue];
			break;
		case ResourceManagerRequestStatusDownloadingFailed :
			break;
		case ResourceManagerRequestStatusDataPrepared :
			dataType = (ResourceManagerDataType)[[userInfo objectForKey: ResourceManagerParameterKeyDataType] integerValue];
			switch( dataType ) {
				case ResourceManagerDataTypeImage :
					if( (image = [userInfo objectForKey: ResourceManagerParameterKeyDataObject]) != nil ) {
						self.image = image;
					}
					break;
				default :
					break;
			}
			break;
		case ResourceManagerRequestStatusRequestSkipped :
			break;
		case ResourceManagerRequestStatusRetrySkipped :
			break;
		case ResourceManagerRequestStatusRequestCanceled :
			break;
		default :
			break;
	}
}

- (BOOL) setImageUrl: (NSString *)urlString remakerName: (NSString *)remakerName remakerParameter: (id)remakerParameter
{
	UIImage		*image;
	
	if( [urlString length] <= 0 ) {
		return NO;
	}
	
	if( [remakerName length] > 0 ) {
		if( (_identifierForRemakerParameter = [[ResourceManager defaultManager] remakeIdentifierWithParameter: remakerParameter forName: remakerName]) == nil ) {
			return NO;
		}
		_remakerParameter = remakerParameter;
	} else {
		_identifierForRemakerParameter = nil;
		_remakerParameter = nil;
	}
	
	_imageUrlString = urlString;
	_remakerName = remakerName;
	
	if( (image = [[ResourceManager defaultManager] imageFromUrlString: _imageUrlString remakerName: _remakerName remakerParameter: remakerParameter cryptModuleName: nil ifHaveDataWaitUntilDone: YES]) != nil ) {
		self.image = image;
	}
	
	return YES;
}

- (BOOL) setProgressView: (id)progressView asDisplayView: (BOOL)asDisplayView
{
	if( [progressView conformsToProtocol: @protocol(HJImageViewProgressProtocol)] == NO ) {
		return NO;
	}
	
	if( _progressView == nil ) {
		[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(asyncHttpDelivererReport:) name: AsyncHttpDelivererNotification object: nil];
	} else {
		if( (_progressViewAsDisplayView == YES) && ([_progressView isKindOfClass: [UIView class]] == YES) ) {
			[_progressView removeFromSuperview];
		}
		_progressView = nil;
	}
	
	_progressView = progressView;
	_progressViewAsDisplayView = asDisplayView;
	
	if( (_progressViewAsDisplayView == YES) && ([_progressView isKindOfClass: [UIView class]] == YES) ) {
		[self addSubview: _progressView];
		[self setNeedsLayout];
	}

	return YES;
}

- (void) removeProgressView
{
	if( _progressView == nil ) {
		return;
	}
	
	[[NSNotificationCenter defaultCenter] removeObserver: self name: AsyncHttpDelivererNotification object: nil];
	
	if( (_progressViewAsDisplayView == YES) && ([_progressView isKindOfClass: [UIView class]] == YES) ) {
		[_progressView removeFromSuperview];
	}
	_progressView = nil;
}

- (UIImage *) image
{
	return _imageView.image;
}

- (void) setImage: (UIImage *)image
{
	BOOL		needSizeToFits;
	
	needSizeToFits = NO;
	
	if( image != nil ) {
		if( _imageView.image == nil ) {
			needSizeToFits = YES;
		} else {
			if( (_imageView.image.size.width != image.size.width) || (_imageView.image.size.height != image.size.height) ) {
				needSizeToFits = YES;
			}
		}
	}

	_imageView.image = image;
	
	_imageUrlString = nil;
	_remakerName = nil;
	_identifierForRemakerParameter = nil;
	
	if( needSizeToFits == YES ) {
		[_imageView sizeThatFits: image.size];
	}
}

@end
