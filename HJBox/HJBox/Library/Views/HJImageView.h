//
//  HJImageView.h
//  HJBox
//
//  Created by Na Tae Hyun on 13. 11. 5..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import <UIKit/UIKit.h>


@interface HJImageView : UIView
{
	NSString			*_imageUrlString;
	NSString			*_remakerName;
	id					_remakerParameter;
	NSString			*_identifierForRemakerParameter;
	UIImageView			*_imageView;
	NSUInteger			_asyncDelivererIssuedId;
	unsigned long long	_contentLength;
	id					_progressView;
	BOOL				_progressViewAsDisplayView;
}

- (BOOL) setImageUrl: (NSString *)urlString remakerName: (NSString *)remakerName remakerParameter: (id)remakerParameter;
- (BOOL) setProgressView: (id)progressView asDisplayView: (BOOL)asDisplayView;
- (void) removeProgressView;

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, assign) NSUInteger asyncDelivererIssuedId;

@end


@protocol HJImageViewProgressProtocol

@optional
- (void) hjImageViewConnected: (HJImageView *)hjImageView contentLength: (unsigned long long)contentLength;
- (void) hjImageViewTransfering: (HJImageView *)hjImageView rate: (double)rate;
- (void) hjImageViewDone: (HJImageView *)hjImageView;
- (void) hjImageViewFailed: (HJImageView *)hjImageView;
- (void) hjImageViewCanceled: (HJImageView *)hjImageView;

@end