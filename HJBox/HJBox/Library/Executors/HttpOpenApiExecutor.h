//
//  HttpOpenApiExecutor.h
//	HJBox
//
//  Created by Na Tae Hyun on 13. 10. 24..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import <UIKit/UIKit.h>
#import <Hydra/Hydra.h>
#import <HJBox/AsyncHttpDeliverer.h>


#define		HttpOpenApiExecutorName									@"httpOpenApiExecutorName"

#define		HttpOpenApiExecutorParameterKeyStatus						@"httpOpenApiExecutorParameterKeyStatus"
#define		HttpOpenApiExecutorParameterKeyBodyStream					@"httpOpenApiExecutorParameterKeyBodyStream"
#define		HttpOpenApiExecutorParameterKeyCode							@"httpOpenApiExecutorParameterKeyCode"
#define		HttpOpenApiExecutorParameterKeyMessage						@"httpOpenApiExecutorParameterKeyMessage"
#define		HttpOpenApiExecutorParameterKeyEmailAddress					@"httpOpenApiExecutorParameterKeyEmailAddress"
#define		HttpOpenApiExecutorParameterKeyPassword						@"httpOpenApiExecutorParameterKeyPassword"
#define		HttpOpenApiExecutorParameterKeyMemberId						@"httpOpenApiExecutorParameterKeyMemberId"
#define		HttpOpenApiExecutorParameterKeyCloseQueryCall				@"httpOpenApiExecutorParameterKeyCloseQueryCall"
#define		HttpOpenApiExecutorParameterKeyDelivererIssuedId			@"httpOpenApiExecutorParameterKeyDelivererIssuedId"

typedef enum _HttpOpenApiExecutorStatus_
{
	HttpOpenApiExecutorStatusDummy,
	HttpOpenApiExecutorStatusRequested,
	HttpOpenApiExecutorStatusReceived,
	HttpOpenApiExecutorStatusCanceled,
	HttpOpenApiExecutorStatusExpired,
	HttpOpenApiExecutorStatusInvalidParameter,
	HttpOpenApiExecutorStatusInternalError,
	HttpOpenApiExecutorStatusNetworkError,
	HttpOpenApiExecutorStatusFailedResponse,
	HttpOpenApiExecutorStatusDataParsingError,
	HttpOpenApiExecutorStatusEmptyData,
	kCountOfHttpOpenApiExecutorStatus
	
} HttpOpenApiExecutorStatus;

typedef enum _HttpOpenApiExecutorHttpMethodType_
{
	HttpOpenApiExecutorHttpMethodTypeGet,
	HttpOpenApiExecutorHttpMethodTypePost,
	kCountOfHttpOpenApiExecutorHttpMethodType

} HttpOpenApiExecutorHttpMethodType;

typedef enum _HttpOpenApiExecutorReceiveBodyType_
{
	HttpOpenApiExecutorReceiveBodyTypeStream,
	HttpOpenApiExecutorReceiveBodyTypeJson,
	kCountOfHttpOpenApiExecutorReceiveBodyType
	
} HttpOpenApiExecutorReceiveBodyType;


@interface HttpOpenApiExecutor : HYExecuter

// you must override and implement these methods.

- (NSString *) apiUrlFromQuery: (id)anQuery;

// override these methods if need.

- (BOOL) isValidParameterForQuery: (id)anQuery;
- (NSDictionary *) apiParameterFromQuery: (id)anQuery;
- (HttpOpenApiExecutorHttpMethodType) httpMethodType: (id)anQuery;
- (AsyncHttpDelivererPostContentType) postContentTypeFromQuery: (id)anQuery;
- (HttpOpenApiExecutorReceiveBodyType) receiveBodyTypeFromQuery: (id)anQuery;
- (BOOL) customSetupWithDeliverer: (AsyncHttpDeliverer *)deliverer fromQuery: (id)anQuery;
- (BOOL) additionalSetupWithDeliverer: (AsyncHttpDeliverer *)deliverer fromQuery: (id)anQuery;
- (BOOL) appendResultParameterToQuery: (id)anQuery withParsedObject: (id)parsedObject;
- (NSTimeInterval) timeoutIntervalFromQuery: (id)anQuery;
- (NSArray *) trustedHosts;

@end
