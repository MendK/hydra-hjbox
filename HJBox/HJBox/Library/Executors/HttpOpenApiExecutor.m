//
//  HttpOpenApiExecutor.h
//	HJBox
//
//  Created by Na Tae Hyun on 13. 10. 24..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import "HttpOpenApiExecutor.h"


@interface HttpOpenApiExecutor( HttpOpenApiExecutorPrivate )

- (HYResult *) resultForQuery: (id)anQuery withStatus: (HttpOpenApiExecutorStatus)status;

@end


@implementation HttpOpenApiExecutor

- (NSString *) name
{
	return HttpOpenApiExecutorName;
}

- (NSString *) brief
{
	return @"Http executor for communicate open api service.";
}

- (BOOL) calledExecutingWithQuery: (id)anQuery
{
	HYQuery				*closeQuery;
	AsyncHttpDeliverer	*deliverer;
	NSMutableData		*receivedData;
	id					parsedObject;
	NSError				*error;
	NSString			*apiUrlString;
	
	if( [[anQuery parameterForKey: HttpOpenApiExecutorParameterKeyCloseQueryCall] boolValue] == YES ) {
		
		if( [[anQuery parameterForKey: AsyncHttpDelivererParameterKeyFailed] boolValue] == YES ) {
			[self storeResult: [self resultForQuery: anQuery withStatus: HttpOpenApiExecutorStatusNetworkError]];
			return YES;
		}
		
		receivedData = [anQuery parameterForKey: AsyncHttpDelivererParameterKeyBody];
		
		if( [receivedData length] > 0 ) {
			switch( [self receiveBodyTypeFromQuery: anQuery] ) {
				case HttpOpenApiExecutorReceiveBodyTypeJson :
					if( (parsedObject = [NSJSONSerialization JSONObjectWithData: receivedData options: NSJSONReadingMutableContainers error: &error]) != nil ) {
						if( [self appendResultParameterToQuery: anQuery withParsedObject:parsedObject] == YES ) {
							[self storeResult: [self resultForQuery: anQuery withStatus: HttpOpenApiExecutorStatusReceived]];
						} else {
							[self storeResult: [self resultForQuery: anQuery withStatus: HttpOpenApiExecutorStatusDataParsingError]];
						}
					} else {
						[self storeResult: [self resultForQuery: anQuery withStatus: HttpOpenApiExecutorStatusDataParsingError]];
					}
					break;
				default :
					[anQuery setParameter: receivedData forKey: HttpOpenApiExecutorParameterKeyBodyStream];
					[self storeResult: [self resultForQuery: anQuery withStatus: HttpOpenApiExecutorStatusReceived]];
					break;
			}
		} else {
			[self storeResult: [self resultForQuery: anQuery withStatus: HttpOpenApiExecutorStatusEmptyData]];
		}
		
	} else {
		
		apiUrlString = [self apiUrlFromQuery: anQuery];
		if( [apiUrlString length] <= 0 ) {
			[self storeResult: [self resultForQuery: anQuery withStatus: HttpOpenApiExecutorStatusInvalidParameter]];
			return YES;
		}
		
		if( [self isValidParameterForQuery: anQuery] == NO ) {
			[self storeResult: [self resultForQuery: anQuery withStatus: HttpOpenApiExecutorStatusInvalidParameter]];
			return YES;
		}
		
		if( (closeQuery = [HYQuery queryWithWorkerName: [self.employedWorker name] executerName: self.name]) == nil ) {
			[self storeResult: [self resultForQuery: anQuery withStatus: HttpOpenApiExecutorStatusInternalError]];
			return YES;
		}
		
		[closeQuery setParametersFromDictionary: [anQuery paramDict]];
		[closeQuery setParameter: @"Y" forKey: HttpOpenApiExecutorParameterKeyCloseQueryCall];
		
		if( (deliverer = [[AsyncHttpDeliverer alloc] initWithCloseQuery: closeQuery]) == nil ) {
			[self storeResult: [self resultForQuery: anQuery withStatus: HttpOpenApiExecutorStatusInternalError]];
			return YES;
		}
		deliverer.trustedHosts = [self trustedHosts];
		
		[anQuery setParameter: [NSNumber numberWithUnsignedInteger: (NSUInteger)deliverer.issuedId] forKey: HttpOpenApiExecutorParameterKeyDelivererIssuedId];
		[closeQuery setParameter: [NSNumber numberWithUnsignedInteger: (NSUInteger)deliverer.issuedId] forKey: HttpOpenApiExecutorParameterKeyDelivererIssuedId];
		
		if( [self customSetupWithDeliverer: deliverer fromQuery: anQuery] == NO ) {
			switch( [self httpMethodType: anQuery] ) {
				case HttpOpenApiExecutorHttpMethodTypeGet :
					[deliverer setGetWithUrlString: apiUrlString queryStringDict: [self apiParameterFromQuery: anQuery]];
					break;
				case HttpOpenApiExecutorHttpMethodTypePost :
					[deliverer setPostWithUrlString: apiUrlString formDataDict: [self apiParameterFromQuery: anQuery] contentType: [self postContentTypeFromQuery: anQuery]];
					break;
				default :
					[self storeResult: [self resultForQuery: anQuery withStatus: HttpOpenApiExecutorStatusInternalError]];
					return YES;
			}
			if( [self additionalSetupWithDeliverer: deliverer fromQuery: anQuery] == NO ) {
				[self storeResult: [self resultForQuery: anQuery withStatus: HttpOpenApiExecutorStatusInternalError]];
				return YES;
			}
		}
		[deliverer setTimeoutInterval: [self timeoutIntervalFromQuery: anQuery]];
		[self bindAsyncTask: deliverer];
		
		[self storeResult: [self resultForQuery: anQuery withStatus: HttpOpenApiExecutorStatusRequested]];
		
	}
	
	return YES;
}

- (BOOL) calledCancelingWithQuery: (id)anQuery
{
	[self storeResult: [self resultForQuery: anQuery withStatus: HttpOpenApiExecutorStatusCanceled]];
	
	return YES;
}

- (id) resultForExpiredQuery: (id)anQuery
{
	return [self resultForQuery: anQuery withStatus: HttpOpenApiExecutorStatusExpired];
}

- (HYResult *) resultForQuery: (id)anQuery withStatus: (HttpOpenApiExecutorStatus)status
{
	HYResult	*result;
	
	if( (result = [HYResult resultWithName: self.name]) != nil ) {
		[result setParametersFromDictionary: [anQuery paramDict]];
		[result setParameter: [NSNumber numberWithInteger: status] forKey: HttpOpenApiExecutorParameterKeyStatus];
	}
	
	return result;
}

- (NSString *) apiUrlFromQuery: (id)anQuery
{
	return nil;
}

- (BOOL) isValidParameterForQuery: (id)anQuery
{
	return YES;
}

- (NSDictionary *) apiParameterFromQuery: (id)anQuery
{
	return nil;
}

- (HttpOpenApiExecutorHttpMethodType) httpMethodType: (id)anQuery
{
	return HttpOpenApiExecutorHttpMethodTypeGet;
}

- (AsyncHttpDelivererPostContentType) postContentTypeFromQuery: (id)anQuery
{
	return AsyncHttpDelivererPostContentTypeUrlEncoded;
}

- (HttpOpenApiExecutorReceiveBodyType) receiveBodyTypeFromQuery: (id)anQuery
{
	return HttpOpenApiExecutorReceiveBodyTypeJson;
}

- (BOOL) customSetupWithDeliverer: (AsyncHttpDeliverer *)deliverer fromQuery: (id)anQuery
{
	return NO;
}

- (BOOL) additionalSetupWithDeliverer: (AsyncHttpDeliverer *)deliverer fromQuery: (id)anQuery
{
	return YES;
}

- (BOOL) appendResultParameterToQuery: (id)anQuery withParsedObject: (id)parsedObject
{
	return YES;
}

- (NSTimeInterval) timeoutIntervalFromQuery: (id)anQuery
{
	return 8.0f;
}

- (NSArray *) trustedHosts
{
	return nil;
}

@end
