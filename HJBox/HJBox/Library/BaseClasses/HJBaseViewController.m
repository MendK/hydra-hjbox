//
//  HJBaseViewController.m
//  HJBox
//
//  Created by Na Tae Hyun on 13. 10. 24..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import "HJBaseViewController.h"


@interface HJBaseViewController (HJBaseViewControllerPrivate)

- (void)automaticSelectorSwitcherForRegistedManagerNotification:(NSNotification *)notificatoin;

@end


@implementation HJBaseViewController

@synthesize bannerView = _bannerView;
@synthesize containerView = _containerView;
@synthesize curtainView = _curtainView;
@dynamic containerFrame;

- (id) init
{
	if( (self = [super init]) != nil ) {
		if( (_observingNotificationSelectorDict = [[NSMutableDictionary alloc] init]) == nil ) {
			return nil;
		}
		if( (_observingNotificatinoStatusKeyDict = [[NSMutableDictionary alloc] init]) == nil ) {
			return nil;
		}
		if( (_containerView = [[UIView alloc] init]) == nil ) {
			return nil;
		}
		if( [self prepareResources] == NO ) {
			return nil;
		}
		[self prepareHandlers];
		[self preparedResourcesAndHandlers];
	}
	
	return self;
}

- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) viewDidLoad
{
	CGRect								containerFrame;
	BOOL								keepStatusBarArea;
	HJBaseViewControllerBannerViewType	bannerViewType;
	CGRect								bannerViewFrame;
	UIView								*curtainView;
	
	[super viewDidLoad];
	
	containerFrame = self.view.bounds;
	keepStatusBarArea = NO;
	if( [self keepStatusBarArea] == YES ) {
		if( [[[UIDevice currentDevice] systemVersion] floatValue] >= 7 ) {
			keepStatusBarArea = YES;
		}
	}
	
	if( keepStatusBarArea == YES ) {
		if( _statusBarView == nil ) {
			_statusBarView = [[UIView alloc] init];
		}
		_statusBarView.backgroundColor = [self colorForStatusBar];
		_statusBarView.frame = CGRectMake( 0.0f, 0.0f, self.view.bounds.size.width, [self heightOfStatusBar] );
		[self.view addSubview: _statusBarView];
		containerFrame.origin.y += [self heightOfStatusBar];
		containerFrame.size.height -= [self heightOfStatusBar];
	}
	
	bannerViewType = [self bannerViewType];
	
	switch( bannerViewType ) {
		case HJBaseViewControllerBannerViewTypeTop :
		case HJBaseViewControllerBannerViewTypeBottom :
			if( _bannerView == nil ) {
				_bannerView = [[UIView alloc] init];
			}
			_bannerView.backgroundColor = [self backgroundColorForBannerView];
			bannerViewFrame.size.width = containerFrame.size.width;
			bannerViewFrame.size.height = [self heightOfBannerView];
			if( bannerViewType == HJBaseViewControllerBannerViewTypeTop ) {
				bannerViewFrame.origin = containerFrame.origin;
				containerFrame.origin.y += [self heightOfBannerView];
			} else {
				bannerViewFrame.origin.x = containerFrame.origin.x;
				bannerViewFrame.origin.y = containerFrame.size.height - bannerViewFrame.size.height;
				if( keepStatusBarArea == YES ) {
					bannerViewFrame.origin.y += [self heightOfStatusBar];
				}
			}
			containerFrame.size.height -= [self heightOfBannerView];
			_bannerView.frame = bannerViewFrame;
			[self.view addSubview: _bannerView];
			break;
		default:
			break;
	}
	
	_containerView.backgroundColor = [self backgroundColorForContainerView];
	_containerView.frame = containerFrame;
	
	[self.view addSubview: _containerView];
	
	if( (curtainView = [self curtainView]) != nil ) {
		curtainView.frame = containerFrame;
		curtainView.alpha = 0.0f;
		_curtainView = curtainView;
		[self.view addSubview: _curtainView];
	}
	
	[self containerViewReady];
}

- (void)automaticSelectorSwitcherForRegistedManagerNotification:(NSNotification *)notificatoin
{
	NSDictionary			*userInfo;
	NSString				*managerName;
	NSString				*statusKey;
	NSString				*managerStatus;
	NSMutableDictionary		*selectorDict;
	SEL						selector;
	IMP						imp;
	
	managerName = [notificatoin name];
	if( (statusKey = [_observingNotificatinoStatusKeyDict objectForKey:managerName]) == nil ) {
		return;
	}
	userInfo = [notificatoin userInfo];
	managerStatus = [[userInfo objectForKey: statusKey] stringValue];
	
	if( ([managerName length] <= 0) || (managerStatus < 0) ) {
		return;
	}
	if( (selectorDict = [_observingNotificationSelectorDict objectForKey: managerName]) == nil ) {
		return;
	}
	if( (selector = [[selectorDict objectForKey:managerStatus] pointerValue]) == nil ) {
		return;
	}
	
	imp = [self methodForSelector:selector];
	void (*func)(id, SEL, id) = (void *)imp;
	func(self, selector, userInfo);
}

- (BOOL)addObserverForManagerName:(NSString *)managerName withStatusKey:(NSString *)statusKey
{
	NSMutableDictionary		*selectorDict;
	
	if( ([managerName length] <= 0) || ([statusKey length] <= 0) ) {
		return NO;
	}
	if( [_observingNotificationSelectorDict objectForKey: managerName] != nil ) {
		[_observingNotificatinoStatusKeyDict setObject: statusKey forKey: managerName];
		return YES;
	}
	if( (selectorDict = [[NSMutableDictionary alloc] init]) == nil ) {
		return NO;
	}
	
	[_observingNotificationSelectorDict setObject: selectorDict forKey: managerName];
	[_observingNotificatinoStatusKeyDict setObject: statusKey forKey: managerName];
	
	[[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(automaticSelectorSwitcherForRegistedManagerNotification:) name:managerName object:nil];
	
	return YES;
}

- (BOOL)registerSelector:(SEL)selector withStatus:(NSInteger)status forManagerName:(NSString *)managerName
{
	NSMutableDictionary		*selectorDict;
	NSValue					*value;
	
	if( (selector == nil) || (status <= 0) || ([managerName length] <= 0) ) {
		return NO;
	}
	if( (selectorDict = [_observingNotificationSelectorDict objectForKey: managerName]) == nil ) {
		return NO;
	}
	
	if( (value = [NSValue valueWithPointer: selector]) == nil ) {
		return NO;
	}
	[selectorDict setObject: value forKey: [[NSNumber numberWithInteger:status] stringValue]];
	
	return YES;
}

- (BOOL)registerDefaultSelector:(SEL)selector forManagerName:(NSString *)managerName
{
	NSMutableDictionary		*selectorDict;
	NSValue					*value;
	
	if( (selector == nil) || ([managerName length] <= 0) ) {
		return NO;
	}
	if( (selectorDict = [_observingNotificationSelectorDict objectForKey: managerName]) == nil ) {
		return NO;
	}
	
	if( (value = [NSValue valueWithPointer: selector]) == nil ) {
		return NO;
	}
	[selectorDict setObject: value forKey: [[NSNumber numberWithInteger:0] stringValue]];
	
	return YES;
}

- (NSString *) name
{
	// override me, if need :)
	return nil;
}

- (BOOL) keepStatusBarArea
{
	// override me, if need :)
	return NO;
}

- (CGFloat) heightOfStatusBar
{
	// override me, if need :)
	return 20.0f;
}

- (UIColor *) colorForStatusBar
{
	// override me, if need :)
	return [UIColor blackColor];
}

- (HJBaseViewControllerBannerViewType) bannerViewType
{
	// override me, if need :)
	return HJBaseViewControllerBannerViewTypeNone;
}

- (CGFloat) heightOfBannerView
{
	// override me, if need :)
	if( [self bannerViewType] == HJBaseViewControllerBannerViewTypeNone ) {
		return 0.0f;
	}
	
	return 50.0f;
}

- (UIColor *) backgroundColorForBannerView
{
	// override me, if need :)
	return [UIColor blackColor];
}

- (UIColor *) backgroundColorForContainerView
{
	// override me, if need :)
	return [self colorForStatusBar];
}

- (UIView *) curtainView
{
	// override me, if need : )
	UIView		*view;
	
	if( _curtainView != nil ) {
		return _curtainView;
	}
	
	if( (view = [[UIView alloc] init]) == nil ) {
		return nil;
	}
	view.backgroundColor = [UIColor blackColor];
	
	return view;
}

- (void) curtain: (BOOL)down animateTime: (NSTimeInterval)animateTime
{
	// override me, if need :)
	if( down == YES ) {
		_curtainView.alpha = 0.0f;
	} else {
		_curtainView.alpha = 0.4f;
	}
	
	[UIView animateWithDuration: animateTime delay: 0.0f options: UIViewAnimationOptionCurveEaseInOut animations: ^{
		if( down == YES ) {
			_curtainView.alpha = 0.4f;
		} else {
			_curtainView.alpha = 0.0f;
		}
	} completion: nil];
}

- (BOOL) prepareResources
{
	// override me, if need :)
	return YES;
}

- (void) prepareHandlers
{
	// override me, if need :)
}
		 
- (void) preparedResourcesAndHandlers
{
	// override me, if need : )
}

- (void) containerViewReady
{
	// override me, if need :)
}

- (CGRect) containerFrame
{
	if( _containerView == nil ) {
		return self.view.bounds;
	}
	
	return _containerView.bounds;
}

@end
