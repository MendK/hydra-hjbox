//
//  HJBaseViewController.h
//  HJBox
//
//  Created by Na Tae Hyun on 13. 10. 24..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import <UIKit/UIKit.h>
#import <Hydra/HYManager.h>


typedef enum HJBaseViewControllerBannerViewType
{
	HJBaseViewControllerBannerViewTypeNone,
	HJBaseViewControllerBannerViewTypeTop,
	HJBaseViewControllerBannerViewTypeBottom
	
} HJBaseViewControllerBannerViewType;


@interface HJBaseViewController : UIViewController
{
	NSMutableDictionary		*_observingNotificationSelectorDict;
	NSMutableDictionary		*_observingNotificatinoStatusKeyDict;
	UIView					*_statusBarView;
	UIView					*_bannerView;
	UIView					*_containerView;
	UIView					*_curtainView;
}

- (BOOL)addObserverForManagerName:(NSString *)managerName withStatusKey:(NSString *)statusKey;
- (BOOL)registerSelector:(SEL)selector withStatus:(NSInteger)status forManagerName:(NSString *)managerName;
- (BOOL)registerDefaultSelector:(SEL)selector forManagerName:(NSString *)managerName;

// override these methods if need.

- (NSString *) name;
- (BOOL) keepStatusBarArea;
- (CGFloat) heightOfStatusBar;
- (UIColor *) colorForStatusBar;
- (HJBaseViewControllerBannerViewType) bannerViewType;
- (CGFloat) heightOfBannerView;;
- (UIColor *) backgroundColorForBannerView;
- (UIColor *) backgroundColorForContainerView;
- (UIView *) curtainView;
- (void) curtain: (BOOL)down animateTime: (NSTimeInterval)animateTime;
- (BOOL) prepareResources;
- (void) prepareHandlers;
- (void) preparedResourcesAndHandlers;
- (void) containerViewReady;

@property (nonatomic, readonly) UIView *bannerView;
@property (nonatomic, readonly) UIView *containerView;
@property (nonatomic, readonly) UIView *curtainView;
@property (nonatomic, readonly) CGRect containerFrame;

@end
