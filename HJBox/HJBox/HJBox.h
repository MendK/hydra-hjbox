//
//  HJBox.h
//  HJBox
//
//  Created by Simon on 1/17/15.
//  Copyright (c) 2015 P9SOFT. All rights reserved.
//
//  Licensed under the MIT license.

#import <UIKit/UIKit.h>

//! Project version number for HJBox.
FOUNDATION_EXPORT double HJBoxVersionNumber;

//! Project version string for HJBox.
FOUNDATION_EXPORT const unsigned char HJBoxVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HJBox/PublicHeader.h>


#import <HJBox/AssetsLibraryManager.h>
#import <HJBox/AsyncHttpDeliverer.h>
#import <HJBox/AVCameraManager.h>

#import <HJBox/Communicator.h>
#import <HJBox/CommunicatorDogma.h>
#import <HJBox/CommunicatorExecutor.h>

#import <HJBox/CoreLocationManager.h>

#import <HJBox/HJBaseViewController.h>
#import <HJBox/HttpOpenApiExecutor.h>
#import <HJBox/HJImageView.h>

// ResourceManager
#import <HJBox/CryptModuleBase64.h>
#import <HJBox/CryptModuleCompress.h>
#import <HJBox/CryptModuleGZip.h>
#import <HJBox/CryptModuleRSA.h>

#import <HJBox/ResourceRemakerResizeImage.h>
#import <HJBox/ResourceRemakerRoundedCornerImage.h>

#import <HJBox/ResourceManager.h>
#import <HJBox/ResourceManagerLocalJobExecutor.h>
#import <HJBox/ResourceManagerRemoteJobExecutor.h>

//#import <HJBox/