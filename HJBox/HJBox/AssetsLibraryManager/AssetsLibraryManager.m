//
//  AssetsLibraryManager.m
//  HJBox
//
//  Created by Na Tae Hyun on 13. 11. 4..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import "AssetsLibraryManager.h"


AssetsLibraryManager	*g_defaultAssetsLibraryManager;


@interface AssetsLibraryManager( AssetsLibraryManagerPrivate )

- (void) postNotifyWithStatus: (AssetsLibraryManagerStatus)status;
- (void) postNotifyWithParamDict: (NSDictionary *)paramDict;
- (void) requestAssetForPoppingGroup;
- (void) requestAllAssets;

@end


@implementation AssetsLibraryManager

@synthesize status = _status;
@dynamic groups;
@dynamic assetsForGroup;

- (id) init
{
	if( (self = [super init]) != nil ) {
		
		_status = AssetsLibraryManagerStatusIdle;
		
		if( (_assetsLibrary = [[ALAssetsLibrary alloc] init]) == nil ) {
			return nil;
		}
		
		if( (_groups = [[NSMutableArray alloc] init]) == nil ) {
			return nil;
		}
		
		if( (_assetsForGroup = [[NSMutableDictionary alloc] init]) == nil ) {
			return nil;
		}
		
		if( (_queue = [[NSMutableArray alloc] init]) == nil ) {
			return nil;
		}
				
		if( (_lock = [[NSLock alloc] init]) == nil ) {
			return nil;
		}
		
	}
	
	return self;
}

+ (AssetsLibraryManager *) defaultManager
{
	@synchronized( self ) {
		if( g_defaultAssetsLibraryManager == nil ) {
			g_defaultAssetsLibraryManager = [[AssetsLibraryManager alloc] init];
		}
	}
	
	return g_defaultAssetsLibraryManager;
}

- (void) postNotifyWithStatus: (AssetsLibraryManagerStatus)status
{
	NSDictionary	*paramDict;
	
	paramDict = [NSDictionary dictionaryWithObject: [NSNumber numberWithInteger: (NSInteger)status] forKey: AssetsLibraryManagerNotifyParameterKeyStatus];
	
	[self performSelectorOnMainThread: @selector(postNotifyWithParamDict:) withObject: paramDict waitUntilDone: NO];
}

- (void) postNotifyWithParamDict: (NSDictionary *)paramDict
{
	[[NSNotificationCenter defaultCenter] postNotificationName: AssetsLibraryManagerNotification object: self userInfo: paramDict];
}

- (void) requestAssetForPoppingGroup
{
	ALAssetsGroup	*group;
	ALAssetsFilter	*filter;
	
	if( [_queue count] <= 0 ) {
		[_lock lock];
		_status = AssetsLibraryManagerStatusIdle;
		[_lock unlock];
		[self postNotifyWithStatus: AssetsLibraryManagerStatusAssetsReady];
		return;
	}
	
	if( (_assets = [[NSMutableArray alloc] init]) == nil ) {
		[_lock lock];
		_status = AssetsLibraryManagerStatusIdle;
		[_lock unlock];
		[self postNotifyWithStatus: AssetsLibraryManagerStatusUnknownError];
		return;
	}
	
	ALAssetsGroupEnumerationResultsBlock assetsEnumerationBlock = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
		
		if( result != nil ) {
			[_assets insertObject: result atIndex: 0];
		} else {
			[self performSelectorOnMainThread: @selector(requestAssetForPoppingGroup) withObject: nil waitUntilDone: NO];
		}
		
	};
	
	group = [_queue objectAtIndex: 0];
	[_queue removeObjectAtIndex: 0];
	filter = [ALAssetsFilter allPhotos];
	[_assetsForGroup setObject: _assets forKey: [group valueForProperty: ALAssetsGroupPropertyName]];
	
	[group setAssetsFilter: filter];
	[group enumerateAssetsUsingBlock: assetsEnumerationBlock];
}

- (void) requestAllAssets
{
	if( [_groups count] == 0 ) {
		[_lock lock];
		_status = AssetsLibraryManagerStatusIdle;
		[_lock unlock];
		[self postNotifyWithStatus: AssetsLibraryManagerStatusAssetsReady];
		return;
	}
	
	[self postNotifyWithStatus: AssetsLibraryManagerStatusRequestingAssets];
	
	[_queue addObjectsFromArray: _groups];
	
	[self requestAssetForPoppingGroup];
}

- (BOOL) requestAllPhotos
{
	BOOL	inRequesting;
	
	[_lock lock];
	
	inRequesting = (_status == AssetsLibraryManagerStatusRequesting);
	if( inRequesting == NO ) {
		[_groups removeAllObjects];
		[_assetsForGroup removeAllObjects];
		[_queue removeAllObjects];
		_assets = nil;
		_status = AssetsLibraryManagerStatusRequesting;
	}
	
	[_lock unlock];
	
	if( inRequesting == YES ) {
		return YES;
	}
	
	[self postNotifyWithStatus: AssetsLibraryManagerStatusRequestingGroups];
	
	ALAssetsLibraryAccessFailureBlock failureBlock = ^(NSError *error) {
		
		[_lock lock];
		_status = AssetsLibraryManagerStatusIdle;
		[_lock unlock];
		
		switch ([error code]) {
			case ALAssetsLibraryAccessUserDeniedError:
			case ALAssetsLibraryAccessGloballyDeniedError:
				[self postNotifyWithStatus: AssetsLibraryManagerStatusAccessDenied];
				break;
			default:
				[self postNotifyWithStatus: AssetsLibraryManagerStatusUnknownError];
				break;
		}
		
	};
	
	ALAssetsLibraryGroupsEnumerationResultsBlock listGroupBlock = ^(ALAssetsGroup *group, BOOL *stop) {
		
		ALAssetsFilter *onlyPhotosFilter = [ALAssetsFilter allPhotos];
		
		[group setAssetsFilter:onlyPhotosFilter];
		
		if( [group numberOfAssets] > 0) {
			[_groups addObject: group];
		} else {
			[self postNotifyWithStatus: AssetsLibraryManagerStatusGroupsReady];
			[self performSelectorOnMainThread: @selector(requestAllAssets) withObject: nil waitUntilDone: NO];
		}
		
	};
	
	[_assetsLibrary enumerateGroupsWithTypes: ALAssetsGroupAlbum|ALAssetsGroupEvent|ALAssetsGroupFaces|ALAssetsGroupSavedPhotos usingBlock: listGroupBlock failureBlock: failureBlock];
	
	return YES;
}

- (BOOL) reset
{
	BOOL	inRequesting;
	
	[_lock lock];
	
	inRequesting = (_status == AssetsLibraryManagerStatusRequesting);
	
	if( inRequesting == NO ) {
		[_groups removeAllObjects];
		[_assetsForGroup removeAllObjects];
		[_queue removeAllObjects];
		_assets = nil;
		_status = AssetsLibraryManagerStatusIdle;
	}
	
	[_lock unlock];
	
	return (inRequesting == NO);
}

- (NSArray *) assetsForGroupName: (NSString *)name
{
	NSMutableArray		*assets;
	NSArray				*list;
	
	if( [name length] <= 0 ) {
		return nil;
	}
	
	[_lock lock];
	
	assets = [_assetsForGroup objectForKey: name];
	
	if( [assets count] <= 0 ) {
		list = nil;
	} else {
		list = [NSArray arrayWithArray: assets];
	}
	
	[_lock unlock];
	
	return list;
}

- (NSArray *) groups
{
	NSArray			*list;
	
	[_lock lock];
	
	list = [NSArray arrayWithArray: _groups];
	
	[_lock unlock];
	
	return list;
}

- (NSDictionary *) assetsForGroup
{
	NSDictionary	*dict;
	
	[_lock lock];
	
	dict = [NSDictionary dictionaryWithDictionary: _assetsForGroup];
	
	[_lock unlock];
	
	return dict;
}

@end
