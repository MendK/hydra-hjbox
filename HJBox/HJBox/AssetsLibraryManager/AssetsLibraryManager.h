//
//  AssetsLibraryManager.h
//  HJBox
//
//  Created by Na Tae Hyun on 13. 11. 4..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>


#define		AssetsLibraryManagerNotification						@"assetsLibraryManagerNotification"

#define		AssetsLibraryManagerNotifyParameterKeyStatus				@"assetsLibraryManagerNotifyParameterKeyStatus"


typedef enum _AssetsLibraryManagerStatus_
{
	AssetsLibraryManagerStatusDummy,
	AssetsLibraryManagerStatusIdle,
	AssetsLibraryManagerStatusRequesting,
	AssetsLibraryManagerStatusRequestingGroups,
	AssetsLibraryManagerStatusGroupsReady,
	AssetsLibraryManagerStatusRequestingAssets,
	AssetsLibraryManagerStatusAssetsReady,
	AssetsLibraryManagerStatusAccessDenied,
	AssetsLibraryManagerStatusUnknownError,
	kCountOfAssetsLibraryManagerStatus
	
} AssetsLibraryManagerStatus;


@interface AssetsLibraryManager : NSObject
{
	AssetsLibraryManagerStatus	_status;
	ALAssetsLibrary				*_assetsLibrary;
	NSMutableArray				*_groups;
	NSMutableDictionary			*_assetsForGroup;
	NSMutableArray				*_queue;
	NSMutableArray				*_assets;
	NSLock						*_lock;
}

+ (AssetsLibraryManager *) defaultManager;

- (BOOL) requestAllPhotos;
- (BOOL) reset;
- (NSArray *) assetsForGroupName: (NSString *)name;

@property (nonatomic, readonly) AssetsLibraryManagerStatus status;
@property (nonatomic, readonly) NSArray *groups;
@property (nonatomic, readonly) NSDictionary *assetsForGroup;

@end
