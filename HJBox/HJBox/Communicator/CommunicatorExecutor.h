//
//  CommunicatorExecutor.h
//  HJBox
//
//  Created by Na Tae Hyun on 13. 4. 18..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import <UIKit/UIKit.h>
#import <Hydra/Hydra.h>


#define		CommunicatorExecutorName										@"communicatorExecutorName"

#define		CommunicatorExecutorParameterKeyOperation						@"communicatorExecutorParameterKeyOperation"
#define		CommunicatorExecutorParameterKeyStatus							@"communicatorExecutorParameterKeyStatus"
#define		CommunicatorExecutorParameterKeyHeaderObject					@"communicatorExecutorParameterKeyHeaderObject"
#define		CommunicatorExecutorParameterKeyBodyObject						@"communicatorExecutorParameterKeyBodyObject"
#define		CommunicatorExecutorParameterKeyServerAddress					@"communicatorExecutorParameterKeyServerAddress"
#define		CommunicatorExecutorParameterKeyServerPort						@"communicatorExecutorParameterKeyServerPort"


typedef enum _CommunicatorExecutorOperation_
{
	CommunicatorExecutorOperationDummy,
	CommunicatorExecutorOperationConnect,
	CommunicatorExecutorOperationDisconnect,
	CommunicatorExecutorOperationDisconnected,
	CommunicatorExecutorOperationSend,
	CommunicatorExecutorOperationReceive,
	CommunicatorExecutorOperationBrokenPacket,
	kCountOfCommunicatorExecutorOperation
	
} CommunicatorExecutorOperation;

typedef enum _CommunicatorExecutorStatus_
{
	CommunicatorExecutorStatusDummy,
	CommunicatorExecutorStatusConnected,
	CommunicatorExecutorStatusConnectFailed,
	CommunicatorExecutorStatusDisconnected,
	CommunicatorExecutorStatusDisconnectFailed,
	CommunicatorExecutorStatusSent,
	CommunicatorExecutorStatusSendFailed,
	CommunicatorExecutorStatusReceived,
	CommunicatorExecutorStatusReceiveFailed,
	CommunicatorExecutorStatusRecevedHeader,
	CommunicatorExecutorStatusReceivedBody,
	CommunicatorExecutorStatusBrokenPacket,
	CommunicatorExecutorStatusUnknownOperation,
	CommunicatorExecutorStatusInvalidParameter,
	CommunicatorExecutorStatusNetworkError,
	CommunicatorExecutorStatusInternalError,
	CommunicatorExecutorStatusCanceled,
	CommunicatorExecutorStatusExpired,
	CommunicatorExecutorStatusError,
	kCountOfCommunicatorExecutorStatus
	
} CommunicatorExecutorStatus;


@interface CommunicatorExecutor : HYExecuter
{
}

@end
