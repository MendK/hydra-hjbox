//
//  CommunicatorDogma.m
//  HJBox
//
//  Created by Na Tae Hyun on 13. 9. 9..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import "CommunicatorDogma.h"


@implementation CommunicatorDogma

- (CommunicatorDogmaReadingMethodType) readingMethodType
{
	return CommunicatorDogmaReadingMethodTypeStream;
}

- (int) sizeOfHeader
{
	return 0;
}

- (id) headerObjectFromHeaderStream: (unsigned char *)stream withStreamSize: (int)streamSize
{
	if( (stream == NULL) || (streamSize <= 0) ) {
		return nil;
	}
	
	return [NSData dataWithBytes: stream length: streamSize];
}

- (BOOL) isBrokenHeaderObject: (id)headerObject
{
	return (headerObject == nil);
}

- (int) sizeOfBodyFromHeaderObject: (id)headerObject
{
	return 0;
}

- (int) sizeOfBodyIfStreamHaveEof: (unsigned char *)stream withStreamSize: (int)streamSize appendedSize: (int)appendedSize
{
	return streamSize;
}

- (int) sizeOfbodyAtStream: (unsigned char *)stream withStreamSize: (int)streamSize appendedSize: (int)appendedSize
{
	return streamSize;
}

- (id) bodyObjectFromBodyStream: (unsigned char *)stream withStreamSize: (int)streamSize headerObject: (id)headerObject
{
	if( (stream == NULL) || (streamSize <= 0) ) {
		return nil;
	}
	
	return [NSData dataWithBytes: stream length: streamSize];
}

- (BOOL) isBrokenBodyObject: (id)bodyObject
{
	return (bodyObject == nil);
}

- (int) maxSizeOfWriteBuffer
{
	return 8192;
}

- (int) prepareWriteBuffer: (unsigned char *)writeBuffer withWriteBufferSize: (int)writeBufferSize fromHeaderObject: (id)headerObject bodyObject: (id)bodyObject
{
	int				headerLength;
	int				bodyLength;
	int				amountLength;
	unsigned char	*plook;
	
	if( (writeBuffer == NULL) || (writeBufferSize <= 0) ) {
		return -1;
	}
	
	if( headerObject != nil ) {
		if( [headerObject isKindOfClass: [NSData class]] == NO ) {
			return -1;
		}
		headerLength = (int)[headerObject length];
	} else {
		headerLength = 0;
	}
	
	if( [bodyObject isKindOfClass: [NSData class]] == NO ) {
		return -1;
	}
	bodyLength = (int)[bodyObject length];
	
	amountLength = headerLength + bodyLength;
	
	if( (amountLength <= 0) || (amountLength > writeBufferSize) ) {
		return -1;
	}
		
	plook = writeBuffer;
	
	if( headerLength > 0 ) {
		memcpy( plook, (unsigned char *)[headerObject bytes], headerLength );
		plook += headerLength;
	}
	if( bodyLength > 0 ) {
		memcpy( plook, (unsigned char *)[bodyObject bytes], bodyLength );
	}
	
	return amountLength;
}

@end
