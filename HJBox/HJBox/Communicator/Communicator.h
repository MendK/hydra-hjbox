//
//  Communicator.h
//  HJBox
//
//  Created by Na Tae Hyun on 13. 9. 9..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import <Foundation/Foundation.h>
#import <Hydra/Hydra.h>
#import <HJBox/CommunicatorDogma.h>
#import <HJBox/CommunicatorExecutor.h>


#define		CommunicatorName								@"communicatorName"


@interface Communicator : HYWorker
{
	id					_dogma;
	NSString			*_serverAddress;
	NSInteger			_serverPort;
	NSLock				*_lock;
	int					_sockfd;
	NSMutableData		*_receivedData;
	unsigned char		*_writeBuffer;
	int					_writeBufferSize;
}

// public methods.

- (id) initWithDogma: (id)dogma;

- (BOOL) setDogma: (id)dogma;

- (void) connectToServerAddress: (NSString *)serverAddress port: (int)serverPort;
- (void) disconnect;
- (void) writeToServerHeaderObject: (id)headerObject bodyObject: (id)bodyObject;

@property (nonatomic, readonly) NSString *serverAddress;
@property (nonatomic, readonly) NSInteger serverPort;
@property (nonatomic, readonly) BOOL online;

// these methods are used for internal handling.
// you may not need to using these methods directly.

- (BOOL) _connectToServerAddress: (NSString *)serverAddress port: (int)serverPort;
- (int) _writeToServerHeaderObject: (id)headerObject bodyObject: (id)bodyObject;
- (BOOL) _disconnect;
- (void) _disconnected;

@end
