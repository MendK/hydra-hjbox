//
//  CommunicatorDogma.h
//  HJBox
//
//  Created by Na Tae Hyun on 13. 9. 9..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import <Foundation/Foundation.h>


typedef enum _CommunicatorDogmaReadingMethodType_
{
	CommunicatorDogmaReadingMethodTypeStream,
	CommunicatorDogmaReadingMethodTypeHeaderWithBody,
	CommunicatorDogmaReadingMethodTypeBodyWithEof,
	CommunicatorDogmaReadingMethodTypeCustomFormat,
	kCountOfCommunicatorDogmaReadingMethodType
	
} CommunicatorDogmaReadingMethodType;


@interface CommunicatorDogma : NSObject
{
}

// override these methods if need.

- (CommunicatorDogmaReadingMethodType) readingMethodType;

- (int) sizeOfHeader;
- (id) headerObjectFromHeaderStream: (unsigned char *)stream withStreamSize: (int)streamSize;
- (BOOL) isBrokenHeaderObject: (id)headerObject;
- (int) sizeOfBodyFromHeaderObject: (id)headerObject;

- (int) sizeOfBodyIfStreamHaveEof: (unsigned char *)stream withStreamSize: (int)streamSize appendedSize: (int)appendedSize;

- (int) sizeOfbodyAtStream: (unsigned char *)stream withStreamSize: (int)streamSize appendedSize: (int)appendedSize;

- (id) bodyObjectFromBodyStream: (unsigned char *)stream withStreamSize: (int)streamSize headerObject: (id)headerObject;
- (BOOL) isBrokenBodyObject: (id)bodyObject;

- (int) maxSizeOfWriteBuffer;
- (int) prepareWriteBuffer: (unsigned char *)writeBuffer withWriteBufferSize: (int)writeBufferSize fromHeaderObject: (id)headerObject bodyObject: (id)bodyObject;

@end
