//
//  Communicator.m
//  HJBox
//
//  Created by Na Tae Hyun on 13. 9. 9..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import <sys/types.h>
#import <sys/socket.h>
#import <netinet/in.h>
#import <arpa/inet.h>
#import <netdb.h>
#import "CommunicatorDogma.h"
#import "CommunicatorExecutor.h"
#import "Communicator.h"


@interface Communicator( CommunicatorPrivate )

- (void) pushQueryForWithOperation: (CommunicatorExecutorOperation)operation header: (id)headerObject body: (id)bodyObject;
- (void) reader: (id)anParamter;

@end


@implementation Communicator

@synthesize serverAddress = _serverAddress;
@synthesize serverPort = _serverPort;
@dynamic online;

- (id) init
{
	if( (self = [super init]) != nil ) {
		_dogma = [[CommunicatorDogma alloc] init];
	}
	
	return self;
}

- (id) initWithDogma: (id)dogma
{
	if( (self = [super init]) != nil ) {
		if( [dogma isKindOfClass: [CommunicatorDogma class]] == NO ) {
			return nil;
		}
		_dogma = dogma;
	}
	
	return self;
}

- (NSString *) name
{
	return CommunicatorName;
}

- (NSString *) brief
{
	return @"Communicator for TCP/IP socket communication.";
}

- (NSString *) customDataDescription
{
	NSString	*desc;
	
	desc = [NSString stringWithFormat: @"<reading_method=\"%d\"", (int)[_dogma readingMethodType]];
	if( _sockfd > 0 ) {
		desc = [desc stringByAppendingFormat: @"<connected online=\"Y\" address=\"%@\" port=\"%ld\" sockfd=\"%d\"", _serverAddress, (long)_serverPort, _sockfd];
	} else {
		desc = [desc stringByAppendingFormat: @"<connected online=\"N\""];
	}
	
	return desc;
}

- (BOOL) didInit
{
	_sockfd = -1;
	if( (_lock = [[NSLock alloc] init]) == nil ) {
		return NO;
	}
	if( (_receivedData = [[NSMutableData alloc] init]) == nil ) {
		return NO;
	}
	if( [self addExecuter: [[CommunicatorExecutor alloc] init]] == NO ) {
		return NO;
	}
	
	return YES;
}

- (void) willDealloc
{
	if( _writeBuffer != NULL ) {
		free( _writeBuffer );
		_writeBuffer = NULL;
	}
}

- (BOOL) setDogma: (id)dogma
{
	if( [dogma isKindOfClass: [CommunicatorDogma class]] == NO ) {
		return NO;
	}
	
	_dogma = dogma;
	
	return YES;
}

- (void) connectToServerAddress: (NSString *)serverAddress port: (int)serverPort
{
	HYQuery		*query;
		
	query = [HYQuery queryWithWorkerName: self.name executerName: CommunicatorExecutorName];
	[query setParameter: [NSNumber numberWithInteger: (NSInteger)CommunicatorExecutorOperationConnect] forKey: CommunicatorExecutorParameterKeyOperation];
	[query setParameter: serverAddress forKey: CommunicatorExecutorParameterKeyServerAddress];
	[query setParameter: [NSNumber numberWithInt: serverPort] forKey: CommunicatorExecutorParameterKeyServerPort];
	
	[self pushQuery: query];
}

- (void) disconnect
{
	HYQuery		*query;
	
	query = [HYQuery queryWithWorkerName: self.name executerName: CommunicatorExecutorName];
	[query setParameter: [NSNumber numberWithInteger: (NSInteger)CommunicatorExecutorOperationDisconnect] forKey: CommunicatorExecutorParameterKeyOperation];
	
	[self pushQuery: query];
}

- (void) writeToServerHeaderObject: (id)headerObject bodyObject: (id)bodyObject
{
	HYQuery		*query;
	
	query = [HYQuery queryWithWorkerName: self.name executerName: CommunicatorExecutorName];
	[query setParameter: [NSNumber numberWithInteger: (NSInteger)CommunicatorExecutorOperationSend] forKey: CommunicatorExecutorParameterKeyOperation];
	[query setParameter: headerObject forKey: CommunicatorExecutorParameterKeyHeaderObject];
	[query setParameter: bodyObject forKey: CommunicatorExecutorParameterKeyBodyObject];
	
	[self pushQuery: query];
}

- (BOOL) online
{
	if( [_lock tryLock] == NO ) {
		return YES;
	}
	
	[_lock unlock];
	return NO;
}

- (BOOL) _connectToServerAddress: (NSString *)serverAddress port: (int)serverPort
{
	struct sockaddr_in		servaddr;
	struct hostent			*phostipref;
	
	if( ([serverAddress length] <= 0) || (serverPort <= 0) ) {
		return NO;
	}
	
	if( [_dogma readingMethodType] == CommunicatorDogmaReadingMethodTypeHeaderWithBody ) {
		if( [_dogma sizeOfHeader] <= 0 ) {
			return NO;
		}
	}
	
	if( [_lock tryLock] == NO ) {
		return NO;
	}
	
	if( (phostipref = gethostbyname( [serverAddress UTF8String] )) == NULL ) {
		[_lock unlock];
		return NO;
	}
	
	if( (_sockfd = socket( PF_INET, SOCK_STREAM, IPPROTO_TCP )) < 0 ) {
		[_lock unlock];
		return NO;
	}
	
	memset( &servaddr, 0, sizeof(servaddr) );
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons( serverPort );
	memcpy( &(servaddr.sin_addr), phostipref->h_addr, phostipref->h_length );
	
	_serverAddress = serverAddress;
	_serverPort = serverPort;
	
	if( connect( _sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0 ) {
		close( _sockfd );
		_sockfd = -1;
		[_lock unlock];
		return NO;
	}
	
	[NSThread detachNewThreadSelector: @selector(reader:) toTarget: self withObject: nil];
	
	return YES;
}

- (int) _writeToServerHeaderObject: (id)headerObject bodyObject: (id)bodyObject
{
	int				length;
	unsigned char	*plook;
	int				wbytes;
	int				leftbytes;
	
	if( (headerObject == nil) && (bodyObject == nil) ) {
		return -1;
	}
	
	if( _dogma == nil ) {
		return 0;
	}
	
	if( (length = [_dogma maxSizeOfWriteBuffer]) != _writeBufferSize ) {
		if( length <= 0 ) {
			return -1;
		}
		if( _writeBuffer != NULL ) {
			free( _writeBuffer );
			_writeBuffer = NULL;
		}
		if( (_writeBuffer = (unsigned char *)malloc( length )) == NULL ) {
			return -1;
		}
		_writeBufferSize = length;
	}
	
	if( (length = [_dogma prepareWriteBuffer: _writeBuffer withWriteBufferSize: _writeBufferSize fromHeaderObject: headerObject bodyObject: bodyObject]) <= 0 ) {
		return -1;
	}
	
	plook = _writeBuffer;
	leftbytes = length;
	
	while( (plook - _writeBuffer) < length ) {
		if( (wbytes = (int)write( _sockfd, plook, leftbytes )) <= 0 ) {
			return -1;
		}
		plook += wbytes;
		leftbytes -= wbytes;
	}
	
	return length;
}

- (BOOL) _disconnect
{
	if( [_lock tryLock] == YES ) {
		[_lock unlock];
		return YES;
	}
	
	return (close( _sockfd ) == 0);
}

- (void) _disconnected
{
	[_lock unlock];
}

- (void) pushQueryForWithOperation: (CommunicatorExecutorOperation)operation header: (id)headerObject body: (id)bodyObject;
{
	HYQuery		*query;
	
	query = [HYQuery queryWithWorkerName: self.name executerName: CommunicatorExecutorName];
	[query setParameter: [NSNumber numberWithInteger: (NSInteger)operation] forKey: CommunicatorExecutorParameterKeyOperation];
	[query setParameter: headerObject forKey: CommunicatorExecutorParameterKeyHeaderObject];
	[query setParameter: bodyObject forKey: CommunicatorExecutorParameterKeyBodyObject];
	
	[self pushQuery: query];
}

- (void) reader: (id)anParamter
{
	int									flags;
	fd_set								rset;
	BOOL								gotBrokenPacket;
	unsigned char						rbuff[8192];
	int									nbytes;
	int									rbytes;
	CommunicatorDogmaReadingMethodType	type;
	int									sizeOfHeader;
	int									sizeOfBody;
	id									headerObject;
	id									bodyObject;
	HYQuery								*query;
	
	gotBrokenPacket = NO;
	sizeOfBody = 0;
	headerObject = nil;
	bodyObject = nil;
	
	FD_ZERO( &rset );
	FD_SET( _sockfd, &rset );
	
	while( 1 ) {
		
		@autoreleasepool {
		
			flags = fcntl( _sockfd, F_GETFL );
			fcntl( _sockfd, F_SETFL, flags|O_NONBLOCK );
			
			if( select( _sockfd+1, &rset, NULL, NULL, NULL ) != 1 ) {
				break;
			}
			
			flags = fcntl( _sockfd, F_GETFL );
			fcntl( _sockfd, F_SETFL, flags&~O_NONBLOCK );
			
			rbytes = 0;
			while( (nbytes = (int)read( _sockfd, rbuff, 8192 )) > 0 ) {
				[_receivedData appendBytes: rbuff length: nbytes];
				rbytes += nbytes;
			}
			if( rbytes <= 0 ) {
				if( (errno != EAGAIN) && (errno != EWOULDBLOCK) ) {
					break;
				}
			}
			
			if( _dogma == nil ) {
				type = CommunicatorDogmaReadingMethodTypeStream;
				sizeOfHeader = 0;
			} else {
				type = [_dogma readingMethodType];
				sizeOfHeader = [_dogma sizeOfHeader];
			}
			
			if( type == CommunicatorDogmaReadingMethodTypeHeaderWithBody ) {
				while( 1 ) {
					if( headerObject == nil ) {
						if( [_receivedData length] < (NSUInteger)sizeOfHeader ) {
							break;
						}
						headerObject = [_dogma headerObjectFromHeaderStream: (unsigned char *)[_receivedData bytes] withStreamSize: sizeOfHeader];
						if( [_dogma isBrokenHeaderObject: headerObject] == YES ) {
							gotBrokenPacket = YES;
							[self pushQueryForWithOperation: CommunicatorExecutorOperationBrokenPacket header: headerObject body: nil];
							break;
						}
						sizeOfBody = [_dogma sizeOfBodyFromHeaderObject: headerObject];
						[_receivedData replaceBytesInRange: NSMakeRange( 0, sizeOfHeader ) withBytes: NULL length: 0];
						if( sizeOfBody <= 0 ) {
							[self pushQueryForWithOperation: CommunicatorExecutorOperationReceive header: headerObject body: nil];
							headerObject = nil;
						}
					}
					if( headerObject != nil ) {
						if( [_receivedData length] < sizeOfBody ) {
							break;
						}
						bodyObject = [_dogma bodyObjectFromBodyStream: (unsigned char *)[_receivedData bytes] withStreamSize: sizeOfBody headerObject: headerObject];
						if( [_dogma isBrokenBodyObject: bodyObject] == YES ) {
							gotBrokenPacket = YES;
							[self pushQueryForWithOperation: CommunicatorExecutorOperationBrokenPacket header: headerObject body: bodyObject];
							break;
						}
						[_receivedData replaceBytesInRange: NSMakeRange( 0, sizeOfBody ) withBytes: NULL length: 0];
						[self pushQueryForWithOperation: CommunicatorExecutorOperationReceive header: headerObject body: bodyObject];
						sizeOfBody = 0;
						headerObject = nil;
						bodyObject = nil;

					}
				}
			} else if( type == CommunicatorDogmaReadingMethodTypeBodyWithEof ) {
				while( (sizeOfBody = [_dogma sizeOfBodyIfStreamHaveEof: (unsigned char *)[_receivedData bytes] withStreamSize: (int)[_receivedData length] appendedSize: rbytes]) > 0 ) {
					bodyObject = [_dogma bodyObjectFromBodyStream: (unsigned char *)[_receivedData bytes] withStreamSize: sizeOfBody headerObject: nil];
					if( [_dogma isBrokenBodyObject: bodyObject] == YES ) {
						gotBrokenPacket = YES;
						[self pushQueryForWithOperation: CommunicatorExecutorOperationBrokenPacket header: nil body: bodyObject];
						break;
					}
					[_receivedData replaceBytesInRange: NSMakeRange( 0, sizeOfBody ) withBytes: NULL length: 0];
					[self pushQueryForWithOperation: CommunicatorExecutorOperationReceive header: nil body: bodyObject];
					bodyObject = nil;
				}
			} else if( type == CommunicatorDogmaReadingMethodTypeCustomFormat ) {
				while( (sizeOfBody = [_dogma sizeOfbodyAtStream: (unsigned char *)[_receivedData bytes] withStreamSize: (int)[_receivedData length] appendedSize: rbytes]) > 0 ) {
					bodyObject = [_dogma bodyObjectFromBodyStream: (unsigned char *)[_receivedData bytes] withStreamSize: sizeOfBody headerObject: nil];
					if( [_dogma isBrokenBodyObject: bodyObject] == YES ) {
						gotBrokenPacket = YES;
						[self pushQueryForWithOperation: CommunicatorExecutorOperationBrokenPacket header: nil body: bodyObject];
						break;
					}
					[_receivedData replaceBytesInRange: NSMakeRange( 0, sizeOfBody ) withBytes: NULL length: 0];
					[self pushQueryForWithOperation: CommunicatorExecutorOperationReceive header: nil body: bodyObject];
					bodyObject = nil;
				}
			} else {
				bodyObject = [NSData dataWithBytes: [_receivedData bytes] length: [_receivedData length]];
				[_receivedData replaceBytesInRange: NSMakeRange( 0, [_receivedData length]) withBytes: NULL length: [_receivedData length]];
				[self pushQueryForWithOperation: CommunicatorExecutorOperationReceive header: nil body: bodyObject];
			}
			
			if( gotBrokenPacket == YES ) {
				break;
			}
			
		}
	}
	
	close( _sockfd );
	_sockfd = -1;
	
	query = [HYQuery queryWithWorkerName: self.name executerName: CommunicatorExecutorName];
	[query setParameter: [NSNumber numberWithInteger: (NSInteger)CommunicatorExecutorOperationDisconnected] forKey: CommunicatorExecutorParameterKeyOperation];
	
	[self pushQuery: query];
}

@end
