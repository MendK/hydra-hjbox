//
//  CommunicatorExecutor.m
//  HJBox
//
//  Created by Na Tae Hyun on 13. 4. 18..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import "CommunicatorExecutor.h"
#import "Communicator.h"


@interface CommunicatorExecutor( CommunicatorExecutorPrivate )

- (HYResult *) resultForQuery: (id)anQuery withStatus: (CommunicatorExecutorStatus)status;
- (void) connectWithQuery: (id)anQuery;
- (void) sendWithQuery: (id)anQuery;
- (void) receiveWithQuery: (id)anQuery;
- (void) disconnectWithQuery: (id)anQuery;
- (void) disconnectedWithQuery: (id)anQuery;
- (void) brokenPacketWithQuery: (id)anQuery;

@end


@implementation CommunicatorExecutor

- (NSString *) name
{
	return CommunicatorExecutorName;
}

- (NSString *) brief
{
	return @"ResourceManager's executor for downloading data from url and remaking, saving data.";
}

- (BOOL) calledExecutingWithQuery: (id)anQuery
{
	CommunicatorExecutorOperation	operation;
	
	operation = (CommunicatorExecutorOperation)[[anQuery parameterForKey: CommunicatorExecutorParameterKeyOperation] integerValue];
	
	switch( operation ) {
		case CommunicatorExecutorOperationConnect :
			[self connectWithQuery: anQuery];
			break;
		case CommunicatorExecutorOperationSend :
			[self sendWithQuery: anQuery];
			break;
		case CommunicatorExecutorOperationReceive :
			[self receiveWithQuery: anQuery];
			break;
		case CommunicatorExecutorOperationDisconnect :
			[self disconnectWithQuery: anQuery];
			break;
		case CommunicatorExecutorOperationDisconnected :
			[self disconnectedWithQuery: anQuery];
			break;
		case CommunicatorExecutorOperationBrokenPacket :
			[self brokenPacketWithQuery: anQuery];
			break;
		default :
			[self storeResult: [self resultForQuery: anQuery withStatus: CommunicatorExecutorStatusUnknownOperation]];
			break;
	}
	
	return YES;
}

- (BOOL) calledCancelingWithQuery: (id)anQuery
{
	[self storeResult: [self resultForQuery: anQuery withStatus: CommunicatorExecutorStatusCanceled]];
	
	return YES;
}

- (id) resultForExpiredQuery: (id)anQuery
{
	return [self resultForQuery: anQuery withStatus: CommunicatorExecutorStatusExpired];
}

- (HYResult *) resultForQuery: (id)anQuery withStatus: (CommunicatorExecutorStatus)status
{
	HYResult	*result;
	
	if( (result = [HYResult resultWithName: self.name]) != nil ) {
		[result setParametersFromDictionary: [anQuery paramDict]];
		[result setParameter: [NSNumber numberWithInteger: status] forKey: CommunicatorExecutorParameterKeyStatus];
	}
	
	return result;
}

- (void) connectWithQuery: (id)anQuery
{
	NSString		*serverAddress;
	int				serverPort;
	
	if( [[self employedWorker] respondsToSelector: @selector(_connectToServerAddress:port:)] == NO ) {
		[self storeResult: [self resultForQuery: anQuery withStatus: CommunicatorExecutorStatusInternalError]];
		return;
	}
	
	serverAddress = [anQuery parameterForKey: CommunicatorExecutorParameterKeyServerAddress];
	serverPort = (int)[[anQuery parameterForKey: CommunicatorExecutorParameterKeyServerPort] integerValue];
	
	if( (serverAddress == nil) || (serverPort <= 0) ) {
		[self storeResult: [self resultForQuery: anQuery withStatus: CommunicatorExecutorStatusInvalidParameter]];
		return;
	}
		
	if( [[self employedWorker] _connectToServerAddress: serverAddress port: (int)serverPort] == NO ) {
		[self storeResult: [self resultForQuery: anQuery withStatus: CommunicatorExecutorStatusConnectFailed]];
	} else {
		[self storeResult: [self resultForQuery: anQuery withStatus: CommunicatorExecutorStatusConnected]];
	}
}

- (void) sendWithQuery: (id)anQuery
{
	id			headerObject;
	id			bodyObject;
	
	if( [[self employedWorker] respondsToSelector: @selector(_writeToServerHeaderObject:bodyObject:)] == NO ) {
		[self storeResult: [self resultForQuery: anQuery withStatus: CommunicatorExecutorStatusInternalError]];
		return;
	}
	
	headerObject = [anQuery parameterForKey: CommunicatorExecutorParameterKeyHeaderObject];
	bodyObject = [anQuery parameterForKey: CommunicatorExecutorParameterKeyBodyObject];
	
	if( (headerObject == nil) && (bodyObject == nil) ) {
		[self storeResult: [self resultForQuery: anQuery withStatus: CommunicatorExecutorStatusInvalidParameter]];
		return;
	}
	
	if( [[self employedWorker] _writeToServerHeaderObject: headerObject bodyObject: bodyObject] < 0 ) {
		[self storeResult: [self resultForQuery: anQuery withStatus: CommunicatorExecutorStatusSendFailed]];
	} else {
		[self storeResult: [self resultForQuery: anQuery withStatus: CommunicatorExecutorStatusSent]];
	}
}

- (void) receiveWithQuery: (id)anQuery
{
	[self storeResult: [self resultForQuery: anQuery withStatus: CommunicatorExecutorStatusReceived]];
}

- (void) disconnectWithQuery: (id)anQuery
{
	HYResult		*result;
	
	if( [[self employedWorker] respondsToSelector: @selector(_disconnect)] == NO ) {
		[self storeResult: [self resultForQuery: anQuery withStatus: CommunicatorExecutorStatusInternalError]];
		return;
	}
	
	if( [[self employedWorker] _disconnect] == NO ) {
		if( (result = [HYResult resultWithName: self.name]) == nil ) {
			[self storeResult: [self resultForQuery: anQuery withStatus: CommunicatorExecutorStatusInternalError]];
		} else {
			[self storeResult: [self resultForQuery: anQuery withStatus: CommunicatorExecutorStatusDisconnectFailed]];
		}
	}
}

- (void) disconnectedWithQuery: (id)anQuery
{
	if( [[self employedWorker] respondsToSelector: @selector(_disconnected)] == NO ) {
		[self storeResult: [self resultForQuery: anQuery withStatus: CommunicatorExecutorStatusInternalError]];
		return;
	}
	
	[[self employedWorker] _disconnected];
	
	[self storeResult: [self resultForQuery: anQuery withStatus: CommunicatorExecutorStatusDisconnected]];
}

- (void) brokenPacketWithQuery: (id)anQuery
{
	[self storeResult: [self resultForQuery: anQuery withStatus: CommunicatorExecutorStatusBrokenPacket]];
}

@end
