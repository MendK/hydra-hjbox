//
//  ResourceRemakerRoundedCornerImage.h
//
//
//  Created by Na Tae Hyun on 13. 5. 13..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import <Foundation/Foundation.h>
#import <HJBox/ResourceManager.h>


#define		ResourceRemakerRoundedCornerImageDefaultName	@"roundedcornerimg"


@interface ResourceRemakerRoundedCornerImage : NSObject <ResourceManagerRemakerProtocol>

+ (NSArray *) parameterFromCorner: (NSInteger)corner border: (NSInteger)border;

@end
