//
//  ResourceRemakerResizeImage.m
//
//
//  Created by Na Tae Hyun on 13. 5. 13..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import <ImageIO/ImageIO.h>
#import "ResourceRemakerResizeImage.h"


#define		kSeparateCharactor						@"_"

@interface ResourceRemakerResizeImage( ResourceRemakerResizeImagePrivate )

@end


@implementation ResourceRemakerResizeImage

+ (NSArray *) parameterFromWidth: (NSInteger)width height: (NSInteger)height
{
	return [NSArray arrayWithObjects: [NSNumber numberWithInteger: width], [NSNumber numberWithInteger: height], nil];
}

- (NSString *) identifierWithParameter: (id)anParameter
{
	NSArray		*list;
	NSNumber	*widthNumber;
	NSNumber	*heightNumber;
	
	if( [anParameter isKindOfClass: [NSArray class]] == NO ) {
		return nil;
	}
	
	list = (NSArray *)anParameter;
	
	if( [list count] != 2 ) {
		return nil;
	}
	
	if( ([[list objectAtIndex: 0] isKindOfClass: [NSNumber class]] == NO) || ([[list objectAtIndex: 1] isKindOfClass: [NSNumber class]] == NO) ) {
		return nil;
	}
	
	widthNumber = [list objectAtIndex: 0];
	heightNumber = [list objectAtIndex: 1];
	
	return [NSString stringWithFormat: @"%@%@%@", widthNumber, kSeparateCharactor, heightNumber];
}

- (NSData *) remakerData: (NSData *)anData withParameter: (id)anParameter
{
	NSString		*identifier;
	NSArray			*list;
	CGSize			resizeSize;
	UIImage			*sourceImage;
	CGSize			sourceSize;
	CGFloat			rate;
	UIImage			*resizedImage;
	CGFloat			scale;
	CGColorSpaceRef	colorSpace;
	CGContextRef	context;
	CGImageRef		resizedImageRef;

	if( [anData length] <= 0 ) {
		return nil;
	}
	if( (identifier = [self identifierWithParameter: anParameter]) == nil ) {
		return nil;
	}
	
	list = [identifier componentsSeparatedByString: kSeparateCharactor];
	
	resizeSize.width = (CGFloat)[[list objectAtIndex: 0] integerValue];
	resizeSize.height = (CGFloat)[[list objectAtIndex: 1] integerValue];
	
	scale = ([[ResourceManager defaultManager] isRetina] == YES) ? 2.0f : 1.0f;
	
	if( (sourceImage = [UIImage imageWithData: anData scale:scale]) == nil ) {
		return nil;
	}
	sourceSize = sourceImage.size;
		
	if( (resizeSize.width <= 0.0f) && (resizeSize.height <= 0.0f) ) {
		
		return anData;
		
	} else if( (resizeSize.width > 0.0f) && (resizeSize.height <= 0.0f) ) {
		
		rate = resizeSize.width / sourceSize.width;
		resizeSize.height = (int)(sourceSize.height * rate);
		
	} else if( (resizeSize.width <= 0) && (resizeSize.height > 0) ) {
		
		rate = resizeSize.height / sourceSize.height;
		resizeSize.width = (int)(sourceSize.width * rate);
		
	}
	
	colorSpace = CGColorSpaceCreateDeviceRGB();
	if( (context = CGBitmapContextCreate( NULL, resizeSize.width*scale, resizeSize.height*scale, 8, 0, colorSpace, (CGBitmapInfo)kCGImageAlphaPremultipliedLast) ) == NULL ) {
        CGColorSpaceRelease(colorSpace);
		return nil;
	}
	CGColorSpaceRelease(colorSpace);
	CGContextScaleCTM( context, scale,  scale);
	CGContextDrawImage( context, CGRectMake( 0.0f, 0.0f, resizeSize.width, resizeSize.height ), sourceImage.CGImage );
	resizedImageRef = CGBitmapContextCreateImage( context );
	CGContextRelease( context );
	resizedImage = [UIImage imageWithCGImage: resizedImageRef scale: scale orientation: UIImageOrientationUp];
	CGImageRelease( resizedImageRef );
	
	if( resizedImage == nil ) {
		return nil;
	}
	
	return UIImagePNGRepresentation( resizedImage );
}

@end
