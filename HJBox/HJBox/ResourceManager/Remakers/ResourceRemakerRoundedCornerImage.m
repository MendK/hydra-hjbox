//
//  ResourceRemakerRoundedCornerImage.m
//
//
//  Created by Na Tae Hyun on 13. 5. 13..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import "ResourceRemakerRoundedCornerImage.h"


#define		kSeparateCharactor			@"_"


@interface ResourceRemakerRoundedCornerImage( ResourceRemakerRoundedCornerImagePrivate )

- (UIImage *) imageWithAlphaIfNotHave: (UIImage *)image;
- (void) addRoundedRectToPath: (CGRect)rect context: (CGContextRef)context ovalWidth: (CGFloat)ovalWidth ovalHeight: (CGFloat)ovalHeight;
- (UIImage *) roundedCornerImageFromImage: (UIImage *)image cornerSize: (NSInteger)cornerSize borderSize: (NSInteger)borderSize;

@end


@implementation ResourceRemakerRoundedCornerImage

+ (NSArray *) parameterFromCorner: (NSInteger)corner border: (NSInteger)border
{
	return [NSArray arrayWithObjects: [NSNumber numberWithInteger: corner], [NSNumber numberWithInteger: border], nil];
}

- (NSString *) identifierWithParameter: (id)anParameter
{
	NSArray		*list;
	NSNumber	*cornerSizeNumber;
	NSNumber	*borderSizeNumber;
	
	if( [anParameter isKindOfClass: [NSArray class]] == NO ) {
		return nil;
	}
	
	list = (NSArray *)anParameter;
	
	if( [list count] != 2 ) {
		return nil;
	}
	
	if( ([[list objectAtIndex: 0] isKindOfClass: [NSNumber class]] == NO) || ([[list objectAtIndex: 1] isKindOfClass: [NSNumber class]] == NO) ) {
		return nil;
	}
	
	cornerSizeNumber = [list objectAtIndex: 0];
	borderSizeNumber = [list objectAtIndex: 1];
	
	return [NSString stringWithFormat: @"%@%@%@", cornerSizeNumber, kSeparateCharactor, borderSizeNumber];
}

- (NSData *) remakerData: (NSData *)anData withParameter: (id)anParameter
{
	NSString	*identifier;
	NSArray		*list;
	NSInteger	cornerSize;
	NSInteger	borderSize;
	UIImage		*image;

	if( [anData length] <= 0 ) {
		return nil;
	}
	if( (identifier = [self identifierWithParameter: anParameter]) == nil ) {
		return nil;
	}
	
	list = [identifier componentsSeparatedByString: kSeparateCharactor];
	
	cornerSize = [[list objectAtIndex: 0] integerValue];
	borderSize = [[list objectAtIndex: 1] integerValue];
	
	if( (image = [self roundedCornerImageFromImage: [UIImage imageWithData: anData] cornerSize: cornerSize borderSize: borderSize]) == nil ) {
		return nil;
	}
	
	return UIImagePNGRepresentation( image );
}

- (UIImage *) imageWithAlphaIfNotHave: (UIImage *)image
{
	CGImageRef			imageRef;
	CGImageAlphaInfo	alpha;
	size_t				width;
	size_t				height;
	CGColorSpaceRef		colorSpaceRef;
	CGContextRef		offscreenContext;
	CGImageRef			imageRefWithAlpha;
	UIImage				*imageWithAlpha;
	
	imageRef = image.CGImage;
	
	alpha = CGImageGetAlphaInfo( imageRef );
	
	if( (alpha == kCGImageAlphaFirst || alpha == kCGImageAlphaLast || alpha == kCGImageAlphaPremultipliedFirst || alpha == kCGImageAlphaPremultipliedLast) ) {
		return image;
	}
	
	width = CGImageGetWidth( imageRef );
	height = CGImageGetHeight( imageRef );
	
	colorSpaceRef = CGColorSpaceCreateDeviceRGB();
	offscreenContext = CGBitmapContextCreate( NULL, width, height, 8, 0, colorSpaceRef, kCGBitmapByteOrderDefault|kCGImageAlphaPremultipliedFirst );
	CGColorSpaceRelease( colorSpaceRef );

	if( offscreenContext == 0 ) {
		offscreenContext = CGBitmapContextCreate( NULL, width, height, 8, 0, CGImageGetColorSpace(imageRef), kCGBitmapByteOrderDefault|kCGImageAlphaPremultipliedFirst );
	}

	CGContextDrawImage( offscreenContext, CGRectMake(0, 0, width, height), imageRef );
	imageRefWithAlpha = CGBitmapContextCreateImage( offscreenContext );
	imageWithAlpha = [UIImage imageWithCGImage: imageRefWithAlpha];

	CGContextRelease( offscreenContext );
	CGImageRelease( imageRefWithAlpha );

	return imageWithAlpha;
}

- (void) addRoundedRectToPath: (CGRect)rect context: (CGContextRef)context ovalWidth: (CGFloat)ovalWidth ovalHeight: (CGFloat)ovalHeight
{
	CGFloat		fw, fh;
	
	if( (ovalWidth == 0) || (ovalHeight == 0) ) {
		CGContextAddRect(context, rect);
		return;
	}
	
	CGContextSaveGState( context );
	CGContextTranslateCTM( context, CGRectGetMinX(rect), CGRectGetMinY(rect) );
	CGContextScaleCTM( context, ovalWidth, ovalHeight );
	fw = CGRectGetWidth(rect) / ovalWidth;
	fh = CGRectGetHeight(rect) / ovalHeight;
	CGContextMoveToPoint( context, fw, fh/2 );
	CGContextAddArcToPoint( context, fw, fh, fw/2, fh, 1 );
	CGContextAddArcToPoint( context, 0, fh, 0, fh/2, 1 );
	CGContextAddArcToPoint( context, 0, 0, fw/2, 0, 1 );
	CGContextAddArcToPoint( context, fw, 0, fw, fh/2, 1 );
	CGContextClosePath( context );
	CGContextRestoreGState( context );
}

- (UIImage *) roundedCornerImageFromImage: (UIImage *)image cornerSize: (NSInteger)cornerSize borderSize: (NSInteger)borderSize
{
	UIImage			*imageWithAplpah;
	CGImageRef		clippedImage;
	UIImage			*roundedImage;
	CGContextRef	context;
	
	if( (imageWithAplpah = [self imageWithAlphaIfNotHave: image]) == nil ) {
		return nil;
	}
    
	context = CGBitmapContextCreate( NULL,
									imageWithAplpah.size.width,
									imageWithAplpah.size.height,
									CGImageGetBitsPerComponent(imageWithAplpah.CGImage),
									0,
									CGImageGetColorSpace(imageWithAplpah.CGImage),
									CGImageGetBitmapInfo(imageWithAplpah.CGImage)
							);
	
    CGContextBeginPath( context );
	
	[self addRoundedRectToPath: CGRectMake( borderSize, borderSize, imageWithAplpah.size.width-(borderSize*2), imageWithAplpah.size.height-(borderSize*2) )
					   context: context
					 ovalWidth: cornerSize
					ovalHeight: cornerSize
	 ];
	
    CGContextClosePath( context );
    CGContextClip( context );
	
    CGContextDrawImage( context, CGRectMake(0, 0, imageWithAplpah.size.width, imageWithAplpah.size.height), imageWithAplpah.CGImage );
	
	clippedImage = CGBitmapContextCreateImage( context );
    CGContextRelease( context );
	
	roundedImage = [UIImage imageWithCGImage: clippedImage];
    CGImageRelease(clippedImage);
    
    return roundedImage;
}

@end
