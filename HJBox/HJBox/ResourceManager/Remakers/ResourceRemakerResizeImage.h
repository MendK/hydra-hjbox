//
//  ResourceRemakerResizeImage.h
//
//
//  Created by Na Tae Hyun on 13. 5. 13..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import <Foundation/Foundation.h>
#import <HJBox/ResourceManager.h>


#define		ResourceRemakerResizeImageDefaultName		@"resizeimg"


@interface ResourceRemakerResizeImage : NSObject <ResourceManagerRemakerProtocol>

+ (NSArray *) parameterFromWidth: (NSInteger)width height: (NSInteger)height;

@end
