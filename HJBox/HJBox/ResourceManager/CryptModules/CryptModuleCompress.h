//
//  CryptModuleCompress.h
//  HJBox
//
//  Created by Na Tae Hyun on 13. 10. 4..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import <Foundation/Foundation.h>
#import <HJBox/ResourceManager.h>


#define		CryptModuleCompressDefaultName			@"Z"


@interface CryptModuleCompress : NSObject <ResourceManagerCryptProtocol>

@end
