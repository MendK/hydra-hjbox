//
//  CryptModuleGZip.h
//  HJBox
//
//  Created by Na Tae Hyun on 13. 10. 4..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import <Foundation/Foundation.h>
#import <libkern/OSAtomic.h>
#import <HJBox/ResourceManager.h>


#define		CryptModuleGzipDefaultName			@"gz"


@interface CryptModuleGZip : NSObject <ResourceManagerCryptProtocol>
{
	int32_t			_issuedId;
	NSUInteger		_readBufferSize;
}

@property (nonatomic, assign) NSUInteger readBufferSize;

@end
