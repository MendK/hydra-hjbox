//
//  CryptModuleRSA.h
//  HJBox
//
//  Created by Tae Hyun, Na on 2014. 12. 18..
//  Copyright (c) 2014년 TeamP9. All rights reserved.
//
//  Licensed under the MIT license.

#import <Foundation/Foundation.h>
#import <Security/Security.h>
//#import <CommonCrypto/CommonDigest.h>
//#import <CommonCrypto/CommonCryptor.h>
#import <HJBox/ResourceManager.h>


#define		CryptModuleRSADefaultName			@"RSA"


@interface CryptModuleRSA : NSObject <ResourceManagerCryptProtocol>
{
	SecCertificateRef	_certificateRef;
	SecPolicyRef		_policyRef;
	SecTrustRef			_trustRef;
	SecKeyRef			_publicKeyRef;
	size_t				_maximumSizeOfPlainText;
}

- (BOOL)loadPublicKeyFromData:(NSData *)publicKeyData;

@property (nonatomic, readonly) size_t maximumSizeOfPlainText;

@end
