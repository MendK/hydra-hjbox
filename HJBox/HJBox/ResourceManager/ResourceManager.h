//
//  ResourceManager.h
//  HJBox
//
//  Created by Na Tae Hyun on 13. 4. 16..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import <UIKit/UIKit.h>
#import <Hydra/Hydra.h>


#define		ResourceManagerOriginalRemakerName				@"origin"
#define		ResourceManagerDownloadingTemporaryName			@".downloading"
#define		ResourceManagerRetainCountFileName				@"rc.dict"
#define		ResourceManagerRemoveMarkFileName				@"rm.dict"

#define		ResourceManagerNotification						@"resourceManagerNotification"

#define		ResourceManagerParameterKeyStatus							@"resourceManagerParameterKeyStatus"
#define		ResourceManagerParameterKeyUrlString						@"resourceManagerParameterKeyUrlString"
#define		ResourceManagerParameterKeyFileName							@"resourceManagerParameterKeyFileName"
#define		ResourceManagerParameterKeyFilePath							@"resourceManagerParameterKeyFilePath"
#define		ResourceManagerParameterKeyRemakerName						@"resourceManagerParameterKeyRemakerName"
#define		ResourceManagerParameterKeyRemakerParameter					@"resourceManagerParameterKeyRemakerParameter"
#define		ResourceManagerParameterKeyCryptMoudleName					@"resourceManagerParameterKeyCryptModuleName"
#define		ResourceManagerParameterKeyDataType							@"resourceManagerParameterKeyDataType"
#define		ResourceManagerParameterKeyDataObject						@"resourceManagerParameterKeyDataObject"
#define		ResourceManagerParameterKeyDataPath							@"resourceManagerParameterKeyDataPath"
#define		ResourceManagerParameterKeyAsyncHttpDelivererIssuedId		@"resourceManagerParameterKeyAsyncHttpDelivererIssuedId"
#define		ResourceManagerParameterKeyAmountItemSize					@"resourceManagerParameterKeyAmountItemSize"

typedef enum _ResourceManagerRequestStatus_
{
	ResourceManagerRequestStatusDownloadingStart,
	ResourceManagerRequestStatusDownloadingFailed,
	ResourceManagerRequestStatusDataPrepared,
	ResourceManagerRequestStatusSavingDone,
	ResourceManagerRequestStatusSavingFailed,
	ResourceManagerRequestStatusRemovingDone,
	ResourceManagerRequestStatusRemovingFailed,
	ResourceManagerRequestStatusGotAmountItemSize,
	ResourceManagerRequestStatusRequestSkipped,
	ResourceManagerRequestStatusRetrySkipped,
	ResourceManagerRequestStatusRequestCanceled,
	kCountOfResourceManagerRequestStatus
	
} ResourceManagerRequestStatus;

typedef enum _ResourceManagerDataType_
{
	ResourceManagerDataTypeString,
	ResourceManagerDataTypeImage,
	ResourceManagerDataTypeData,
	ResourceManagerDataTypeFilePath,
	kCountOfResourceManagerDataType
	
} ResourceManagerDataType;


@protocol ResourceManagerRemakerProtocol

- (NSString *) identifierWithParameter: (id)anParameter;
- (NSData *) remakerData: (NSData *)anData withParameter: (id)anParameter;

@end


@protocol ResourceManagerCryptProtocol

- (NSData *) encryptData: (NSData *)anData;
- (NSData *) decryptData: (NSData *)anData;
- (BOOL) encryptData: (NSData *)anData toFilePath: (NSString *)path;
- (NSData *) decryptDataFromFilePath: (NSString *)path;

@end


@interface ResourceManager : HYManager
{
	BOOL					_standby;
	BOOL					_isRetina;
	BOOL					_paused;
	NSUInteger				_usedMemorySize;
	NSUInteger				_limitSizeOfMemory;
	NSString				*_repositoryPath;
	NSString				*_retainCountDictPath;
	NSString				*_removeMarkDictPath;
	NSMutableDictionary		*_loadedResourceDict;
	NSMutableArray			*_referenceOrder;
	NSMutableDictionary		*_requestingUrlDict;
	NSMutableDictionary		*_reservedRequestDict;
	NSMutableDictionary		*_remakedPathDict;
	NSLock					*_lockForResourceDict;
	NSMutableDictionary		*_loadedUrlHashKeyDict;
	NSLock					*_lockForUrlHashKeyDict;
	NSMutableDictionary		*_remakerDict;
	NSMutableDictionary		*_cryptModuleDict;
	NSMutableDictionary		*_retainCountDict;
	NSMutableDictionary		*_removeMarkDict;
	NSLock					*_lockForSupportDict;
	BOOL					_changed;
}

+ (ResourceManager *) defaultManager;

- (BOOL) standbyWithRepositoryPath: (NSString *)path localJobWorkerName: (NSString *)localJobWorkerName remoteJobWorkerName: (NSString *)remoteJobWorkerName;
- (void) sync;

- (BOOL) setRemaker: (id)remaker forName: (NSString *)name;
- (void) removeRemakerForName: (NSString *)name;
- (void) removeAllRemakers;
- (BOOL) haveRemakerForName: (NSString *)name;
- (NSString *) remakeIdentifierWithParameter: (id)anParameter forName: (NSString *)name;
- (NSData *) remakeData: (NSData *)anData withParameter: (id)anParameter forName: (NSString *)name;

- (BOOL) setCryptModule: (id)cryptModule forName: (NSString *)name;
- (void) removeCryptModuleForName: (NSString *)name;
- (void) removeAllCryptModules;
- (BOOL) haveCryptModuleForName: (NSString *)name;
- (NSData *) encryptData: (NSData *)anData forName: (NSString *)name;
- (NSData *) decryptData: (NSData *)anData forName: (NSString *)name;
- (BOOL) encryptData: (NSData *)anData toFilePath: (NSString *)path forName: (NSString *)name;
- (NSData *) decryptDataFromFilePath: (NSString *)path forName: (NSString *)name;

- (BOOL) saveString: (NSString *)string forFileName: (NSString *)name cryptModuleName: (NSString *)cryptModuleName waitUntilDone: (BOOL)waitUntilDone;
- (BOOL) saveImage: (UIImage *)image forFileName: (NSString *)name cryptModuleName: (NSString *)cryptModuleName waitUntilDone: (BOOL)waitUntilDone;
- (BOOL) saveData: (NSData *)data forFileName: (NSString *)name cryptModuleName: (NSString *)cryptModuleName waitUntilDone: (BOOL)waitUntilDone;
- (BOOL) saveFile: (NSString *)path forFileName: (NSString *)name cryptModuleName: (NSString *)cryptModuleName removeSource: (BOOL)removeSource waitUntilDone: (BOOL)waitUntilDone;

- (BOOL) saveString: (NSString *)string forUrlString: (NSString *)urlString cryptModuleName: (NSString *)cryptModuleName waitUntilDone: (BOOL)waitUntilDone;
- (BOOL) saveImage: (UIImage *)image forUrlString: (NSString *)urlString cryptModuleName: (NSString *)cryptModuleName waitUntilDone: (BOOL)waitUntilDone;
- (BOOL) saveData: (NSData *)data forUrlString: (NSString *)urlString cryptModuleName: (NSString *)cryptModuleName waitUntilDone: (BOOL)waitUntilDone;
- (BOOL) saveFile: (NSString *)path forUrlString: (NSString *)urlString cryptModuleName:(NSString *)cryptModuleName removeSource:(BOOL)removeSource waitUntilDone:(BOOL)waitUntilDone;

- (NSString *) stringFromFilePath: (NSString *)path cryptModuleName: (NSString *)cryptModuleName waitUntilDone: (BOOL)waitUntilDone;
- (NSString *) stringFromResourceName: (NSString *)name cryptModuleName: (NSString *)cryptModuleName waitUntilDone: (BOOL)waitUntilDone;
- (NSString *) stringFromFileName: (NSString *)name cryptModuleName: (NSString *)cryptModuleName waitUntilDone: (BOOL)waitUntilDone;
- (NSString *) stringFromUrlString: (NSString *)urlString cryptModuleName: (NSString *)cryptModuleName ifHaveDataWaitUntilDone: (BOOL)ifHaveDataWaitUntilDone;
- (NSString *) stringFromUrlString: (NSString *)urlString remakerName: (NSString *)remakerName remakerParameter: (id)remakerParameter cryptModuleName: (NSString *)cryptModuleName ifHaveDataWaitUntilDone: (BOOL)ifHaveDataWaitUntilDone;

- (UIImage *) imageFromFilePath: (NSString *)path cryptModuleName: (NSString *)cryptModuleName waitUntilDone: (BOOL)waitUntilDone;
- (UIImage *) imageFromResourceName: (NSString *)name;
- (UIImage *) imageFromResourceName: (NSString *)name cryptModuleName: (NSString *)cryptModuleName waitUntilDone: (BOOL)waitUntilDone;
- (UIImage *) imageFromResourceName: (NSString *)name atBundleName: (NSString *)bundleName waitUntilDone:(BOOL)waitUntilDone;
- (UIImage *) imageFromResourceName: (NSString *)name cryptModuleName: (NSString *)cryptModuleName atBundleName: (NSString *)bundleName waitUntilDone:(BOOL)waitUntilDone;
- (UIImage *) imageFromFileName: (NSString *)name cryptModuleName: (NSString *)cryptModuleName waitUntilDone: (BOOL)waitUntilDone;
- (UIImage *) imageFromUrlString: (NSString *)urlString cryptModuleName: (NSString *)cryptModuleName ifHaveDataWaitUntilDone: (BOOL)ifHaveDataWaitUntilDone;
- (UIImage *) imageFromUrlString: (NSString *)urlString remakerName: (NSString *)remakerName remakerParameter: (id)remakerParameter cryptModuleName: (NSString *)cryptModuleName ifHaveDataWaitUntilDone: (BOOL)ifHaveDataWaitUntilDone;

- (NSData *) dataFromFilePath: (NSString *)path cryptModuleName: (NSString *)cryptModuleName waitUntilDone: (BOOL)waitUntilDone;
- (NSData *) dataFromResourceName: (NSString *)name cryptModuleName: (NSString *)cryptModuleName waitUntilDone: (BOOL)waitUntilDone;
- (NSData *) dataFromFileName: (NSString *)name cryptModuleName: (NSString *)cryptModuleName waitUntilDone: (BOOL)waitUntilDone;
- (NSData *) dataFromUrlString: (NSString *)urlString cryptModuleName: (NSString *)cryptModuleName ifHaveDataWaitUntilDone: (BOOL)ifHaveDataWaitUntilDone;
- (NSData *) dataFromUrlString: (NSString *)urlString remakerName: (NSString *)remakerName remakerParameter: (id)remakerParameter cryptModuleName: (NSString *)cryptModuleName ifHaveDataWaitUntilDone: (BOOL)ifHaveDataWaitUntilDone;

- (NSString *) pathFromResourceName: (NSString *)name;
- (NSString *) pathFromFileNameFolder: (NSString *)name;
- (NSString *) pathFromFileName: (NSString *)name;
- (NSString *) pathFromUrlString: (NSString *)urlString;
- (NSString *) pathFromUrlString: (NSString *)urlString remakerName: (NSString *)remakerName remakerParameter: (id)remakerParameter;
- (NSString *) urlStringFromFileName: (NSString *)name;
- (NSString *) folderPathFromUrlString: (NSString *)urlString;
- (NSString *) temporaryFilePathFromUrlString: (NSString *)urlString;

- (NSDate *) createDateForFilePath: (NSString *)path;
- (NSDate *) createDateForCachedUrlString: (NSString *)urlString;
- (NSDate *) createDateForSavedFileName: (NSString *)name;

- (NSUInteger) amountSizeOfAllItems: (BOOL)waitUntilDone;

- (BOOL) removeItemForFilePath: (NSString *)path force: (BOOL)force;
- (BOOL) removeItemForFileName: (NSString *)name force: (BOOL)force;
- (BOOL) removeItemForUrlString: (NSString *)urlString force: (BOOL)force;
- (BOOL) removeItemsForElapsedTimeFromNow: (NSTimeInterval)elpasedTime force: (BOOL)force waitUntilDone: (BOOL)waitUntilDone;
- (BOOL) removeItemsForMaximumSizeBoundary: (NSUInteger)boundarySize force: (BOOL)force waitUntilDone: (BOOL)waitUntilDone;
- (BOOL) removeAllItems: (BOOL)force waitUntilDone: (BOOL)waitUntilDone;

- (void) unloadItemForFilePath: (NSString *)path;
- (void) unloadItemForResourceName: (NSString *)name;
- (void) unloadItemForFileName: (NSString *)name;
- (void) unloadItemForUrlString: (NSString *)urlString;
- (void) unloadItemForUrlString: (NSString *)urlString remakerName: (NSString *)remakerName remakerParameter: (id)remakerParameter;
- (void) unloadAllItems;

- (NSInteger) retainCountForFilePath: (NSString *)path;
- (NSInteger) retainCountForFileName: (NSString *)name;
- (NSInteger) retainCountForUrlString: (NSString *)urlString;
- (NSInteger) retainCountForUrlString: (NSString *)urlString remakerName: (NSString *)remakerName remakerParameter: (id)remakerParameter;
- (NSInteger) retainFilePath: (NSString *)path;
- (NSInteger) retainFileName: (NSString *)name;
- (NSInteger) retainUrlString: (NSString *)urlString;
- (NSInteger) retainUrlString: (NSString *)urlString remakerName: (NSString *)remakerName remakerParameter: (id)remakerParameter;
- (NSInteger) releaseFilePath: (NSString *)path;
- (NSInteger) releaseFileName: (NSString *)name;
- (NSInteger) releaseUrlString: (NSString *)urlString;
- (NSInteger) releaseUrlString: (NSString *)urlString remakerName: (NSString *)remakerName remakerParameter: (id)remakerParameter;

- (void) pauseTransfering;
- (void) resumeTransfering;
- (void) cancelTransfering;

- (NSData *) readDataFromPath: (NSString *)path cryptModuleName: (NSString *)cryptModuleName;
- (BOOL) writeData: (NSData *)data toPath: (NSString *)path cryptModuleName: (NSString *)cryptModuleName;

- (UIImage *) imageWithColor: (UIColor *)color size: (CGSize)size;
- (UIImage *) image: (UIImage *)image byRemakerName: (NSString *)remakerName remakerParameter: (id)remakerParameter;

@property (nonatomic, readonly) NSString *repositoryPath;
@property (nonatomic, readonly) BOOL standby;
@property (nonatomic, readonly) BOOL isRetina;
@property (nonatomic, readonly) NSUInteger usedMemorySize;
@property (nonatomic, assign) NSUInteger limitSizeOfMemory;

@end
