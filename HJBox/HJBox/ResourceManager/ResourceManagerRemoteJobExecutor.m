//
//  ResourceManagerRemoteJobExecutor.m
//  HJBox
//
//  Created by Na Tae Hyun on 13. 4. 18..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import "ResourceManagerRemoteJobExecutor.h"
#import "AsyncHttpDeliverer.h"
#import "ResourceManager.h"


#define			kDefaultTimeoutInterval					8
#define			kDefaultMaximumCountOfConnection		16


@interface ResourceManagerRemoteJobExecutor( ResourceManagerRemoteJobExecutorPrivate )

- (HYResult *) resultForQuery: (id)anQuery withStatus: (ResourceManagerRemoteJobExecutorStatus)status;
- (void) requestWithQuery: (id)anQuery;
- (void) receivedWithQuery: (id)anQuery;
- (void) saveWithQuery: (id)anQuery;
- (void) removeWithQuery: (id)anQuery;
- (void) amountItemSizeWithQuery: (id)anQuery;

@end


@implementation ResourceManagerRemoteJobExecutor

@synthesize timeoutInterval = _timeoutInterval;
@synthesize maximumConnection = _maximumConnection;

- (id) init
{
	if( (self = [super init]) != nil ) {
		_timeoutInterval = kDefaultTimeoutInterval;
		_maximumConnection = kDefaultMaximumCountOfConnection;
	}
	
	return self;
}

- (NSString *) name
{
	return ResourceManagerRemoteJobExecutorName;
}

- (NSString *) brief
{
	return @"ResourceManager's executor for downloading data from url and remaking data.";
}

- (BOOL) calledExecutingWithQuery: (id)anQuery
{
	ResourceManagerRemoteJobExecutorOperation	operation;
	
	operation = (ResourceManagerRemoteJobExecutorOperation)[[anQuery parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyOperation] integerValue];
	
	switch( operation ) {
		case ResourceManagerRemoteJobExecutorOperationRequest :
			[self requestWithQuery: anQuery];
			break;
		case ResourceManagerRemoteJobExecutorOperationReceive :
			[self receivedWithQuery: anQuery];
			break;
		default :
			[self storeResult: [self resultForQuery: anQuery withStatus: ResourceManagerRemoteJobExecutorStatusUnknownOperation]];
			break;
	}
	
	return YES;
}

- (BOOL) calledCancelingWithQuery: (id)anQuery
{
	[self storeResult: [self resultForQuery: anQuery withStatus: ResourceManagerRemoteJobExecutorStatusCanceled]];
	
	return YES;
}

- (id) resultForExpiredQuery: (id)anQuery
{
	return [self resultForQuery: anQuery withStatus: ResourceManagerRemoteJobExecutorStatusExpired];
}

- (HYResult *) resultForQuery: (id)anQuery withStatus: (ResourceManagerRemoteJobExecutorStatus)status
{
	HYResult	*result;
	
	if( (result = [HYResult resultWithName: self.name]) != nil ) {
		[result setParametersFromDictionary: [anQuery paramDict]];
		[result setParameter: [NSNumber numberWithInteger: status] forKey: ResourceManagerRemoteJobExecutorParameterKeyStatus];
	}
	
	return result;
}

- (void) requestWithQuery: (id)anQuery
{
	ResourceManagerRemoteJobExecutorDataType	dataType;
	NSString									*urlString;
	NSString									*fileName;
	NSString									*remakerName;
	id											remakerParameter;
	NSString									*cryptModuleName;
	NSString									*originPath;
	NSString									*remakerPath;
	NSString									*temporaryPath;
	NSData										*inputData;
	NSData										*outputData;
	HYQuery										*closeQuery;
	AsyncHttpDeliverer							*deliverer;
	HYResult									*result;
	
	dataType = (ResourceManagerRemoteJobExecutorDataType)[[anQuery parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyDataType] integerValue];
	urlString = [anQuery parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyUrlString];
	fileName = [anQuery parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyFileName];
	remakerName = [anQuery parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyRemakerName];
	remakerParameter = [anQuery parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyRemakerParameter];
	cryptModuleName = [anQuery parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyCryptModuleName];
	
	if( [urlString length] <= 0 ) {
		urlString = [[ResourceManager defaultManager] urlStringFromFileName: fileName];
	}
	if( ([urlString length] <= 0) || ([remakerName length] <= 0) ) {
		[self storeResult: [self resultForQuery: anQuery withStatus: ResourceManagerRemoteJobExecutorStatusInvalidParameter]];
		return;
	}
	
	originPath = [[ResourceManager defaultManager] pathFromUrlString: urlString remakerName: ResourceManagerOriginalRemakerName remakerParameter: nil];
	outputData = nil;
	
	if( [remakerName isEqualToString: ResourceManagerOriginalRemakerName] == NO ) {
		
		remakerPath = [[ResourceManager defaultManager] pathFromUrlString: urlString remakerName: remakerName remakerParameter: remakerParameter];
		if( (outputData = [[ResourceManager defaultManager] dataFromFilePath: remakerPath cryptModuleName: cryptModuleName waitUntilDone: YES]) == nil ) {
			if( (inputData = [[ResourceManager defaultManager] dataFromFilePath: originPath cryptModuleName: cryptModuleName waitUntilDone: YES]) != nil ) {
				if( (outputData = [[ResourceManager defaultManager] remakeData: inputData withParameter: remakerParameter forName: remakerName]) != nil ) {
					if( [[ResourceManager defaultManager] writeData: outputData toPath: remakerPath cryptModuleName: cryptModuleName] == NO ) {
						[self storeResult: [self resultForQuery: anQuery withStatus: ResourceManagerRemoteJobExecutorStatusInternalError]];
						return;
					}
				}
			}
		}
		
	} else {
		
		remakerPath = originPath;
		outputData = [[ResourceManager defaultManager] dataFromFilePath: originPath cryptModuleName: cryptModuleName waitUntilDone: YES];
		
	}
	
	if( outputData == nil ) {
		
		if( (temporaryPath = [[ResourceManager defaultManager] temporaryFilePathFromUrlString: urlString]) == nil ) {
			[self storeResult: [self resultForQuery: anQuery withStatus: ResourceManagerRemoteJobExecutorStatusInternalError]];
			return;
		}
		if( (closeQuery = [HYQuery queryWithWorkerName: [self.employedWorker name] executerName: self.name]) == nil ) {
			[self storeResult: [self resultForQuery: anQuery withStatus: ResourceManagerRemoteJobExecutorStatusInternalError]];
			return;
		}
		
		[closeQuery setParametersFromDictionary: [anQuery paramDict]];
		[closeQuery setParameter: [NSNumber numberWithInteger: ResourceManagerRemoteJobExecutorOperationReceive] forKey: ResourceManagerRemoteJobExecutorParameterKeyOperation];
		[closeQuery setParameter: originPath forKey: ResourceManagerRemoteJobExecutorParameterKeyDataPath];
		[closeQuery setParameter: temporaryPath forKey: ResourceManagerRemoteJobExecutorParameterKeyTemporaryPath];
		
		if( (deliverer = [[AsyncHttpDeliverer alloc] initWithCloseQuery: closeQuery]) == nil ) {
			[self storeResult: [self resultForQuery: anQuery withStatus: ResourceManagerRemoteJobExecutorStatusInternalError]];
			return;
		}
		[deliverer setGetWithUrlString: urlString toFilePath: temporaryPath];
		[deliverer setTimeoutInterval: self.timeoutInterval];
		[deliverer setNotifyStatus: YES];
		[deliverer activeLimiterName: self.name withCount: self.maximumConnection];
		
		[self bindAsyncTask: deliverer];
		
	}
	
	if( (result = [HYResult resultWithName: self.name]) == nil ) {
		[self storeResult: [self resultForQuery: anQuery withStatus: ResourceManagerRemoteJobExecutorStatusInternalError]];
		return;
	}
	
	[result setParametersFromDictionary: [anQuery paramDict]];
	[result setParameter: remakerPath forKey: ResourceManagerRemoteJobExecutorParameterKeyFilePath];
	
	if( outputData == nil ) {
		[result setParameter: [NSNumber numberWithInteger: ResourceManagerRemoteJobExecutorStatusRequested] forKey: ResourceManagerRemoteJobExecutorParameterKeyStatus];
		[result setParameter: [NSNumber numberWithUnsignedInteger:(NSUInteger)deliverer.issuedId] forKey: ResourceManagerRemoteJobExecutorParameterKeyAsyncHttpDelivererIssuedId];
	} else {
		[result setParameter: [NSNumber numberWithInteger: ResourceManagerRemoteJobExecutorStatusReceived] forKey: ResourceManagerRemoteJobExecutorParameterKeyStatus];
		[result setParameter: [NSNumber numberWithInteger: dataType] forKey: ResourceManagerRemoteJobExecutorParameterKeyDataType];
		switch( dataType ) {
			case ResourceManagerRemoteJobExecutorDataTypeString :
				[result setParameter: [[NSString alloc] initWithData: outputData encoding: NSUTF8StringEncoding] forKey: ResourceManagerRemoteJobExecutorParameterKeyString];
				break;
			case ResourceManagerRemoteJobExecutorDataTypeImage :
				[result setParameter: [UIImage imageWithData: outputData] forKey: ResourceManagerRemoteJobExecutorParameterKeyImage];
				break;
			case ResourceManagerRemoteJobExecutorDataTypeDataObject :
				[result setParameter: outputData forKey: ResourceManagerRemoteJobExecutorParameterKeyDataObject];
				break;
			default :
				outputData = nil;
				[result setParameter: [NSNumber numberWithInteger: ResourceManagerRemoteJobExecutorStatusInvalidParameter] forKey: ResourceManagerRemoteJobExecutorParameterKeyStatus];
				break;
		}
	}
	
	[self storeResult: result];
}

- (void) receivedWithQuery: (id)anQuery
{
	ResourceManagerRemoteJobExecutorDataType		dataType;
	NSString							*urlString;
	NSString							*remakerName;
	id									remakerParameter;
	NSString							*cryptModuleName;
	NSString							*originPath;
	NSString							*remakerPath;
	NSString							*temporaryPath;
	NSData								*inputData;
	NSData								*outputData;
	HYResult							*result;
	
	dataType = (ResourceManagerRemoteJobExecutorDataType)[[anQuery parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyDataType] integerValue];
	urlString = [anQuery parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyUrlString];
	remakerName = [anQuery parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyRemakerName];
	remakerParameter = [anQuery parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyRemakerParameter];
	cryptModuleName = [anQuery parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyCryptModuleName];
	originPath = [anQuery parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyDataPath];
	temporaryPath = [anQuery parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyTemporaryPath];
	
	if( ([urlString length] <= 0) || ([remakerName length] <= 0) || ([originPath length] <= 0) || ([temporaryPath length] <= 0) ) {
		[self storeResult: [self resultForQuery: anQuery withStatus: ResourceManagerRemoteJobExecutorStatusInvalidParameter]];
		return;
	}
	
	if( [[anQuery parameterForKey: AsyncHttpDelivererParameterKeyFailed] boolValue] == YES ) {
		[self storeResult: [self resultForQuery: anQuery withStatus: ResourceManagerRemoteJobExecutorStatusNetworkError]];
		return;
	}
	
	[[NSFileManager defaultManager] removeItemAtPath: originPath error: nil];
	
	if( [[NSFileManager defaultManager] moveItemAtPath: temporaryPath toPath: originPath error: nil] == NO ) {
		[[NSFileManager defaultManager] removeItemAtPath: temporaryPath error: nil];
		[self storeResult: [self resultForQuery: anQuery withStatus: ResourceManagerRemoteJobExecutorStatusInternalError]];
		return;
	}
	
	[[NSFileManager defaultManager] removeItemAtPath: temporaryPath error: nil];
	
	outputData = nil;
	
	if( [remakerName isEqualToString: ResourceManagerOriginalRemakerName] == NO ) {
		
		remakerPath = [[ResourceManager defaultManager] pathFromUrlString: urlString remakerName: remakerName remakerParameter: remakerParameter];
		
		if( (inputData = [[ResourceManager defaultManager] dataFromFilePath: originPath cryptModuleName: cryptModuleName waitUntilDone: YES]) != nil ) {
			if( (outputData = [[ResourceManager defaultManager] remakeData: inputData withParameter: remakerParameter forName: remakerName]) != nil ) {
				if( [[ResourceManager defaultManager] writeData: outputData toPath: remakerPath cryptModuleName: cryptModuleName] == NO ) {
					[self storeResult: [self resultForQuery: anQuery withStatus: ResourceManagerRemoteJobExecutorStatusInternalError]];
					return;
				}
			}
		}
		
	} else {
		
		remakerPath = originPath;
		outputData = [[ResourceManager defaultManager] dataFromFilePath: originPath cryptModuleName: cryptModuleName waitUntilDone: YES];
		
	}
	
	if( (result = [HYResult resultWithName: self.name]) == nil ) {
		[self storeResult: [self resultForQuery: anQuery withStatus: ResourceManagerRemoteJobExecutorStatusInternalError]];
		return;
	}
	
	[result setParametersFromDictionary: [anQuery paramDict]];
	[result setParameter: remakerPath forKey: ResourceManagerRemoteJobExecutorParameterKeyFilePath];
	
	if( outputData == nil ) {
		[result setParameter: [NSNumber numberWithInteger: ResourceManagerRemoteJobExecutorStatusInternalError] forKey: ResourceManagerRemoteJobExecutorParameterKeyStatus];
		[result setParameter: [anQuery parameterForKey: AsyncHttpDelivererParameterKeyIssuedId] forKey: ResourceManagerRemoteJobExecutorParameterKeyAsyncHttpDelivererIssuedId];
	} else {
		[result setParameter: [NSNumber numberWithInteger: ResourceManagerRemoteJobExecutorStatusReceived] forKey: ResourceManagerRemoteJobExecutorParameterKeyStatus];
		[result setParameter: [NSNumber numberWithInteger: dataType] forKey: ResourceManagerRemoteJobExecutorParameterKeyDataType];
		switch( dataType ) {
			case ResourceManagerRemoteJobExecutorDataTypeString :
				[result setParameter: [[NSString alloc] initWithData: outputData encoding: NSUTF8StringEncoding] forKey: ResourceManagerRemoteJobExecutorParameterKeyString];
				break;
			case ResourceManagerRemoteJobExecutorDataTypeImage :
				[result setParameter: [UIImage imageWithData: outputData] forKey: ResourceManagerRemoteJobExecutorParameterKeyImage];
				break;
			case ResourceManagerRemoteJobExecutorDataTypeDataObject :
				[result setParameter: outputData forKey: ResourceManagerRemoteJobExecutorParameterKeyDataObject];
				break;
			default :
				outputData = nil;
				[result setParameter: [NSNumber numberWithInteger: ResourceManagerRemoteJobExecutorStatusInvalidParameter] forKey: ResourceManagerRemoteJobExecutorParameterKeyStatus];
				break;
		}
	}
	
	[self storeResult: result];
}

@end
