//
//  ResourceManagerRemoteJobExecutor.h
//  HJBox
//
//  Created by Na Tae Hyun on 13. 4. 18..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import <UIKit/UIKit.h>
#import <Hydra/Hydra.h>


#define		ResourceManagerRemoteJobExecutorName									@"resourceManagerRemoteJobExecutorName"

#define		ResourceManagerRemoteJobExecutorParameterKeyOperation					@"resourceManagerRemoteJobExecutorParameterKeyOperation"
#define		ResourceManagerRemoteJobExecutorParameterKeyDataType					@"resourceManagerRemoteJobExecutorParameterKeyDataType"
#define		ResourceManagerRemoteJobExecutorParameterKeyStatus						@"resourceManagerRemoteJobExecutorParameterKeyStatus"
#define		ResourceManagerRemoteJobExecutorParameterKeyUrlString					@"resourceManagerRemoteJobExecutorParameterKeyUrlString"
#define		ResourceManagerRemoteJobExecutorParameterKeyFileName					@"resourceManagerRemoteJobExecutorParameterFileName"
#define		ResourceManagerRemoteJobExecutorParameterKeyFilePath					@"resourceManagerRemoteJobExecutorParameterFilePath"
#define		ResourceManagerRemoteJobExecutorParameterKeyRemakerName					@"resourceManagerRemoteJobExecutorParameterKeyRemakerName"
#define		ResourceManagerRemoteJobExecutorParameterKeyRemakerParameter			@"resourceManagerRemoteJobExecutorParameterKeyRemakerParameter"
#define		ResourceManagerRemoteJobExecutorParameterKeyCryptModuleName				@"resourceManagerRemoteJobExecutorParameterCryptModuleName"
#define		ResourceManagerRemoteJobExecutorParameterKeyAsyncHttpDelivererIssuedId	@"resourceManagerRemoteJobExecutorParameterKeyAsyncHttpDeliverereIssuedId"
#define		ResourceManagerRemoteJobExecutorParameterKeyString						@"resourceManagerRemoteJobExecutorParameterKeyString"
#define		ResourceManagerRemoteJobExecutorParameterKeyImage						@"resourceManagerRemoteJobExecutorParameterKeyImage"
#define		ResourceManagerRemoteJobExecutorParameterKeyDataObject					@"resourceManagerRemoteJobExecutorParameterKeyDataObject"
#define		ResourceManagerRemoteJobExecutorParameterKeyDataPath					@"resourceManagerRemoteJobExecutorParameterKeyDataPath"
#define		ResourceManagerRemoteJobExecutorParameterKeyTemporaryPath				@"resourceManagerRemoteJobExecutorParameterKeyTemporaryPath"


typedef enum _ResourceManagerRemoteJobExecutorOperation_
{
	ResourceManagerRemoteJobExecutorOperationDummy,
	ResourceManagerRemoteJobExecutorOperationRequest,
	ResourceManagerRemoteJobExecutorOperationReceive,
	kCountOfResourceManagerRemoteJobExecutorOperation
	
} ResourceManagerRemoteJobExecutorOperation;

typedef enum _ResourceManagerRemoteJobExecutorDataType_
{
	ResourceManagerRemoteJobExecutorDataTypeDummy,
	ResourceManagerRemoteJobExecutorDataTypeString,
	ResourceManagerRemoteJobExecutorDataTypeImage,
	ResourceManagerRemoteJobExecutorDataTypeDataObject,
	ResourceManagerRemoteJobExecutorDataTypeDataPath,
	kCountOfResourceManagerRemoteJobExecutorDataType
	
} ResourceManagerRemoteJobExecutorDataType;

typedef enum _ResourceManagerRemoteJobExecutorStatus_
{
	ResourceManagerRemoteJobExecutorStatusDummy,
	ResourceManagerRemoteJobExecutorStatusRequested,
	ResourceManagerRemoteJobExecutorStatusReceived,
	ResourceManagerRemoteJobExecutorStatusCanceled,
	ResourceManagerRemoteJobExecutorStatusExpired,
	ResourceManagerRemoteJobExecutorStatusUnknownOperation,
	ResourceManagerRemoteJobExecutorStatusInvalidParameter,
	ResourceManagerRemoteJobExecutorStatusNetworkError,
	ResourceManagerRemoteJobExecutorStatusInternalError,
	kCountOfResourceManagerRemoteJobExecutorStatus
	
} ResourceManagerRemoteJobExecutorStatus;


@interface ResourceManagerRemoteJobExecutor : HYExecuter
{
	NSTimeInterval		_timeoutInterval;
	NSInteger			_maximumConnection;
}

@property (nonatomic, assign) NSTimeInterval timeoutInterval;
@property (nonatomic, assign) NSInteger maximumConnection;

@end
