//
//  ResourceManagerLocalJobExecutor.m
//  HJBox
//
//  Created by Na Tae Hyun on 13. 4. 18..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import "ResourceManagerLocalJobExecutor.h"
#import "AsyncHttpDeliverer.h"
#import "ResourceManager.h"


@interface ResourceManagerLocalJobExecutor( ResourceManagerLocalJobExecutorPrivate )

- (HYResult *) resultForQuery: (id)anQuery withStatus: (ResourceManagerLocalJobExecutorStatus)status;
- (void) saveWithQuery: (id)anQuery;
- (void) removeWithQuery: (id)anQuery;
- (void) amountItemSizeWithQuery: (id)anQuery;

@end


@implementation ResourceManagerLocalJobExecutor

- (NSString *) name
{
	return ResourceManagerLocalJobExecutorName;
}

- (NSString *) brief
{
	return @"ResourceManager's executor for save, remove, amount size operation.";
}

- (BOOL) calledExecutingWithQuery: (id)anQuery
{
	ResourceManagerLocalJobExecutorOperation	operation;
	
	operation = (ResourceManagerLocalJobExecutorOperation)[[anQuery parameterForKey: ResourceManagerLocalJobExecutorParameterKeyOperation] integerValue];
	
	switch( operation ) {
		case ResourceManagerLocalJobExecutorOperationSave :
			[self saveWithQuery: anQuery];
			break;
		case ResourceManagerLocalJobExecutorOperationRemove :
			[self removeWithQuery: anQuery];
			break;
		case ResourceManagerLocalJobExecutorOperationAmountItemSize :
			[self amountItemSizeWithQuery: anQuery];
			break;
		default :
			[self storeResult: [self resultForQuery: anQuery withStatus: ResourceManagerLocalJobExecutorStatusUnknownOperation]];
			break;
	}
	
	return YES;
}

- (BOOL) calledCancelingWithQuery: (id)anQuery
{
	[self storeResult: [self resultForQuery: anQuery withStatus: ResourceManagerLocalJobExecutorStatusCanceled]];
	
	return YES;
}

- (id) resultForExpiredQuery: (id)anQuery
{
	return [self resultForQuery: anQuery withStatus: ResourceManagerLocalJobExecutorStatusExpired];
}

- (HYResult *) resultForQuery: (id)anQuery withStatus: (ResourceManagerLocalJobExecutorStatus)status
{
	HYResult	*result;
	
	if( (result = [HYResult resultWithName: self.name]) != nil ) {
		[result setParametersFromDictionary: [anQuery paramDict]];
		[result setParameter: [NSNumber numberWithInteger: status] forKey: ResourceManagerLocalJobExecutorParameterKeyStatus];
	}
	
	return result;
}

- (void) saveWithQuery: (id)anQuery
{
	ResourceManagerLocalJobExecutorDataType		dataType;
	NSString									*fileName;
	NSString									*urlString;
	NSString									*cryptModuleName;
	NSData										*data;
	NSString									*filePath;
	BOOL										removeSourceFlag;
	NSString									*originPath;
	HYResult									*result;
	ResourceManagerLocalJobExecutorStatus		status;
	
	dataType = (ResourceManagerLocalJobExecutorDataType)[[anQuery parameterForKey: ResourceManagerLocalJobExecutorParameterKeyDataType] integerValue];
	fileName = [anQuery parameterForKey: ResourceManagerLocalJobExecutorParameterKeyFileName];
	urlString = [anQuery parameterForKey: ResourceManagerLocalJobExecutorParameterKeyUrlString];
	cryptModuleName = [anQuery parameterForKey: ResourceManagerLocalJobExecutorParameterKeyCryptModuleName];
	
	if( ([fileName length] <= 0) && ([urlString length] <= 0) ) {
		[self storeResult: [self resultForQuery: anQuery withStatus: ResourceManagerLocalJobExecutorStatusInvalidParameter]];
		return;
	}
	
	if( [fileName length] > 0 ) {
		originPath = [[ResourceManager defaultManager] pathFromFileName: fileName];
	} else {
		originPath = [[ResourceManager defaultManager] pathFromUrlString: urlString];
	}
	
	if( (result = [HYResult resultWithName: self.name]) == nil ) {
		[self storeResult: [self resultForQuery: anQuery withStatus: ResourceManagerLocalJobExecutorStatusInternalError]];
		return;
	}
	
	[result setParametersFromDictionary: [anQuery paramDict]];
	[result setParameter: originPath forKey: ResourceManagerLocalJobExecutorParameterKeyFilePath];
	
	switch( dataType ) {
		case ResourceManagerLocalJobExecutorDataTypeDataObject :
			if( (data = [anQuery parameterForKey: ResourceManagerLocalJobExecutorParameterKeyDataObject]) == nil ) {
				status = ResourceManagerLocalJobExecutorStatusInvalidParameter;
			} else {
				if( [fileName length] > 0 ) {
					if( [[ResourceManager defaultManager] saveData: data forFileName: fileName cryptModuleName: cryptModuleName waitUntilDone: YES] == NO ) {
						status = ResourceManagerLocalJobExecutorStatusInternalError;
					} else {
						status = ResourceManagerLocalJobExecutorStatusSaved;
					}
				} else {
					if( [[ResourceManager defaultManager] saveData: data forUrlString: urlString cryptModuleName: cryptModuleName waitUntilDone: YES] == NO ) {
						status = ResourceManagerLocalJobExecutorStatusInternalError;
					} else {
						status = ResourceManagerLocalJobExecutorStatusSaved;
					}
				}
			}
			break;
		case ResourceManagerLocalJobExecutorDataTypeDataPath :
			if( (filePath = [anQuery parameterForKey: ResourceManagerLocalJobExecutorParameterKeyDataPath]) == nil ) {
				status = ResourceManagerLocalJobExecutorStatusInvalidParameter;
			} else {
				removeSourceFlag = [[anQuery parameterForKey: ResourceManagerLocalJobExecutorParameterKeyRemoveSourceFile] boolValue];
				if( [fileName length] > 0 ) {
					if( [[ResourceManager defaultManager] saveFile: filePath forFileName: fileName cryptModuleName: cryptModuleName removeSource: removeSourceFlag waitUntilDone: YES] == NO ) {
						status = ResourceManagerLocalJobExecutorStatusInternalError;
					} else {
						status = ResourceManagerLocalJobExecutorStatusSaved;
					}
				} else {
					if( [[ResourceManager defaultManager] saveFile: filePath forUrlString: urlString cryptModuleName: cryptModuleName removeSource: removeSourceFlag waitUntilDone: YES] == NO ) {
						status = ResourceManagerLocalJobExecutorStatusInternalError;
					} else {
						status = ResourceManagerLocalJobExecutorStatusSaved;
					}
				}
			}
			break;
		default :
			status = ResourceManagerLocalJobExecutorStatusInvalidParameter;
			break;
	}
	
	[result setParameter: [NSNumber numberWithInteger: status] forKey: ResourceManagerLocalJobExecutorParameterKeyStatus];
	
	[self storeResult: result];
}

- (void) removeWithQuery: (id)anQuery
{
	NSNumber				*boundarySizeValue;
	NSNumber				*timeValue;
	BOOL					force;
	HYResult				*result;
	
	if( (result = [HYResult resultWithName: self.name]) == nil ) {
		[self storeResult: [self resultForQuery: anQuery withStatus: ResourceManagerLocalJobExecutorStatusInternalError]];
		return;
	}
	
	force = [[anQuery parameterForKey: ResourceManagerLocalJobExecutorParameterKeyForce] boolValue];
	boundarySizeValue = [anQuery parameterForKey: ResourceManagerLocalJobExecutorParameterKeyBoundarySize];
	timeValue = [anQuery parameterForKey: ResourceManagerLocalJobExecutorParameterKeyTime];
	
	if( (boundarySizeValue == nil) && (timeValue == nil) ) {
		if( [[ResourceManager defaultManager] removeAllItems: force waitUntilDone: YES] == NO ) {
			[self storeResult: [self resultForQuery: anQuery withStatus: ResourceManagerLocalJobExecutorStatusInternalError]];
			return;
		}
	} else {
		if( boundarySizeValue != nil ) {
			if( [[ResourceManager defaultManager] removeItemsForMaximumSizeBoundary: [boundarySizeValue unsignedIntegerValue] force: force waitUntilDone: YES] == NO ) {
				[self storeResult: [self resultForQuery: anQuery withStatus: ResourceManagerLocalJobExecutorStatusInternalError]];
				return;
			}
		}
		if( timeValue != nil ) {
			if( [[ResourceManager defaultManager] removeItemsForElapsedTimeFromNow: (NSTimeInterval)[timeValue doubleValue] force: force waitUntilDone: YES] == NO ) {
				[self storeResult: [self resultForQuery: anQuery withStatus: ResourceManagerLocalJobExecutorStatusInternalError]];
				return;
			}
		}
	}
		
	[self storeResult: [self resultForQuery: anQuery withStatus: ResourceManagerLocalJobExecutorStatusRemoved]];
}

- (void) amountItemSizeWithQuery: (id)anQuery
{
	HYResult				*result;
	NSUInteger				amountItemSize;
	
	if( (result = [HYResult resultWithName: self.name]) == nil ) {
		[self storeResult: [self resultForQuery: anQuery withStatus: ResourceManagerLocalJobExecutorStatusInternalError]];
		return;
	}
	
	amountItemSize = [[ResourceManager defaultManager] amountSizeOfAllItems: YES];
	
	[anQuery setParameter: [NSNumber numberWithUnsignedInteger: amountItemSize] forKey: ResourceManagerLocalJobExecutorParameterKeyAmounItemSize];
	
	[self storeResult: [self resultForQuery: anQuery withStatus: ResourceManagerLocalJobExecutorStatusAmountItemSize]];
}

@end
