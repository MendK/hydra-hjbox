//
//  ResourceManagerLocalJobExecutor.h
//  HJBox
//
//  Created by Na Tae Hyun on 13. 4. 18..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import <UIKit/UIKit.h>
#import <Hydra/Hydra.h>


#define		ResourceManagerLocalJobExecutorName										@"resourceManagerLocalJobExecutorName"

#define		ResourceManagerLocalJobExecutorParameterKeyOperation					@"resourceManagerLocalJobExecutorParameterKeyOperation"
#define		ResourceManagerLocalJobExecutorParameterKeyDataType						@"resourceManagerLocalJobExecutorParameterKeyDataType"
#define		ResourceManagerLocalJobExecutorParameterKeyStatus						@"resourceManagerLocalJobExecutorParameterKeyStatus"
#define		ResourceManagerLocalJobExecutorParameterKeyFileName						@"resourceManagerLocalJobExecutorParameterFileName"
#define		ResourceManagerLocalJobExecutorParameterKeyUrlString					@"resourceManagerLocalJobExecutorParameterKeyUrlString"
#define		ResourceManagerLocalJobExecutorParameterKeyFilePath						@"resourceManagerLocalJobExecutorParameterFilePath"
#define		ResourceManagerLocalJobExecutorParameterKeyCryptModuleName				@"resourceManagerLocalJobExecutorParameterCryptModuleName"
#define		ResourceManagerLocalJobExecutorParameterKeyDataObject					@"resourceManagerLocalJobExecutorParameterKeyDataObject"
#define		ResourceManagerLocalJobExecutorParameterKeyDataPath						@"resourceManagerLocalJobExecutorParameterKeyDataPath"
#define		ResourceManagerLocalJobExecutorParameterKeyRemoveSourceFile				@"resourceManagerLocalJobExecutorParameterKeyRemoveSoureFile"
#define		ResourceManagerLocalJobExecutorParameterKeyTime							@"resourceManagerLocalJobExecutorParameterKeyTime"
#define		ResourceManagerLocalJobExecutorParameterKeyBoundarySize					@"resourceManagerLocalJobExecutorParameterKeyBoundarySize"
#define		ResourceManagerLocalJobExecutorParameterKeyForce						@"resourceManagerLocalJobExecutorParameterKeyForce"
#define		ResourceManagerLocalJobExecutorParameterKeyAmounItemSize				@"resourceManagerLocalJobExecutorParameterKeyAmountItemSize"


typedef enum _ResourceManagerLocalJobExecutorOperation_
{
	ResourceManagerLocalJobExecutorOperationDummy,
	ResourceManagerLocalJobExecutorOperationSave,
	ResourceManagerLocalJobExecutorOperationRemove,
	ResourceManagerLocalJobExecutorOperationAmountItemSize,
	kCountOfResourceManagerLocalJobExecutorOperation
	
} ResourceManagerLocalJobExecutorOperation;

typedef enum _ResourceManagerLocalJobExecutorDataType_
{
	ResourceManagerLocalJobExecutorDataTypeDummy,
	ResourceManagerLocalJobExecutorDataTypeDataObject,
	ResourceManagerLocalJobExecutorDataTypeDataPath,
	kCountOfResourceManagerLocalJobExecutorDataType
	
} ResourceManagerLocalJobExecutorDataType;

typedef enum _ResourceManagerLocalJobExecutorStatus_
{
	ResourceManagerLocalJobExecutorStatusDummy,
	ResourceManagerLocalJobExecutorStatusSaved,
	ResourceManagerLocalJobExecutorStatusRemoved,
	ResourceManagerLocalJobExecutorStatusAmountItemSize,
	ResourceManagerLocalJobExecutorStatusCanceled,
	ResourceManagerLocalJobExecutorStatusExpired,
	ResourceManagerLocalJobExecutorStatusUnknownOperation,
	ResourceManagerLocalJobExecutorStatusInvalidParameter,
	ResourceManagerLocalJobExecutorStatusInternalError,
	kCountOfResourceManagerLocalJobExecutorStatus
	
} ResourceManagerLocalJobExecutorStatus;


@interface ResourceManagerLocalJobExecutor : HYExecuter

@end
