//
//  ResourceManager.m
//  HJBox
//
//  Created by Na Tae Hyun on 13. 4. 16..
//  Copyright (c) 2013년 Na Tae Hyun. All rights reserved.
//
//  Licensed under the MIT license.

#import <mach/mach.h>
#import <mach/mach_host.h>
#import <CommonCrypto/CommonHMAC.h>
#import "AsyncHttpDeliverer.h"
#import "ResourceManagerLocalJobExecutor.h"
#import "ResourceManagerRemoteJobExecutor.h"
#import "ResourceManager.h"


#define			kMiminumURLStringLength					8
#define			kRemakerPathComponent					@"remaker"


ResourceManager		*g_defaultResourceManager;


@interface ResourceManager( ResourceManagerPrivate )

- (NSUInteger) amountSizeForDirectory: (NSString *)path;
- (NSString *) retinaImagePathForOriginalPath: (NSString *)path;
- (void) setRemakedPath: (NSString *)remakedPath withOriginalPath: (NSString *)originalPath;
- (void) setData: (id)data forKey: (NSString *)key;
- (void) balancingWithLimitSizeOfMemory;
- (void) touchLoadedResourceForPath: (NSString *)path;
- (NSMutableDictionary *) localJobHandlerWithResult: (HYResult *)result;
- (NSMutableDictionary *) remoteJobHandlerWithResult: (HYResult *)result;
- (NSString *) urlHashKeyFromUrlString: (NSString *)urlString;
- (id) resourceFromFilePath: (NSString *)path cryptModuleName:(NSString *)cryptModuleName waitUntilDone:(BOOL)waitUntilDone dataType: (ResourceManagerDataType)dataType onlyCheckMemoryCache: (BOOL)onlyCheckMemoryCache;
- (id) resourceFromUrlString: (NSString *)urlString remakerName: (NSString *)remakerName remakerParameter: (id)remakerParameter cryptModuleName:(NSString *)cryptModuleName ifHaveDataWaitUntilDone:(BOOL)ifHaveDataWaitUntilDone dataType: (ResourceManagerDataType)dataType;

@end


@implementation ResourceManager

@synthesize repositoryPath = _repositoryPath;
@synthesize standby = _standby;
@synthesize isRetina = _isRetina;
@synthesize usedMemorySize = _usedMemorySize;
@dynamic limitSizeOfMemory;

- (NSString *) name
{
	return ResourceManagerNotification;
}

- (NSString *) brief
{
	return @"resource manager";
}

- (BOOL) didInit
{
	if( (_loadedResourceDict = [[NSMutableDictionary alloc] init]) == nil ) {
		return NO;
	}
	if( (_referenceOrder = [[NSMutableArray alloc] init]) == nil ) {
		return NO;
	}
	if( (_requestingUrlDict = [[NSMutableDictionary alloc] init]) == nil ) {
		return NO;
	}
	if( (_reservedRequestDict = [[NSMutableDictionary alloc] init]) == nil ) {
		return NO;
	}
	if( (_remakedPathDict = [[NSMutableDictionary alloc] init]) == nil ) {
		return NO;
	}
	if( (_lockForResourceDict = [[NSLock alloc] init]) == nil ) {
		return NO;
	}
	if( (_loadedUrlHashKeyDict = [[NSMutableDictionary alloc] init]) == nil ) {
		return NO;
	}
	if( (_lockForUrlHashKeyDict = [[NSLock alloc] init]) == nil ) {
		return NO;
	}
	if( (_remakerDict = [[NSMutableDictionary alloc] init]) == nil ) {
		return NO;
	}
	if( (_cryptModuleDict = [[NSMutableDictionary alloc] init]) == nil ) {
		return NO;
	}
	if( (_lockForSupportDict = [[NSLock alloc] init]) == nil ) {
		return NO;
	}
	
	return YES;
}

- (void) willDealloc
{
	[self sync];
}

- (NSUInteger) amountSizeForDirectory: (NSString *)path
{
	NSUInteger		amountSize;
	NSArray			*list;
	NSString		*fileName;
	NSString		*filePath;
	NSDictionary	*dict;
	
	if( [path length] <= 0 ) {
		return 0;
	}
	
	amountSize = 0;
	
	if( (list = [[NSFileManager defaultManager] contentsOfDirectoryAtPath: path error: nil]) == nil ) {
		return 0;
	}
	
	for( fileName in list ) {
		if( [[fileName substringToIndex: 1] isEqualToString: @"."] == YES ) {
			continue;
		}
		filePath = [path stringByAppendingPathComponent: fileName];
		if( (dict = [[NSFileManager defaultManager] attributesOfItemAtPath: filePath error: nil]) == nil ) {
			continue;
		}
		amountSize += (NSUInteger)[dict fileSize];
	}
	
	return amountSize;
}

- (NSString *) retinaImagePathForOriginalPath: (NSString *)path
{
	NSString		*fileName;
	NSString		*fileExtension;
	NSString		*retinaPath;
	
	if( [path length] <= 0 ) {
		return nil;
	}
	
	fileExtension = [path pathExtension];
	
	if( [fileExtension length] > 0 ) {
		fileName = [path stringByDeletingPathExtension];
		retinaPath = [NSString stringWithFormat: @"%@@2x.%@", fileName, fileExtension];
	} else {
		retinaPath = [NSString stringWithFormat: @"%@@2x", path];
	}
	
	return retinaPath;
}

- (void) setRemakedPath: (NSString *)remakedPath withOriginalPath: (NSString *)originalPath
{
	NSMutableDictionary		*subDict;
	
	if( (subDict = [_remakedPathDict objectForKey: originalPath]) == nil ) {
		if( (subDict = [[NSMutableDictionary alloc] init]) != nil ) {
			[_remakedPathDict setObject: subDict forKey: originalPath];
		}
	}
	if( subDict != nil ) {
		[subDict setObject: @"Y" forKey: remakedPath];
	}
}

- (void) setData: (id)data forKey: (NSString *)key
{
	id				oldData;
	NSUInteger		size;
	
	if( (data == nil) || ([key length] <= 0) ) {
		return;
	}
	
	if( (oldData = [_loadedResourceDict objectForKey: key]) != nil ) {
		if( [oldData isKindOfClass: [NSString class]] == YES ) {
			size = [oldData lengthOfBytesUsingEncoding: NSUTF8StringEncoding];
		} else if( [oldData isKindOfClass: [UIImage class]] == YES ) {
			size = (NSUInteger)(CGImageGetBytesPerRow(((UIImage *)oldData).CGImage)*CGImageGetHeight(((UIImage *)oldData).CGImage));
		} else if( [oldData isKindOfClass: [NSData class]] == YES ) {
			size = [oldData length];
		} else {
			size = 0;
		}
		_usedMemorySize -= size;
	} else {
		[_referenceOrder insertObject: key atIndex: 0];
	}
	
	if( [data isKindOfClass: [NSString class]] == YES ) {
		size = [data lengthOfBytesUsingEncoding: NSUTF8StringEncoding];
	} else if( [data isKindOfClass: [UIImage class]] == YES ) {
		size = (NSUInteger)(CGImageGetBytesPerRow(((UIImage *)data).CGImage)*CGImageGetHeight(((UIImage *)data).CGImage));
	} else if( [data isKindOfClass: [NSData class]] == YES ) {
		size = [data length];
	} else {
		size = 0;
	}
	_usedMemorySize += size;
	
	[_loadedResourceDict setObject: data forKey: key];
	
	if( oldData != nil ) {
		[self touchLoadedResourceForPath: key];
	}
	[self balancingWithLimitSizeOfMemory];
}

- (void) balancingWithLimitSizeOfMemory
{
	NSString	*path;
	id			anObject;
	UIImage		*image;
	NSUInteger	size;
	
	if( _limitSizeOfMemory == 0 ) {
		[_loadedResourceDict removeAllObjects];
		[_referenceOrder removeAllObjects];
		_usedMemorySize = 0;
	} else {
		if( _usedMemorySize > _limitSizeOfMemory ) {
			while( [_referenceOrder count] > 0 ) {
				path = [_referenceOrder lastObject];
				anObject = [_loadedResourceDict objectForKey: path];
				if( [anObject isKindOfClass: [NSString class]] == YES ) {
					size = [anObject lengthOfBytesUsingEncoding: NSUTF8StringEncoding];
				} else if( [anObject isKindOfClass: [UIImage class]] == YES ) {
					image = (UIImage *)anObject;
					size = (NSUInteger)(CGImageGetBytesPerRow(image.CGImage)*CGImageGetHeight(image.CGImage));
				} else if( [anObject isKindOfClass: [NSData class]] == YES ) {
					size = [anObject length];
				} else {
					size = 0;
				}
				_usedMemorySize -= size;
				[_loadedResourceDict removeObjectForKey: path];
				[_referenceOrder removeLastObject];
				if( _usedMemorySize <= _limitSizeOfMemory ) {
					break;
				}
			}
		}
	}
}

- (void) touchLoadedResourceForPath: (NSString *)path
{
	NSUInteger		i, count;
	
	count = [_referenceOrder count];
	
	for( i=0 ; i<count ; ++i ) {
		if( [[_referenceOrder objectAtIndex: i] isEqualToString: path] == YES ) {
			[_referenceOrder removeObjectAtIndex: i];
			[_referenceOrder insertObject: path atIndex: 0];
			break;
		}
	}
}

- (NSMutableDictionary *) localJobHandlerWithResult: (HYResult *)result
{
	NSMutableDictionary							*paramDict;
	ResourceManagerLocalJobExecutorOperation	operation;
	ResourceManagerLocalJobExecutorStatus		status;
	NSString									*fileName;
	NSString									*cryptModuleName;
	ResourceManagerLocalJobExecutorDataType		dataType;
	NSString									*filePath;
	
	if( result == nil ) {
		return nil;
	}
	
	if( (paramDict = [[NSMutableDictionary alloc] init]) == nil ) {
		return nil;
	}
		
	operation = (ResourceManagerLocalJobExecutorOperation)[[result parameterForKey: ResourceManagerLocalJobExecutorParameterKeyOperation] integerValue];
	status = (ResourceManagerLocalJobExecutorStatus)[[result parameterForKey: ResourceManagerLocalJobExecutorParameterKeyStatus] integerValue];
	fileName = [result parameterForKey: ResourceManagerLocalJobExecutorParameterKeyFileName];
	filePath = [result parameterForKey: ResourceManagerLocalJobExecutorParameterKeyFilePath];
	cryptModuleName = [result parameterForKey: ResourceManagerLocalJobExecutorParameterKeyCryptModuleName];
	dataType = (ResourceManagerLocalJobExecutorDataType)[[result parameterForKey: ResourceManagerLocalJobExecutorParameterKeyDataType] integerValue];
	
	switch( operation ) {
		case ResourceManagerLocalJobExecutorOperationSave :
			if( status == ResourceManagerLocalJobExecutorStatusSaved ) {
				[paramDict setObject: [NSNumber numberWithInteger: (NSInteger)ResourceManagerRequestStatusSavingDone] forKey: ResourceManagerParameterKeyStatus];
			} else {
				[paramDict setObject: [NSNumber numberWithInteger: (NSInteger)ResourceManagerRequestStatusSavingFailed] forKey: ResourceManagerParameterKeyStatus];
			}
			break;
		case ResourceManagerLocalJobExecutorOperationRemove :
			if( status == ResourceManagerLocalJobExecutorStatusRemoved ) {
				[paramDict setObject: [NSNumber numberWithInteger: (NSInteger)ResourceManagerRequestStatusRemovingDone] forKey: ResourceManagerParameterKeyStatus];
			} else {
				[paramDict setObject: [NSNumber numberWithInteger: (NSInteger)ResourceManagerRequestStatusRemovingFailed] forKey: ResourceManagerParameterKeyStatus];
			}
			break;
		case ResourceManagerLocalJobExecutorOperationAmountItemSize :
			[paramDict setObject: [NSNumber numberWithInteger: (NSInteger)ResourceManagerRequestStatusGotAmountItemSize] forKey: ResourceManagerParameterKeyStatus];
			[paramDict setObject: [result parameterForKey: ResourceManagerLocalJobExecutorParameterKeyAmounItemSize] forKey: ResourceManagerParameterKeyAmountItemSize];
			break;
		default :
			return nil;
	}
	
	if( [fileName length] > 0 ) {
		[paramDict setObject: fileName forKey: ResourceManagerParameterKeyFileName];
	}
	if( [filePath length] > 0 ) {
		[paramDict setObject: filePath forKey: ResourceManagerParameterKeyDataPath];
	}
	if( [cryptModuleName length] > 0 ) {
		[paramDict setObject: cryptModuleName forKey: ResourceManagerParameterKeyCryptMoudleName];
	}
	[paramDict setObject: [NSNumber numberWithInteger: (NSInteger)dataType] forKey: ResourceManagerParameterKeyDataType];
	
	return paramDict;
}

- (NSMutableDictionary *) remoteJobHandlerWithResult: (HYResult *)result
{
	NSMutableDictionary							*paramDict;
	BOOL										removeUrlHashKeyFromRequestingPathDict;
	BOOL										dataPrepared;
	ResourceManagerRemoteJobExecutorOperation	operation;
	ResourceManagerRemoteJobExecutorStatus		status;
	NSString									*urlString;
	NSString									*fileName;
	NSString									*remakerName;
	id											remakerParameter;
	NSString									*cryptModuleName;
	ResourceManagerRemoteJobExecutorDataType	dataType;
	id											dataObject;
	NSString									*filePath;
	NSUInteger									asyncHttpDelivererIssuedId;
	NSMutableArray								*list;
	NSArray										*subList;
	NSString									*originalPath;
	
	if( result == nil ) {
		return nil;
	}
	
	if( (paramDict = [[NSMutableDictionary alloc] init]) == nil ) {
		return nil;
	}
	
	removeUrlHashKeyFromRequestingPathDict = NO;
	dataPrepared = NO;
	
	operation = (ResourceManagerRemoteJobExecutorOperation)[[result parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyOperation] integerValue];
	status = (ResourceManagerRemoteJobExecutorStatus)[[result parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyStatus] integerValue];
	urlString = [result parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyUrlString];
	fileName = [result parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyFileName];
	filePath = [result parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyFilePath];
	remakerName = [result parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyRemakerName];
	remakerParameter = [result parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyRemakerParameter];
	cryptModuleName = [result parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyCryptModuleName];
	dataType = (ResourceManagerRemoteJobExecutorDataType)[[result parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyDataType] integerValue];
	asyncHttpDelivererIssuedId = [[result parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyAsyncHttpDelivererIssuedId] unsignedIntegerValue];
	
	switch( operation ) {
		case ResourceManagerRemoteJobExecutorOperationRequest :
			if( status == ResourceManagerRemoteJobExecutorStatusRequested ) {
				[paramDict setObject: [NSNumber numberWithInteger: (NSInteger)ResourceManagerRequestStatusDownloadingStart] forKey: ResourceManagerParameterKeyStatus];
			} else if( status == ResourceManagerRemoteJobExecutorStatusReceived ) {
				[paramDict setObject: [NSNumber numberWithInteger: (NSInteger)ResourceManagerRequestStatusDataPrepared] forKey: ResourceManagerParameterKeyStatus];
				removeUrlHashKeyFromRequestingPathDict = YES;
				dataPrepared = YES;
			} else {
				[paramDict setObject: [NSNumber numberWithInteger: (NSInteger)ResourceManagerRequestStatusDownloadingFailed] forKey: ResourceManagerParameterKeyStatus];
				removeUrlHashKeyFromRequestingPathDict = YES;
			}
			break;
		case ResourceManagerRemoteJobExecutorOperationReceive :
			if( status == ResourceManagerRemoteJobExecutorStatusReceived ) {
				[paramDict setObject: [NSNumber numberWithInteger: (NSInteger)ResourceManagerRequestStatusDataPrepared] forKey: ResourceManagerParameterKeyStatus];
			} else {
				[paramDict setObject: [NSNumber numberWithInteger: (NSInteger)ResourceManagerRequestStatusDownloadingFailed] forKey: ResourceManagerParameterKeyStatus];
			}
			removeUrlHashKeyFromRequestingPathDict = YES;
			dataPrepared = YES;
			break;
		default :
			return nil;
	}
	
	if( dataPrepared == YES ) {
		switch( dataType ) {
			case ResourceManagerRemoteJobExecutorDataTypeString :
				if( (dataObject = [result parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyString]) != nil ) {
					[paramDict setObject: [NSNumber numberWithInteger: ResourceManagerDataTypeString] forKey: ResourceManagerParameterKeyDataType];
					[paramDict setObject: dataObject forKey: ResourceManagerParameterKeyDataObject];
				}
				break;
			case ResourceManagerRemoteJobExecutorDataTypeImage :
				if( (dataObject = [result parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyImage]) != nil ) {
					[paramDict setObject: [NSNumber numberWithInteger: ResourceManagerDataTypeImage] forKey: ResourceManagerParameterKeyDataType];
					[paramDict setObject: dataObject forKey: ResourceManagerParameterKeyDataObject];
				}
				break;
			case ResourceManagerRemoteJobExecutorDataTypeDataObject :
				if( (dataObject = [result parameterForKey: ResourceManagerRemoteJobExecutorParameterKeyDataObject]) != nil ) {
					[paramDict setObject: [NSNumber numberWithInteger: ResourceManagerDataTypeData] forKey: ResourceManagerParameterKeyDataType];
					[paramDict setObject: dataObject forKey: ResourceManagerParameterKeyDataObject];
				}
				break;
			default :
				break;
		}
		if( [filePath length] > 0 ) {
			[_lockForResourceDict lock];
			if( ([urlString length] > 0) && ([remakerName isEqualToString: ResourceManagerOriginalRemakerName] == NO) ) {
				if( (originalPath = [self pathFromUrlString: urlString]) != nil ) {
					[self setRemakedPath: filePath withOriginalPath: originalPath];
				}
			}
			[self setData: dataObject forKey: filePath];
			[_lockForResourceDict unlock];
		}
	}
	
	if( [urlString length] > 0 ) {
		[paramDict setObject: urlString forKey: ResourceManagerParameterKeyUrlString];
	}
	if( [fileName length] > 0 ) {
		[paramDict setObject: fileName forKey: ResourceManagerParameterKeyFileName];
	}
	if( [filePath length] > 0 ) {
		[paramDict setObject: filePath forKey: ResourceManagerParameterKeyDataPath];
	}
	if( [remakerName length] > 0 ) {
		[paramDict setObject: remakerName forKey: ResourceManagerParameterKeyRemakerName];
	}
	if( remakerParameter != nil ) {
		[paramDict setObject: remakerParameter forKey: ResourceManagerParameterKeyRemakerParameter];
	}
	if( [cryptModuleName length] > 0 ) {
		[paramDict setObject: cryptModuleName forKey: ResourceManagerParameterKeyCryptMoudleName];
	}
	if( asyncHttpDelivererIssuedId > 0 ) {
		[paramDict setObject: [NSNumber numberWithUnsignedInteger: asyncHttpDelivererIssuedId] forKey: ResourceManagerParameterKeyAsyncHttpDelivererIssuedId];
	}
	
	if( (removeUrlHashKeyFromRequestingPathDict == YES) && ([urlString length] > 0) ) {
		[_lockForResourceDict lock];
		[_requestingUrlDict removeObjectForKey: urlString];
		list = [_reservedRequestDict objectForKey: urlString];
		if( [list count] > 0 ) {
			subList = [list objectAtIndex: 0];
			remakerName = [subList objectAtIndex: 0];
			if( [subList count] > 1 ) {
				remakerParameter = [subList objectAtIndex: 1];
			} else {
				remakerParameter = nil;
			}
			[list removeObjectAtIndex: 0];
			if( [list count] <= 0 ) {
				[_reservedRequestDict removeObjectForKey: urlString];
			}
		} else {
			remakerName = nil;
		}
		[_lockForResourceDict unlock];
		if( [remakerName length] > 0 ) {
			switch( dataType ) {
				case ResourceManagerRemoteJobExecutorDataTypeString :
					[self resourceFromUrlString: urlString remakerName: remakerName remakerParameter: remakerParameter cryptModuleName: cryptModuleName ifHaveDataWaitUntilDone: NO dataType: ResourceManagerDataTypeString];
					break;
				case ResourceManagerRemoteJobExecutorDataTypeImage :
					[self resourceFromUrlString: urlString remakerName: remakerName remakerParameter: remakerParameter cryptModuleName: cryptModuleName ifHaveDataWaitUntilDone: NO dataType: ResourceManagerDataTypeImage];
					break;
				case ResourceManagerRemoteJobExecutorDataTypeDataObject :
					[self resourceFromUrlString: urlString remakerName: remakerName remakerParameter: remakerParameter cryptModuleName: cryptModuleName ifHaveDataWaitUntilDone: NO dataType: ResourceManagerDataTypeData];
					break;
				default :
					break;
			}
		}
	}
	
	return paramDict;
}

- (NSString *) urlHashKeyFromUrlString: (NSString *)urlString
{
	const char		*utf8String;
	unsigned char	pattern[16];
	NSString		*urlHashKey;
	
	if( self.standby == NO ) {
		return nil;
	}
	
	if( [urlString length] <= 0 ) {
		return nil;
	}
	
	if( (utf8String = [urlString UTF8String]) != NULL ) {
		CC_MD5( utf8String, (CC_LONG)strlen(utf8String), pattern );
		urlHashKey = [NSString stringWithFormat: @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
					  pattern[0], pattern[1], pattern[2], pattern[3], pattern[4], pattern[5], pattern[6], pattern[7],
					  pattern[8], pattern[9], pattern[10], pattern[11], pattern[12], pattern[13], pattern[14], pattern[15]];
	} else {
		urlHashKey = nil;
	}
	
	return urlHashKey;
}

- (id) resourceFromFilePath: (NSString *)path cryptModuleName:(NSString *)cryptModuleName waitUntilDone:(BOOL)waitUntilDone dataType: (ResourceManagerDataType)dataType onlyCheckMemoryCache: (BOOL)onlyCheckMemoryCache
{
	id				anObject;
	NSData			*data;
	NSString		*retinaPath;
	BOOL			retinaLoaded;
	
	if( [path length] <= 0 ) {
		return nil;
	}
	
	[_lockForResourceDict lock];
	
	anObject = [_loadedResourceDict objectForKey: path];
	retinaLoaded = NO;
	
	switch( dataType ) {
		case ResourceManagerDataTypeString :
			if( [anObject isKindOfClass: [NSData class]] == NO ) {
				anObject = nil;
			}
			break;
		case ResourceManagerDataTypeImage :
			if( [anObject isKindOfClass: [UIImage class]] == NO ) {
				anObject = nil;
			}
			break;
		case ResourceManagerDataTypeData :
			if( [anObject isKindOfClass: [NSData class]] == NO ) {
				anObject = nil;
			}
			break;
		default :
			anObject = nil;
			break;
	}
	
	if( onlyCheckMemoryCache == YES ) {
		[_lockForResourceDict unlock];
		return anObject;
	}
	
	if( anObject == nil ) {
		if( [cryptModuleName length] > 0 ) {
			data = [self decryptDataFromFilePath: path forName: cryptModuleName];
		} else {
			if( dataType == ResourceManagerDataTypeImage ) {
				if( self.isRetina == YES ) {
					if( (retinaPath = [self retinaImagePathForOriginalPath: path]) != nil ) {
						if( (data = [NSData dataWithContentsOfFile: retinaPath]) != nil ) {
							retinaLoaded = YES;
						}
					} else {
						data = nil;
					}
				}
				if( data == nil ) {
					data = [NSData dataWithContentsOfFile: path];
				}
			} else {
				data = [NSData dataWithContentsOfFile: path];
			}
		}
		if( data != nil ) {
			switch( dataType ) {
				case ResourceManagerDataTypeString :
					anObject = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
					break;
				case ResourceManagerDataTypeImage :
					if( retinaLoaded == NO ) {
						anObject = [UIImage imageWithData: data];
					} else {
						anObject = [UIImage imageWithCGImage: [[UIImage imageWithData:data] CGImage] scale: 2.0f orientation: UIImageOrientationUp];
					}
					break;
				case ResourceManagerDataTypeData :
					anObject = data;
					break;
				default :
					anObject = nil;
					break;
			}
		}
		if( anObject != nil ) {
			[self setData: anObject forKey: path];
		}
	} else {
		[self touchLoadedResourceForPath: path];
	}
	
	[_lockForResourceDict unlock];
	
	return anObject;
}

- (id) resourceFromUrlString: (NSString *)urlString remakerName: (NSString *)remakerName remakerParameter: (id)remakerParameter cryptModuleName:(NSString *)cryptModuleName ifHaveDataWaitUntilDone:(BOOL)ifHaveDataWaitUntilDone dataType: (ResourceManagerDataType)dataType
{
	NSString									*filePath;
	ResourceManagerRemoteJobExecutorDataType	executorDataType;
	id											anObject;
	HYQuery										*query;
	NSMutableDictionary							*paramDict;
	NSMutableArray								*list;
	NSArray										*subList;
	NSString									*originalPath;
	
	if( [urlString length] <= 0 ) {
		return nil;
	}
	if( [remakerName length] <= 0 ) {
		remakerName = ResourceManagerOriginalRemakerName;
	}
	
	filePath = [self pathFromUrlString: urlString remakerName: remakerName remakerParameter: remakerParameter];
	
	if( (anObject = [self resourceFromFilePath: filePath cryptModuleName: cryptModuleName waitUntilDone: YES dataType: dataType onlyCheckMemoryCache: YES]) != nil ) {
		if( ([urlString length] > 0) && ([remakerName isEqualToString: ResourceManagerOriginalRemakerName] == NO) ) {
			if( (originalPath = [self pathFromUrlString: urlString]) != nil ) {
				[_lockForResourceDict lock];
				[self setRemakedPath: filePath withOriginalPath: originalPath];
				[_lockForResourceDict unlock];
			}
		}
		return anObject;
	}
	
	if( ifHaveDataWaitUntilDone == YES ) {
		if( (anObject = [self resourceFromFilePath: filePath cryptModuleName: cryptModuleName waitUntilDone: YES dataType: dataType onlyCheckMemoryCache: NO]) != nil ) {
			if( ([urlString length] > 0) && ([remakerName isEqualToString: ResourceManagerOriginalRemakerName] == NO) ) {
				if( (originalPath = [self pathFromUrlString: urlString]) != nil ) {
					[_lockForResourceDict lock];
					[self setRemakedPath: filePath withOriginalPath: originalPath];
					[_lockForResourceDict unlock];
				}
			}
			return anObject;
		}
	}
	
	[_lockForResourceDict lock];
	
	if( [_requestingUrlDict objectForKey: urlString] != nil ) {
		
		if( [remakerName isEqualToString: ResourceManagerOriginalRemakerName] == YES ) {
			
			[_lockForResourceDict unlock];
			
			if( (paramDict = [[NSMutableDictionary alloc] init]) != nil ) {
				[paramDict setObject: [NSNumber numberWithInteger:(NSInteger)ResourceManagerRequestStatusRequestSkipped] forKey: ResourceManagerParameterKeyStatus];
				[paramDict setObject: urlString forKey: ResourceManagerParameterKeyUrlString];
				[paramDict setObject: remakerName forKey: ResourceManagerParameterKeyRemakerName];
				if( remakerParameter != nil ) {
					[paramDict setObject: remakerParameter forKey: ResourceManagerParameterKeyRemakerParameter];
				}
				if( [cryptModuleName length] > 0 ) {
					[paramDict setObject: cryptModuleName forKey: ResourceManagerParameterKeyCryptMoudleName];
				}
				[self performSelectorOnMainThread: @selector(postNotifyWithParamDict:) withObject: paramDict waitUntilDone: NO];
			}
			
		} else {
			
			if( (list = [_reservedRequestDict objectForKey: urlString]) == nil ) {
				if( (list = [[NSMutableArray alloc] init]) != nil ) {
					[_reservedRequestDict setObject: list forKey: urlString];
				}
			}
			if( list != nil ) {
				if( remakerParameter == nil ) {
					subList = [NSArray arrayWithObjects: remakerName, nil];
				} else {
					subList = [NSArray arrayWithObjects: remakerName, remakerParameter, nil];
				}
				if( subList != nil ) {
					[list addObject: subList];
				}
			}
			
			[_lockForResourceDict unlock];
			
		}
		
	} else {
		
		[_requestingUrlDict setObject: @"Y" forKey: urlString];
		
		[_lockForResourceDict unlock];
		
		if( (query = [self queryForExecutorName: ResourceManagerRemoteJobExecutorName]) != nil ) {
			
			if( [remakerName isEqualToString: ResourceManagerOriginalRemakerName] == YES ) {
				[self removeItemForUrlString: urlString force: YES];
			}
			
			switch( dataType ) {
				case ResourceManagerDataTypeString :
					executorDataType = ResourceManagerRemoteJobExecutorDataTypeString;
					break;
				case ResourceManagerDataTypeImage :
					executorDataType = ResourceManagerRemoteJobExecutorDataTypeImage;
					break;
				default :
					executorDataType = ResourceManagerRemoteJobExecutorDataTypeDataObject;
					break;
			}
			
			[query setParameter: [NSNumber numberWithInteger: ResourceManagerRemoteJobExecutorOperationRequest] forKey: ResourceManagerRemoteJobExecutorParameterKeyOperation];
			[query setParameter: [NSNumber numberWithInteger: executorDataType] forKey: ResourceManagerRemoteJobExecutorParameterKeyDataType];
			[query setParameter: urlString forKey: ResourceManagerRemoteJobExecutorParameterKeyUrlString];
			[query setParameter: remakerName forKey: ResourceManagerRemoteJobExecutorParameterKeyRemakerName];
			[query setParameter: remakerParameter forKey: ResourceManagerRemoteJobExecutorParameterKeyRemakerParameter];
			[query setParameter: cryptModuleName forKey: ResourceManagerRemoteJobExecutorParameterKeyCryptModuleName];
			[[Hydra defaultHydra] pushQuery: query];
			
		} else {
			
			[_lockForResourceDict lock];
			[_requestingUrlDict removeObjectForKey: urlString];
			[_lockForResourceDict unlock];
			
		}
		
	}
	
	return nil;
}

+ (ResourceManager *) defaultManager
{
	@synchronized( self ) {
		if( g_defaultResourceManager == nil ) {
			g_defaultResourceManager = [[ResourceManager alloc] init];
		}
	}
	
	return g_defaultResourceManager;
}

- (BOOL) standbyWithRepositoryPath: (NSString *)path localJobWorkerName: (NSString *)localJobWorkerName remoteJobWorkerName: (NSString *)remoteJobWorkerName
{
	BOOL			isDirectory;
	
	if( (self.standby == YES) || ([path length] <= 0) ) {
		return NO;
	}
	
	if( [[NSFileManager defaultManager]	fileExistsAtPath: path isDirectory: &isDirectory] == NO ) {
		if( [[NSFileManager defaultManager] createDirectoryAtPath: path withIntermediateDirectories: YES attributes: nil error: nil] == NO ) {
			return NO;
		}
	} else {
		if( isDirectory == NO ) {
			return NO;
		}
	}
	
	if( [[path substringFromIndex: [path length]-1] isEqualToString: @"/"] == YES ) {
		path = [path substringToIndex: [path length]-1];
	}
	
	_repositoryPath = [path copy];
	_retainCountDictPath = [[NSString alloc] initWithFormat: @"%@/%@", _repositoryPath, ResourceManagerRetainCountFileName];
	_removeMarkDictPath = [[NSString alloc] initWithFormat: @"%@/%@", _repositoryPath, ResourceManagerRemoveMarkFileName];
	
	if( [[NSFileManager defaultManager] fileExistsAtPath: _retainCountDictPath] == YES ) {
		_retainCountDict = [[NSMutableDictionary alloc] initWithContentsOfFile: _retainCountDictPath];
	}
	if( _retainCountDict == nil ) {
		if( (_retainCountDict = [[NSMutableDictionary alloc] init]) == nil ) {
			return NO;
		}
	}
	if( [[NSFileManager defaultManager] fileExistsAtPath: _removeMarkDictPath] == YES ) {
		_removeMarkDict = [[NSMutableDictionary alloc] initWithContentsOfFile: _removeMarkDictPath];
	}
	if( _removeMarkDict == nil ) {
		if( (_removeMarkDict = [[NSMutableDictionary alloc] init]) == nil ) {
			return NO;
		}
	}
	
	[self registExecuter: [[ResourceManagerLocalJobExecutor alloc] init] withWorkerName: localJobWorkerName action: @selector(localJobHandlerWithResult:)];
	[self registExecuter: [[ResourceManagerRemoteJobExecutor alloc] init] withWorkerName: remoteJobWorkerName action: @selector(remoteJobHandlerWithResult:)];
	
	_isRetina = (([[UIScreen mainScreen] respondsToSelector: @selector(displayLinkWithTarget:selector:)] == YES) && ([UIScreen mainScreen].scale == 2.0f)) ? YES : NO;
	
	_standby = YES;
		
	return YES;
}

- (void) sync
{
	if( self.standby == NO ) {
		return;
	}
	
	if( _changed == NO ) {
		return;
	}
	
	[_retainCountDict writeToFile: _retainCountDictPath atomically: YES];
	[_removeMarkDict writeToFile: _removeMarkDictPath atomically: YES];
	
	_changed = NO;
}

- (BOOL) setRemaker: (id)remaker forName: (NSString *)name
{
	if( self.standby == NO ) {
		return NO;
	}
	
	if( (remaker == nil) || ([name length] <= 0) ) {
		return NO;
	}
	if( [name isEqualToString: ResourceManagerDownloadingTemporaryName] == YES ) {
		return NO;
	}
	if( [remaker conformsToProtocol: @protocol(ResourceManagerRemakerProtocol)] == NO ) {
		return NO;
	}
	
	[_lockForSupportDict lock];
	[_remakerDict setObject: remaker forKey: name];
	[_lockForSupportDict unlock];
	
	return YES;
}

- (void) removeRemakerForName: (NSString *)name
{
	if( self.standby == NO ) {
		return;
	}
	
	if( [name length] <= 0 ) {
		return;
	}
	
	[_lockForSupportDict lock];
	[_remakerDict removeObjectForKey: name];
	[_lockForSupportDict unlock];
}

- (void) removeAllRemakers
{
	if( self.standby == NO ) {
		return;
	}
	
	[_lockForSupportDict lock];
	[_remakerDict removeAllObjects];
	[_lockForSupportDict unlock];
}

- (BOOL) haveRemakerForName: (NSString *)name
{
	BOOL		flag;
	
	if( self.standby == NO ) {
		return NO;
	}
	
	if( [name length] <= 0 ) {
		return NO;
	}
	
	[_lockForSupportDict lock];
	flag = ([_remakerDict objectForKey: name] != nil);
	[_lockForSupportDict unlock];
	
	return flag;
}

- (NSString *) remakeIdentifierWithParameter: (id)anParameter forName: (NSString *)name
{
	id		remaker;
	
	if( self.standby == NO ) {
		return nil;
	}
	
	if( [name length] <= 0 ) {
		return nil;
	}
	
	[_lockForSupportDict lock];
	remaker = [_remakerDict objectForKey: name];
	[_lockForSupportDict unlock];
	
	return [remaker identifierWithParameter: anParameter];
}

- (NSData *) remakeData: (NSData *)anData withParameter: (id)anParameter forName: (NSString *)name
{
	id		remaker;
	
	if( self.standby == NO ) {
		return nil;
	}
	
	if( ([anData length] <= 0) || ([name length] <= 0) ) {
		return nil;
	}
	
	[_lockForSupportDict lock];
	remaker = [_remakerDict objectForKey: name];
	[_lockForSupportDict unlock];
	
	return [remaker remakerData: anData withParameter: anParameter];
}

- (BOOL) setCryptModule: (id)cryptModule forName: (NSString *)name
{
	if( self.standby == NO ) {
		return NO;
	}
	
	if( (cryptModule == nil) || ([name length] <= 0) ) {
		return NO;
	}
	if( [cryptModule conformsToProtocol: @protocol(ResourceManagerCryptProtocol)] == NO ) {
		return NO;
	}
	
	[_lockForSupportDict lock];
	[_cryptModuleDict setObject: cryptModule forKey: name];
	[_lockForSupportDict unlock];
	
	return YES;
}

- (void) removeCryptModuleForName: (NSString *)name
{
	if( self.standby == NO ) {
		return;
	}
	
	if( [name length] <= 0 ) {
		return;
	}
	
	[_lockForSupportDict lock];
	[_cryptModuleDict removeObjectForKey: name];
	[_lockForSupportDict unlock];
}

- (void) removeAllCryptModules
{
	if( self.standby == NO ) {
		return;
	}
	
	[_lockForSupportDict lock];
	[_cryptModuleDict removeAllObjects];
	[_lockForSupportDict unlock];
}

- (BOOL) haveCryptModuleForName: (NSString *)name
{
	BOOL		flag;
	
	if( self.standby == NO ) {
		return NO;
	}
	
	if( [name length] <= 0 ) {
		return NO;
	}
	
	[_lockForSupportDict lock];
	flag = ([_cryptModuleDict objectForKey: name] != nil);
	[_lockForSupportDict unlock];
		
	return flag;
}

- (NSData *) encryptData: (NSData *)anData forName: (NSString *)name
{
	id		cryptModule;
	
	if( self.standby == NO ) {
		return nil;
	}
	
	if( ([anData length] <= 0) || ([name length] <= 0) ) {
		return nil;
	}
	
	[_lockForSupportDict lock];
	cryptModule = [_cryptModuleDict objectForKey: name];
	[_lockForSupportDict unlock];
	
	return [cryptModule encryptData: anData];
}

- (NSData *) decryptData: (NSData *)anData forName: (NSString *)name
{
	id		cryptModule;
	
	if( self.standby == NO ) {
		return nil;
	}
	
	if( ([anData length] <= 0) || ([name length] <= 0) ) {
		return nil;
	}
	
	[_lockForSupportDict lock];
	cryptModule = [_cryptModuleDict objectForKey: name];
	[_lockForSupportDict unlock];
	
	return [cryptModule decryptData: anData];
}

- (BOOL) encryptData: (NSData *)anData toFilePath: (NSString *)path forName: (NSString *)name
{
	id		cryptModule;
	
	if( self.standby == NO ) {
		return NO;
	}
	
	if( ([anData length] <= 0) || ([path length] <= 0) || ([name length] <= 0 ) ) {
		return NO;
	}
	
	[_lockForSupportDict lock];
	cryptModule = [_cryptModuleDict objectForKey: name];
	[_lockForSupportDict unlock];
	
	if( cryptModule == nil ){
		return NO;
	}
	
	return [cryptModule encryptData: anData toFilePath: path];
}

- (NSData *) decryptDataFromFilePath: (NSString *)path forName: (NSString *)name
{
	id		cryptModule;
	
	if( self.standby == NO ) {
		return nil;
	}
	
	if( ([path length] <= 0) || ([name length] <= 0 ) ) {
		return nil;
	}
	
	[_lockForSupportDict lock];
	cryptModule = [_cryptModuleDict objectForKey: name];
	[_lockForSupportDict unlock];
	
	return [cryptModule decryptDataFromFilePath: path];
}

- (BOOL) saveString: (NSString *)string forFileName: (NSString *)name cryptModuleName:(NSString *)cryptModuleName waitUntilDone:(BOOL)waitUntilDone
{
	if( ([string length] <= 0) || ([name length] <= 0) ) {
		return NO;
	}
	
	return [self saveData: [string dataUsingEncoding: NSUTF8StringEncoding] forFileName: name cryptModuleName: cryptModuleName waitUntilDone: waitUntilDone];
}

- (BOOL) saveImage: (UIImage *)image forFileName: (NSString *)name cryptModuleName:(NSString *)cryptModuleName waitUntilDone:(BOOL)waitUntilDone
{
	if( (image == nil) || ([name length] <= 0) ) {
		return NO;
	}
	
	return [self saveData: UIImagePNGRepresentation(image) forFileName: name cryptModuleName: cryptModuleName waitUntilDone: waitUntilDone];
}

- (BOOL) saveData: (NSData *)data forFileName: (NSString *)name cryptModuleName:(NSString *)cryptModuleName waitUntilDone:(BOOL)waitUntilDone
{
	NSString		*folderPath;
	NSString		*filePath;
	HYQuery			*query;
	
	if( self.standby == NO ) {
		return NO;
	}
	
	if( ([data length] <= 0) || ([name length] <= 0) ) {
		return NO;
	}
	
	if( waitUntilDone == YES ) {
		if( (filePath = [self pathFromFileName: name]) == nil ) {
			return NO;
		}
		if( (folderPath = [filePath stringByDeletingLastPathComponent]) == nil ) {
			return NO;
		}
		if( [[NSFileManager defaultManager] fileExistsAtPath: folderPath] == YES ) {
			[[NSFileManager defaultManager] removeItemAtPath: folderPath error: nil];
		}
		[[NSFileManager defaultManager] createDirectoryAtPath: folderPath withIntermediateDirectories: YES attributes: nil error: nil];
		if( [cryptModuleName length] > 0 ) {
			return [self encryptData: data toFilePath: filePath forName: cryptModuleName];
		}
		return [data writeToFile: filePath atomically: YES];
	}
	
	if( (query = [self queryForExecutorName: ResourceManagerLocalJobExecutorName]) != nil ) {
		[query setParameter: [NSNumber numberWithInteger: ResourceManagerLocalJobExecutorOperationSave] forKey: ResourceManagerLocalJobExecutorParameterKeyOperation];
		[query setParameter: [NSNumber numberWithInteger: ResourceManagerLocalJobExecutorDataTypeDataObject] forKey: ResourceManagerLocalJobExecutorParameterKeyDataType];
		[query setParameter: data forKey: ResourceManagerLocalJobExecutorParameterKeyDataObject];
		[query setParameter: name forKey: ResourceManagerLocalJobExecutorParameterKeyFileName];
		[query setParameter: cryptModuleName forKey: ResourceManagerLocalJobExecutorParameterKeyCryptModuleName];
		[[Hydra defaultHydra] pushQuery: query];
	}
	
	return NO;
}

- (BOOL) saveFile: (NSString *)path forFileName: (NSString *)name cryptModuleName:(NSString *)cryptModuleName removeSource:(BOOL)removeSource waitUntilDone:(BOOL)waitUntilDone
{
	NSString		*folderPath;
	NSString		*filePath;
	HYQuery			*query;
	NSData			*data;
	
	if( self.standby == NO ) {
		return NO;
	}
	
	if( ([path length] <= 0) || ([name length] <= 0) ) {
		return NO;
	}
	
	if( waitUntilDone == YES ) {
		if( (filePath = [self pathFromFileName: name]) == nil ) {
			return NO;
		}
		if( (folderPath = [filePath stringByDeletingLastPathComponent]) == nil ) {
			return NO;
		}
		if( [[NSFileManager defaultManager] fileExistsAtPath: folderPath] == YES ) {
			[[NSFileManager defaultManager] removeItemAtPath: folderPath error: nil];
		}
		[[NSFileManager defaultManager] createDirectoryAtPath: folderPath withIntermediateDirectories: YES attributes: nil error: nil];
		if( [cryptModuleName length] > 0 ) {
			if( (data = [NSData dataWithContentsOfFile: path]) == nil ) {
				return NO;
			}
			return [self encryptData: data toFilePath: filePath forName: cryptModuleName];
		} else {
			if( [[NSFileManager defaultManager] copyItemAtPath: path toPath: filePath error: nil] == NO ) {
				return NO;
			}
		}
		if( removeSource == YES ) {
			[[NSFileManager defaultManager] removeItemAtPath: path error: nil];
		}
	}
	
	if( (query = [self queryForExecutorName: ResourceManagerLocalJobExecutorName]) != nil ) {
		[query setParameter: [NSNumber numberWithInteger: ResourceManagerLocalJobExecutorOperationSave] forKey: ResourceManagerLocalJobExecutorParameterKeyOperation];
		[query setParameter: [NSNumber numberWithInteger: ResourceManagerLocalJobExecutorDataTypeDataPath] forKey: ResourceManagerLocalJobExecutorParameterKeyDataType];
		[query setParameter: path forKey: ResourceManagerLocalJobExecutorParameterKeyDataPath];
		[query setParameter: name forKey: ResourceManagerLocalJobExecutorParameterKeyFileName];
		[query setParameter: cryptModuleName forKey: ResourceManagerLocalJobExecutorParameterKeyCryptModuleName];
		[[Hydra defaultHydra] pushQuery: query];
	}
	
	return NO;
}

- (BOOL) saveString: (NSString *)string forUrlString: (NSString *)urlString cryptModuleName: (NSString *)cryptModuleName waitUntilDone: (BOOL)waitUntilDone
{
	if( ([string length] <= 0) || ([urlString length] <= 0) ) {
		return NO;
	}
	
	return [self saveData: [string dataUsingEncoding: NSUTF8StringEncoding] forUrlString: urlString cryptModuleName: cryptModuleName waitUntilDone: waitUntilDone];
}

- (BOOL) saveImage: (UIImage *)image forUrlString: (NSString *)urlString cryptModuleName: (NSString *)cryptModuleName waitUntilDone: (BOOL)waitUntilDone
{
	if( (image == nil) || ([urlString length] <= 0) ) {
		return NO;
	}
	
	return [self saveData: UIImagePNGRepresentation(image) forUrlString: urlString cryptModuleName: cryptModuleName waitUntilDone: waitUntilDone];
}

- (BOOL) saveData: (NSData *)data forUrlString: (NSString *)urlString cryptModuleName: (NSString *)cryptModuleName waitUntilDone: (BOOL)waitUntilDone
{
	NSString		*folderPath;
	NSString		*filePath;
	HYQuery			*query;
	
	if( self.standby == NO ) {
		return NO;
	}
	
	if( ([data length] <= 0) || ([urlString length] <= 0) ) {
		return NO;
	}
	
	if( waitUntilDone == YES ) {
		if( (filePath = [self pathFromUrlString: urlString]) == nil ) {
			return NO;
		}
		if( (folderPath = [filePath stringByDeletingLastPathComponent]) == nil ) {
			return NO;
		}
		if( [[NSFileManager defaultManager] fileExistsAtPath: folderPath] == YES ) {
			[[NSFileManager defaultManager] removeItemAtPath: folderPath error: nil];
		}
		[[NSFileManager defaultManager] createDirectoryAtPath: folderPath withIntermediateDirectories: YES attributes: nil error: nil];
		if( [cryptModuleName length] > 0 ) {
			return [self encryptData: data toFilePath: filePath forName: cryptModuleName];
		}
		return [data writeToFile: filePath atomically: YES];
	}
	
	if( (query = [self queryForExecutorName: ResourceManagerLocalJobExecutorName]) != nil ) {
		[query setParameter: [NSNumber numberWithInteger: ResourceManagerLocalJobExecutorOperationSave] forKey: ResourceManagerLocalJobExecutorParameterKeyOperation];
		[query setParameter: [NSNumber numberWithInteger: ResourceManagerLocalJobExecutorDataTypeDataObject] forKey: ResourceManagerLocalJobExecutorParameterKeyDataType];
		[query setParameter: data forKey: ResourceManagerLocalJobExecutorParameterKeyDataObject];
		[query setParameter: urlString forKey: ResourceManagerLocalJobExecutorParameterKeyUrlString];
		[query setParameter: cryptModuleName forKey: ResourceManagerLocalJobExecutorParameterKeyCryptModuleName];
		[[Hydra defaultHydra] pushQuery: query];
	}
	
	return NO;
}

- (BOOL) saveFile: (NSString *)path forUrlString: (NSString *)urlString cryptModuleName:(NSString *)cryptModuleName removeSource:(BOOL)removeSource waitUntilDone:(BOOL)waitUntilDone
{
	NSString		*folderPath;
	NSString		*filePath;
	HYQuery			*query;
	NSData			*data;
	
	if( self.standby == NO ) {
		return NO;
	}
	
	if( ([path length] <= 0) || ([urlString length] <= 0) ) {
		return NO;
	}
	
	if( waitUntilDone == YES ) {
		if( (filePath = [self pathFromUrlString: urlString]) == nil ) {
			return NO;
		}
		if( (folderPath = [filePath stringByDeletingLastPathComponent]) == nil ) {
			return NO;
		}
		if( [[NSFileManager defaultManager] fileExistsAtPath: folderPath] == YES ) {
			[[NSFileManager defaultManager] removeItemAtPath: folderPath error: nil];
		}
		[[NSFileManager defaultManager] createDirectoryAtPath: folderPath withIntermediateDirectories: YES attributes: nil error: nil];
		if( [cryptModuleName length] > 0 ) {
			if( (data = [NSData dataWithContentsOfFile: path]) == nil ) {
				return NO;
			}
			return [self encryptData: data toFilePath: filePath forName: cryptModuleName];
		} else {
			if( [[NSFileManager defaultManager] copyItemAtPath: path toPath: filePath error: nil] == NO ) {
				return NO;
			}
		}
		if( removeSource == YES ) {
			[[NSFileManager defaultManager] removeItemAtPath: path error: nil];
		}
	}
	
	if( (query = [self queryForExecutorName: ResourceManagerLocalJobExecutorName]) != nil ) {
		[query setParameter: [NSNumber numberWithInteger: ResourceManagerLocalJobExecutorOperationSave] forKey: ResourceManagerLocalJobExecutorParameterKeyOperation];
		[query setParameter: [NSNumber numberWithInteger: ResourceManagerLocalJobExecutorDataTypeDataPath] forKey: ResourceManagerLocalJobExecutorParameterKeyDataType];
		[query setParameter: path forKey: ResourceManagerLocalJobExecutorParameterKeyDataPath];
		[query setParameter: urlString forKey: ResourceManagerLocalJobExecutorParameterKeyUrlString];
		[query setParameter: cryptModuleName forKey: ResourceManagerLocalJobExecutorParameterKeyCryptModuleName];
		[[Hydra defaultHydra] pushQuery: query];
	}
	
	return NO;
}

- (NSString *) stringFromFilePath: (NSString *)path cryptModuleName:(NSString *)cryptModuleName waitUntilDone:(BOOL)waitUntilDone
{
	HYQuery			*query;
	
	if( self.standby == NO ) {
		return nil;
	}
	
	if( [path length] <= 0 ) {
		return nil;
	}
	
	if( waitUntilDone == YES ) {
		return (NSString *)[self resourceFromFilePath: path cryptModuleName: cryptModuleName waitUntilDone: YES dataType: ResourceManagerDataTypeString onlyCheckMemoryCache: NO];
	}
	
	if( (query = [self queryForExecutorName: ResourceManagerRemoteJobExecutorName]) != nil ) {
		[query setParameter: [NSNumber numberWithInteger: ResourceManagerRemoteJobExecutorOperationRequest] forKey: ResourceManagerRemoteJobExecutorParameterKeyOperation];
		[query setParameter: [NSNumber numberWithInteger: ResourceManagerRemoteJobExecutorDataTypeString] forKey: ResourceManagerRemoteJobExecutorParameterKeyDataType];
		[query setParameter: path forKey: ResourceManagerRemoteJobExecutorParameterKeyFilePath];
		[query setParameter: cryptModuleName forKey: ResourceManagerRemoteJobExecutorParameterKeyCryptModuleName];
		[[Hydra defaultHydra] pushQuery: query];
	}
	
	return nil;
}

- (NSString *) stringFromResourceName: (NSString *)name cryptModuleName:(NSString *)cryptModuleName waitUntilDone:(BOOL)waitUntilDone
{
	NSString		*filePath;
	
	if( self.standby == NO ) {
		return nil;
	}
	
	if( [name length] <= 0 ) {
		return nil;
	}
	
	if( (filePath = [self pathFromResourceName: name]) == nil ) {
		return nil;
	}
	
	return [self stringFromFilePath: filePath cryptModuleName: cryptModuleName waitUntilDone: waitUntilDone];
}

- (NSString *) stringFromFileName: (NSString *)name cryptModuleName:(NSString *)cryptModuleName waitUntilDone:(BOOL)waitUntilDone
{
	NSString		*filePath;
	
	if( self.standby == NO ) {
		return nil;
	}
	
	if( [name length] <= 0 ) {
		return nil;
	}
	
	if( (filePath = [self pathFromFileName: name]) == nil ) {
		return nil;
	}
	
	return [self stringFromFilePath: filePath cryptModuleName: cryptModuleName waitUntilDone: waitUntilDone];
}

- (NSString *) stringFromUrlString: (NSString *)urlString cryptModuleName:(NSString *)cryptModuleName ifHaveDataWaitUntilDone:(BOOL)ifHaveDataWaitUntilDone
{
	return [self stringFromUrlString: urlString remakerName: ResourceManagerOriginalRemakerName remakerParameter: nil cryptModuleName: cryptModuleName ifHaveDataWaitUntilDone: ifHaveDataWaitUntilDone];
}

- (NSString *) stringFromUrlString: (NSString *)urlString remakerName: (NSString *)remakerName remakerParameter: (id)remakerParameter cryptModuleName:(NSString *)cryptModuleName ifHaveDataWaitUntilDone:(BOOL)ifHaveDataWaitUntilDone
{
	return (NSString *)[self resourceFromUrlString: urlString remakerName: remakerName remakerParameter: remakerParameter cryptModuleName: cryptModuleName ifHaveDataWaitUntilDone: ifHaveDataWaitUntilDone dataType: ResourceManagerDataTypeString];
}

- (UIImage *) imageFromFilePath: (NSString *)path cryptModuleName:(NSString *)cryptModuleName waitUntilDone:(BOOL)waitUntilDone
{
	HYQuery			*query;
	
	if( self.standby == NO ) {
		return nil;
	}
	
	if( [path length] <= 0 ) {
		return nil;
	}
	
	if( waitUntilDone == YES ) {
		return (UIImage *)[self resourceFromFilePath: path cryptModuleName: cryptModuleName waitUntilDone: YES dataType: ResourceManagerDataTypeImage onlyCheckMemoryCache: NO];
	}
	
	if( (query = [self queryForExecutorName: ResourceManagerRemoteJobExecutorName]) != nil ) {
		[query setParameter: [NSNumber numberWithInteger: ResourceManagerRemoteJobExecutorOperationRequest] forKey: ResourceManagerRemoteJobExecutorParameterKeyOperation];
		[query setParameter: [NSNumber numberWithInteger: ResourceManagerRemoteJobExecutorDataTypeImage] forKey: ResourceManagerRemoteJobExecutorParameterKeyDataType];
		[query setParameter: path forKey: ResourceManagerRemoteJobExecutorParameterKeyFilePath];
		[query setParameter: cryptModuleName forKey: ResourceManagerRemoteJobExecutorParameterKeyCryptModuleName];
		[[Hydra defaultHydra] pushQuery: query];
	}
	
	return nil;
}

- (UIImage *) imageFromResourceName: (NSString *)name
{
	return [self imageFromResourceName: name cryptModuleName: nil waitUntilDone: YES];
}

- (UIImage *) imageFromResourceName: (NSString *)name cryptModuleName:(NSString *)cryptModuleName waitUntilDone:(BOOL)waitUntilDone
{
	NSString		*filePath;
	
	if( self.standby == NO ) {
		return nil;
	}
	
	if( [name length] <= 0 ) {
		return nil;
	}
	
	if( (filePath = [self pathFromResourceName: name]) == nil ) {
		return nil;
	}
	
	return [self imageFromFilePath: filePath cryptModuleName: cryptModuleName waitUntilDone: waitUntilDone];
}

- (UIImage *) imageFromResourceName: (NSString *)name atBundleName: (NSString *)bundleName waitUntilDone:(BOOL)waitUntilDone
{
	return [self imageFromResourceName: name cryptModuleName: nil atBundleName: bundleName waitUntilDone: waitUntilDone];
}

- (UIImage *) imageFromResourceName: (NSString *)name cryptModuleName: (NSString *)cryptModuleName atBundleName: (NSString *)bundleName waitUntilDone:(BOOL)waitUntilDone
{
	NSBundle		*bundle;
	NSString		*filePath;
	NSURL			*bundleUrl;
	
	if( ([name length] <= 0) || ([bundleName length] <= 0) ) {
		return nil;
	}
	
	if( (bundleUrl = [[NSBundle mainBundle] URLForResource: bundleName withExtension: @"bundle"]) == nil ) {
		return nil;
	}
	
	if( (bundle = [NSBundle bundleWithURL: bundleUrl]) == nil ) {
		return nil;
	}
	
	if( (filePath = [[bundle resourcePath] stringByAppendingPathComponent: name]) == nil ) {
		return nil;
	}
	
	return [self imageFromFilePath: filePath cryptModuleName: cryptModuleName waitUntilDone: waitUntilDone];
}

- (UIImage *) imageFromFileName: (NSString *)name cryptModuleName:(NSString *)cryptModuleName waitUntilDone:(BOOL)waitUntilDone
{
	NSString		*filePath;
	
	if( self.standby == NO ) {
		return nil;
	}
	
	if( [name length] <= 0 ) {
		return nil;
	}
	
	if( (filePath = [self pathFromFileName: name]) == nil ) {
		return nil;
	}
	
	return [self imageFromFilePath: filePath cryptModuleName: cryptModuleName waitUntilDone: waitUntilDone];
}

- (UIImage *) imageFromUrlString: (NSString *)urlString cryptModuleName:(NSString *)cryptModuleName ifHaveDataWaitUntilDone:(BOOL)ifHaveDataWaitUntilDone
{
	return [self imageFromUrlString: urlString remakerName: ResourceManagerOriginalRemakerName remakerParameter: nil cryptModuleName: cryptModuleName ifHaveDataWaitUntilDone: ifHaveDataWaitUntilDone];
}

- (UIImage *) imageFromUrlString: (NSString *)urlString remakerName:(NSString *)remakerName remakerParameter:(id)remakerParameter cryptModuleName:(NSString *)cryptModuleName ifHaveDataWaitUntilDone:(BOOL)ifHaveDataWaitUntilDone
{
	return (UIImage *)[self resourceFromUrlString: urlString remakerName: remakerName remakerParameter: remakerParameter cryptModuleName: cryptModuleName ifHaveDataWaitUntilDone: ifHaveDataWaitUntilDone dataType: ResourceManagerDataTypeImage];
}

- (NSData *) dataFromFilePath: (NSString *)path cryptModuleName:(NSString *)cryptModuleName waitUntilDone:(BOOL)waitUntilDone
{
	HYQuery			*query;
	
	if( self.standby == NO ) {
		return nil;
	}
	
	if( [path length] <= 0 ) {
		return nil;
	}
	
	if( waitUntilDone == YES ) {
		return (NSData *)[self resourceFromFilePath: path cryptModuleName: cryptModuleName waitUntilDone: YES dataType: ResourceManagerDataTypeData onlyCheckMemoryCache: NO];
	}
	
	if( (query = [self queryForExecutorName: ResourceManagerRemoteJobExecutorName]) != nil ) {
		[query setParameter: [NSNumber numberWithInteger: ResourceManagerRemoteJobExecutorOperationRequest] forKey: ResourceManagerRemoteJobExecutorParameterKeyOperation];
		[query setParameter: [NSNumber numberWithInteger: ResourceManagerRemoteJobExecutorDataTypeDataObject] forKey: ResourceManagerRemoteJobExecutorParameterKeyDataType];
		[query setParameter: path forKey: ResourceManagerRemoteJobExecutorParameterKeyFilePath];
		[query setParameter: cryptModuleName forKey: ResourceManagerRemoteJobExecutorParameterKeyCryptModuleName];
		[[Hydra defaultHydra] pushQuery: query];
	}
	
	return nil;
}

- (NSData *) dataFromResourceName: (NSString *)name cryptModuleName:(NSString *)cryptModuleName waitUntilDone:(BOOL)waitUntilDone
{
	NSString		*filePath;
	
	if( self.standby == NO ) {
		return nil;
	}
	
	if( [name length] <= 0 ) {
		return nil;
	}
	
	if( (filePath = [self pathFromResourceName: name]) == nil ) {
		return nil;
	}
	
	return [self dataFromFilePath: filePath cryptModuleName: cryptModuleName waitUntilDone: waitUntilDone];
}

- (NSData *) dataFromFileName: (NSString *)name cryptModuleName:(NSString *)cryptModuleName waitUntilDone:(BOOL)waitUntilDone
{
	NSString		*filePath;
	
	if( self.standby == NO ) {
		return nil;
	}
	
	if( [name length] <= 0 ) {
		return nil;
	}
	
	if( (filePath = [self pathFromFileName: name]) == nil ) {
		return nil;
	}
	
	return [self dataFromFilePath: filePath cryptModuleName: cryptModuleName waitUntilDone: waitUntilDone];
}

- (NSData *) dataFromUrlString: (NSString *)urlString cryptModuleName:(NSString *)cryptModuleName ifHaveDataWaitUntilDone:(BOOL)ifHaveDataWaitUntilDone
{
	return [self dataFromUrlString: urlString remakerName: ResourceManagerOriginalRemakerName remakerParameter: nil cryptModuleName: cryptModuleName ifHaveDataWaitUntilDone: ifHaveDataWaitUntilDone];
}

- (NSData *) dataFromUrlString: (NSString *)urlString remakerName: (NSString *)remakerName remakerParameter: (id)remakerParameter cryptModuleName:(NSString *)cryptModuleName ifHaveDataWaitUntilDone:(BOOL)ifHaveDataWaitUntilDone
{
	return (NSData *)[self resourceFromUrlString: urlString remakerName: remakerName remakerParameter: remakerParameter cryptModuleName: cryptModuleName ifHaveDataWaitUntilDone: ifHaveDataWaitUntilDone dataType: ResourceManagerDataTypeData];

}

- (NSString *) pathFromResourceName: (NSString *)name
{
	NSString	*filePath;
	NSString	*fileName;
	NSString	*fileExtension;
	
	if( self.standby == NO ) {
		return nil;
	}
	
	if( [name length] <= 0 ) {
		return nil;
	}
	
	if( (filePath = [NSString stringWithFormat: @"%@/%@", [[NSBundle mainBundle] resourcePath], name]) == nil ) {
		return nil;
	}
	
	if( [[NSFileManager defaultManager] fileExistsAtPath: filePath] == NO ) {
		fileName = [[name lastPathComponent] stringByDeletingPathExtension];
		fileExtension = [name pathExtension];
		if( (fileName == nil) || (fileExtension == nil) ) {
			return nil;
		}
		filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:fileExtension];
	}
	
	return filePath;
}

- (NSString *) pathFromFileNameFolder: (NSString *)name
{
	NSString		*urlString;
	NSString		*urlHashKey;
	
	if( self.standby == NO ) {
		return nil;
	}
	
	if( [name length] <= 0 ) {
		return nil;
	}
	
	urlString = [self urlStringFromFileName: name];
	
	[_lockForUrlHashKeyDict lock];
	
	if( (urlHashKey = [_loadedUrlHashKeyDict objectForKey: urlString]) == nil ) {
		if( (urlHashKey = [self urlHashKeyFromUrlString: urlString]) != nil ) {
			[_loadedUrlHashKeyDict setObject: urlHashKey forKey: urlString];
		}
	}
	
	[_lockForUrlHashKeyDict unlock];
	
	if( urlHashKey == nil ) {
		return nil;
	}
	
	return [NSString stringWithFormat: @"%@/%@", _repositoryPath, urlHashKey];
}

- (NSString *) pathFromFileName: (NSString *)name
{
	NSString		*urlString;
	NSString		*urlHashKey;
	
	if( self.standby == NO ) {
		return nil;
	}
	
	if( [name length] <= 0 ) {
		return nil;
	}
	
	urlString = [self urlStringFromFileName: name];
	if( (urlHashKey = [self urlHashKeyFromUrlString: urlString]) == nil ) {
		return nil;
	}
	
	return [NSString stringWithFormat: @"%@/%@/%@", _repositoryPath, urlHashKey, ResourceManagerOriginalRemakerName];
}

- (NSString *) pathFromUrlString: (NSString *)urlString
{
	return [self pathFromUrlString: urlString remakerName: ResourceManagerOriginalRemakerName remakerParameter: nil];
}

- (NSString *) pathFromUrlString: (NSString *)urlString remakerName: (NSString *)remakerName remakerParameter: (id)remakerParameter
{
	id				remaker;
	NSString		*urlHashKey;
	NSString		*identifier;
	NSString		*path;
	
	if( self.standby == NO ) {
		return nil;
	}
	
	if( ([urlString length] <= kMiminumURLStringLength) || ([remakerName length] <= 0) ) {
		return nil;
	}
	if( [[urlString substringFromIndex: [urlString length]-1] isEqualToString: @"/"] == YES ) {
		urlString = [urlString substringToIndex: [urlString length]-1];
	}
	[_lockForSupportDict lock];
	remaker = [_remakerDict objectForKey: remakerName];
	[_lockForSupportDict unlock];
	if( remaker == nil ) {
		if( [remakerName isEqualToString: ResourceManagerOriginalRemakerName] == NO ) {
			return nil;
		}
	}
	
	[_lockForUrlHashKeyDict lock];
	
	if( (urlHashKey = [_loadedUrlHashKeyDict objectForKey: urlString]) == nil ) {
		if( (urlHashKey = [self urlHashKeyFromUrlString: urlString]) != nil ) {
			[_loadedUrlHashKeyDict setObject: urlHashKey forKey: urlString];
		}
	}
	
	[_lockForUrlHashKeyDict unlock];
	
	if( urlHashKey == nil ) {
		return nil;
	}
	
	if( remaker != nil ) {
		identifier = [remaker identifierWithParameter: remakerParameter];
	} else {
		identifier = nil;
	}
	if( [identifier length] > 0 ) {
		path = [NSString stringWithFormat: @"%@/%@/%@_%@", _repositoryPath, urlHashKey, remakerName, identifier];
	} else {
		path = [NSString stringWithFormat: @"%@/%@/%@", _repositoryPath, urlHashKey, remakerName];
	}
	
	return path;
}

- (NSString *) urlStringFromFileName: (NSString *)name
{
	return [NSString stringWithFormat: @"rman://%@", name];
}

- (NSString *) folderPathFromUrlString: (NSString *)urlString
{
	NSString		*urlHashKey;
	NSString		*path;
	
	if( self.standby == NO ) {
		return nil;
	}
	
	if( [urlString length] <= kMiminumURLStringLength ) {
		return nil;
	}
	if( [[urlString substringFromIndex: [urlString length]-1] isEqualToString: @"/"] == YES ) {
		urlString = [urlString substringToIndex: [urlString length]-1];
	}
	
	[_lockForUrlHashKeyDict lock];
	
	if( (urlHashKey = [_loadedUrlHashKeyDict objectForKey: urlString]) == nil ) {
		if( (urlHashKey = [self urlHashKeyFromUrlString: urlString]) != nil ) {
			[_loadedUrlHashKeyDict setObject: urlHashKey forKey: urlString];
		}
	}
	
	[_lockForUrlHashKeyDict unlock];
	
	if( urlHashKey == nil ) {
		return nil;
	}
	
	path = [NSString stringWithFormat: @"%@/%@", _repositoryPath, urlHashKey];
	
	return path;
}


- (NSString *) temporaryFilePathFromUrlString: (NSString *)urlString
{
	NSString		*path;
	
	if( self.standby == NO ) {
		return nil;
	}
	
	if( (path = [self folderPathFromUrlString: urlString]) == nil ) {
		return nil;
	}
	
	path = [path stringByAppendingPathComponent: ResourceManagerDownloadingTemporaryName];
	
	return path;
}

- (NSDate *) createDateForFilePath: (NSString *)path
{
	NSDictionary	*attribute;
	
	if( self.standby == NO ) {
		return nil;
	}
	
	if( [path length] <= 0 ) {
		return nil;
	}
	
	if( (attribute = [[NSFileManager defaultManager] attributesOfItemAtPath: path error: nil]) == nil ) {
		return nil;
	}
	
	return [attribute objectForKey: NSFileCreationDate];
}

- (NSDate *) createDateForCachedUrlString: (NSString *)urlString
{
	NSString		*path;
	
	if( self.standby == NO ) {
		return nil;
	}
	
	if( [urlString length] <= 0 ) {
		return nil;
	}
	
	path = [self pathFromUrlString: urlString];
	
	return [self createDateForFilePath: path];
}

- (NSDate *) createDateForSavedFileName: (NSString *)name
{
	NSString		*path;
	
	if( self.standby == NO ) {
		return nil;
	}
	
	if( [name length] <= 0 ) {
		return nil;
	}
	
	path = [self pathFromFileName: name];
	
	return [self createDateForFilePath: path];
}

- (NSUInteger) amountSizeOfAllItems: (BOOL)waitUntilDone
{
	HYQuery			*query;
	NSArray			*list;
	NSUInteger		amountSize;
	NSString		*itemName;
	NSString		*path;
	
	if( self.standby == NO ) {
		return 0;
	}
	
	if( waitUntilDone == NO ) {
		if( (query = [self queryForExecutorName: ResourceManagerLocalJobExecutorName]) != nil ) {
			return 0;
		}
		[query setParameter: [NSNumber numberWithInteger: ResourceManagerLocalJobExecutorOperationAmountItemSize] forKey: ResourceManagerLocalJobExecutorParameterKeyOperation];
		[[Hydra defaultHydra] pushQuery: query];
		return 0;
	}
	
	if( (list = [[NSFileManager defaultManager] contentsOfDirectoryAtPath: _repositoryPath error: nil]) == nil ) {
		return 0;
	}
	
	amountSize = 0;
	
	[_lockForResourceDict lock];
	
	for( itemName in list ) {
		if( ([[itemName substringToIndex: 1] isEqualToString: @"."] == YES) ||
		   ([itemName isEqualToString: ResourceManagerRetainCountFileName] == YES) ||
		   ([itemName isEqualToString: ResourceManagerRemoveMarkFileName] == YES) )
		{
			continue;
		}
		path = [_repositoryPath stringByAppendingPathComponent: itemName];
		amountSize += [self amountSizeForDirectory: path];
	}
	
	[_lockForResourceDict unlock];
	
	return amountSize;
}

- (BOOL) removeItemForFilePath: (NSString *)path force: (BOOL)force
{
	NSString				*targetPath;
	NSMutableDictionary		*subDict;
	NSString				*remakedPath;
	
	if( self.standby == NO ) {
		return NO;
	}
	
	if( [path length] <= 0 ) {
		return NO;
	}
	
	[_lockForResourceDict lock];
	
	if( ([[_retainCountDict objectForKey: path] integerValue] > 0) && (force == NO) ) {
		[_removeMarkDict setObject: @"Y" forKey: path];
		_changed = YES;
	} else {
		if( [[path lastPathComponent] isEqualToString: ResourceManagerOriginalRemakerName] == YES ) {
			targetPath = [path stringByDeletingLastPathComponent];
		} else {
			targetPath = path;
		}
		if( [[NSFileManager defaultManager] removeItemAtPath: targetPath error: nil] == NO ) {
			[_lockForResourceDict unlock];
			return NO;
		}
	}
	
	[_lockForResourceDict unlock];
	
	[self unloadItemForFilePath: path];
	
	if( (subDict = [_remakedPathDict objectForKey: path]) != nil ) {
		for( remakedPath in subDict ) {
			[self unloadItemForFilePath: remakedPath];
		}
	}
	
	return YES;
}

- (BOOL) removeItemForFileName: (NSString *)name force: (BOOL)force
{
	NSString		*path;
	
	if( self.standby == NO ) {
		return NO;
	}
	
	if( [name length] <= 0 ) {
		return NO;
	}
	
	if( (path = [self pathFromFileName: name]) == nil ) {
		return NO;
	}
	
	return [self removeItemForFilePath: path force: force];
}

- (BOOL) removeItemForUrlString: (NSString *)urlString force: (BOOL)force
{
	NSString		*path;
	
	if( self.standby == NO ) {
		return NO;
	}
	
	if( [urlString length] <= 0 ) {
		return NO;
	}
	
	if( (path = [self pathFromUrlString: urlString]) == nil ) {
		return NO;
	}
	
	return [self removeItemForFilePath: path force: force];
}

- (BOOL) removeItemsForElapsedTimeFromNow: (NSTimeInterval)elpasedTime force: (BOOL)force waitUntilDone: (BOOL)waitUntilDone
{
	HYQuery			*query;
	NSArray			*list;
	NSString		*itemName;
	NSString		*path;
	NSDictionary	*dict;
	NSDate			*creationDate;
	
	if( self.standby == NO ) {
		return NO;
	}
	
	if( waitUntilDone == NO ) {
		if( (query = [self queryForExecutorName: ResourceManagerLocalJobExecutorName]) != nil ) {
			return NO;
		}
		[query setParameter: [NSNumber numberWithInteger: ResourceManagerLocalJobExecutorOperationRemove] forKey: ResourceManagerLocalJobExecutorParameterKeyOperation];
		[query setParameter: [NSNumber numberWithDouble: elpasedTime] forKey: ResourceManagerLocalJobExecutorParameterKeyTime];
		[query setParameter: [NSNumber numberWithBool: force] forKey: ResourceManagerLocalJobExecutorParameterKeyForce];
		[[Hydra defaultHydra] pushQuery: query];
		return NO;
	}
	
	if( (list = [[NSFileManager defaultManager] contentsOfDirectoryAtPath: _repositoryPath error: nil]) == nil ) {
		return NO;
	}
	
	[_lockForResourceDict lock];
	
	for( itemName in list ) {
		if( ([[itemName substringToIndex: 1] isEqualToString: @"."] == YES) ||
		   ([itemName isEqualToString: ResourceManagerRetainCountFileName] == YES) ||
		   ([itemName isEqualToString: ResourceManagerRemoveMarkFileName] == YES) )
		{
			continue;
		}
		path = [_repositoryPath stringByAppendingPathComponent: itemName];
		if( (dict = [[NSFileManager defaultManager] attributesOfItemAtPath: path error: nil]) == nil ) {
			continue;
		}
		if( (creationDate = [dict objectForKey: NSFileCreationDate]) == nil ) {
			continue;
		}
		if( [creationDate timeIntervalSinceNow] > elpasedTime ) {
			if( ([[_retainCountDict objectForKey: path] integerValue] > 0) && (force == NO) ) {
				[_removeMarkDict setObject: @"Y" forKey: path];
				_changed = YES;
			} else {
				[[NSFileManager defaultManager] removeItemAtPath: path error: nil];
			}
		}
	}
	
	[_lockForResourceDict unlock];
	
	return YES;
}

- (BOOL) removeItemsForMaximumSizeBoundary: (NSUInteger)boundarySize force: (BOOL)force waitUntilDone: (BOOL)waitUntilDone
{
	HYQuery			*query;
	NSArray			*list;
	NSMutableArray	*sortedList;
	NSString		*itemName;
	NSString		*path;
	NSInteger		amountSize;
	NSInteger		currentSize;
	
	if( self.standby == NO ) {
		return NO;
	}
	
	if( waitUntilDone == NO ) {
		if( (query = [self queryForExecutorName: ResourceManagerLocalJobExecutorName]) != nil ) {
			return NO;
		}
		[query setParameter: [NSNumber numberWithInteger: ResourceManagerLocalJobExecutorOperationRemove] forKey: ResourceManagerLocalJobExecutorParameterKeyOperation];
		[query setParameter: [NSNumber numberWithUnsignedInteger: boundarySize] forKey: ResourceManagerLocalJobExecutorParameterKeyBoundarySize];
		[query setParameter: [NSNumber numberWithBool: force] forKey: ResourceManagerLocalJobExecutorParameterKeyForce];
		[[Hydra defaultHydra] pushQuery: query];
		return NO;
	}
	
	amountSize = [self amountSizeOfAllItems: YES];
	
	if( boundarySize >= amountSize ) {
		return YES;
	}
	
	if( (list = [[NSFileManager defaultManager] contentsOfDirectoryAtPath: _repositoryPath error: nil]) == nil ) {
		return NO;
	}
	
	if( (sortedList = [[NSMutableArray alloc] initWithArray: list]) == nil ) {
		return NO;
	}
	
	[sortedList sortUsingComparator: ^NSComparisonResult( id fileNameA, id fileNameB ) {
		
		NSString	*pathA;
		NSString	*pathB;
		NSDate		*dateA;
		NSDate		*dateB;
		
		pathA = [_repositoryPath stringByAppendingPathComponent: (NSString *)fileNameA];
		pathB = [_repositoryPath stringByAppendingPathComponent: (NSString *)fileNameB];
		
		dateA = [[[NSFileManager defaultManager] attributesOfItemAtPath: pathA error: nil] objectForKey: NSFileCreationDate];
		dateB = [[[NSFileManager defaultManager] attributesOfItemAtPath: pathB error: nil] objectForKey: NSFileCreationDate];
		
		return [dateA compare: dateB];
		
	}];
	
	[_lockForResourceDict lock];
	
	for( itemName in sortedList ) {
		
		if( ([[itemName substringToIndex: 1] isEqualToString: @"."] == YES) ||
		   ([itemName isEqualToString: ResourceManagerRetainCountFileName] == YES) ||
		   ([itemName isEqualToString: ResourceManagerRemoveMarkFileName] == YES) )
		{
			continue;
		}
		path = [_repositoryPath stringByAppendingPathComponent: itemName];
		currentSize = [self amountSizeForDirectory: path];
		if( ([[_retainCountDict objectForKey: path] integerValue] > 0) && (force == NO) ) {
			[_removeMarkDict setObject: @"Y" forKey: path];
			_changed = YES;
		} else {
			[[NSFileManager defaultManager] removeItemAtPath: path error: nil];
		}
		amountSize -= currentSize;
		if( amountSize <= boundarySize ) {
			break;
		}
		
	}
	
	[_lockForResourceDict unlock];
	
	return YES;
}

- (BOOL) removeAllItems: (BOOL)force waitUntilDone: (BOOL)waitUntilDone
{
	HYQuery			*query;
	NSArray			*list;
	NSString		*itemName;
	NSString		*path;
	NSDictionary	*dict;
	
	if( self.standby == NO ) {
		return NO;
	}
	
	if( waitUntilDone == NO ) {
		if( (query = [self queryForExecutorName: ResourceManagerLocalJobExecutorName]) != nil ) {
			return NO;
		}
		[query setParameter: [NSNumber numberWithInteger: ResourceManagerLocalJobExecutorOperationRemove] forKey: ResourceManagerLocalJobExecutorParameterKeyOperation];
		[query setParameter: [NSNumber numberWithBool: force] forKey: ResourceManagerLocalJobExecutorParameterKeyForce];
		[[Hydra defaultHydra] pushQuery: query];
		return NO;
	}
	
	if( (list = [[NSFileManager defaultManager] contentsOfDirectoryAtPath: _repositoryPath error: nil]) == nil ) {
		return NO;
	}
	
	[_lockForResourceDict lock];
	
	for( itemName in list ) {
		if( ([[itemName substringToIndex: 1] isEqualToString: @"."] == YES) ||
		   ([itemName isEqualToString: ResourceManagerRetainCountFileName] == YES) ||
		   ([itemName isEqualToString: ResourceManagerRemoveMarkFileName] == YES) )
		{
			continue;
		}
		path = [_repositoryPath stringByAppendingPathComponent: itemName];
		if( (dict = [[NSFileManager defaultManager] attributesOfItemAtPath: path error: nil]) == nil ) {
			continue;
		}
		if( ([[_retainCountDict objectForKey: path] integerValue] > 0) && (force == NO) ) {
			[_removeMarkDict setObject: @"Y" forKey: path];
			_changed = YES;
		} else {
			[[NSFileManager defaultManager] removeItemAtPath: path error: nil];
		}
	}
	
	[_lockForResourceDict unlock];
	
	return YES;
}

- (void) unloadItemForFilePath: (NSString *)path
{
	id				anObject;
	UIImage			*image;
	NSUInteger		size;
	NSUInteger		i, count;
	
	if( self.standby == NO ) {
		return;
	}
	
	if( [path length] <= 0 ) {
		return;
	}
		
	[_lockForResourceDict lock];
		
	if( (anObject = [_loadedResourceDict objectForKey: path]) == nil ) {
		[_lockForResourceDict unlock];
		return;
	}
	
	if( [anObject isKindOfClass: [NSString class]] == YES ) {
		size = [anObject lengthOfBytesUsingEncoding: NSUTF8StringEncoding];
	} else if( [anObject isKindOfClass: [UIImage class]] == YES ) {
		image = (UIImage *)anObject;
		size = (NSUInteger)(CGImageGetBytesPerRow(image.CGImage)*CGImageGetHeight(image.CGImage));
	} else if( [anObject isKindOfClass: [NSData class]] == YES ) {
		size = [anObject length];
	} else {
		size = 0;
	}
	if( _usedMemorySize >= size ) {
		_usedMemorySize -= size;
	} else {
		_usedMemorySize = 0;
	}
	[_loadedResourceDict removeObjectForKey: path];
	
	count = [_referenceOrder count];
	
	for( i=0 ; i<count ; ++i ) {
		if( [[_referenceOrder objectAtIndex: i] isEqualToString: path] == YES ) {
			[_referenceOrder removeObjectAtIndex: i];
			break;
		}
	}
	
	[_lockForResourceDict unlock];
}

- (void) unloadItemForResourceName: (NSString *)name
{
	NSString		*path;
	
	if( self.standby == NO ) {
		return;
	}
	
	if( [name length] <= 0 ) {
		return;
	}
	
	if( (path = [self pathFromResourceName: name]) == nil ) {
		return;
	}
	
	[self unloadItemForFilePath: path];
}

- (void) unloadItemForFileName: (NSString *)name
{
	NSString		*path;
	
	if( self.standby == NO ) {
		return;
	}
	
	if( [name length] <= 0 ) {
		return;
	}
	
	if( (path = [self pathFromResourceName: name]) == nil ) {
		return;
	}
	
	[self unloadItemForFilePath: path];
}

- (void) unloadItemForUrlString: (NSString *)urlString
{
	[self unloadItemForUrlString: urlString remakerName: ResourceManagerOriginalRemakerName remakerParameter: nil];
}

- (void) unloadItemForUrlString: (NSString *)urlString remakerName: (NSString *)remakerName remakerParameter: (id)remakerParameter
{
	NSString		*path;
	
	if( self.standby == NO ) {
		return;
	}
	
	if( ([urlString length] <= 0) || ([remakerName length] <= 0) ) {
		return;
	}
	
	if( (path = [self pathFromUrlString: urlString remakerName: remakerName remakerParameter: remakerParameter]) == nil ) {
		return;
	}
	
	[self unloadItemForFilePath: path];
}

- (void) unloadAllItems
{
	if( self.standby == NO ) {
		return;
	}
	
	[_lockForResourceDict lock];
	
	[_loadedResourceDict removeAllObjects];
	[_referenceOrder removeAllObjects];
	_usedMemorySize = 0;
	
	[_lockForResourceDict unlock];
}

- (NSInteger) retainCountForFilePath: (NSString *)path
{
	NSInteger		retainCount;
	
	if( self.standby == NO ) {
		return 0;
	}
	
	if( [path length] <= 0 ) {
		return 0;
	}
	
	[_lockForResourceDict lock];
	
	retainCount = [[_retainCountDict objectForKey: path] integerValue];
	
	[_lockForResourceDict unlock];
	
	return retainCount;
}

- (NSInteger) retainCountForFileName: (NSString *)name
{
	NSString		*path;
	
	if( self.standby == NO ) {
		return 0;
	}
	
	if( [name length] <= 0 ) {
		return 0;
	}
	
	if( (path = [self pathFromFileName: name]) == nil ) {
		return 0;
	}
	
	return [self retainCountForFilePath: path];
}
- (NSInteger) retainCountForUrlString: (NSString *)urlString
{
	return [self retainCountForUrlString: urlString remakerName: ResourceManagerOriginalRemakerName remakerParameter: nil];
}

- (NSInteger) retainCountForUrlString: (NSString *)urlString remakerName: (NSString *)remakerName remakerParameter: (id)remakerParameter
{
	NSString		*path;
	
	if( self.standby == NO ) {
		return 0;
	}
	
	if( [urlString length] <= 0 ) {
		return 0;
	}
	
	if( (path = [self pathFromUrlString: urlString remakerName: remakerName remakerParameter: remakerParameter]) == nil ) {
		return 0;
	}
	
	return [self retainCountForFilePath: path];
}

- (NSInteger) retainFilePath: (NSString *)path
{
	NSInteger		retainCount;
	
	if( self.standby == NO ) {
		return 0;
	}
	
	if( [path length] <= 0 ) {
		return 0;
	}
	
	if( [[NSFileManager defaultManager] fileExistsAtPath: path] == NO ) {
		return 0;
	}
	
	[_lockForResourceDict lock];
	
	retainCount = [[_retainCountDict objectForKey: path] integerValue] + 1;
	[_retainCountDict setObject: [NSNumber numberWithInteger: retainCount] forKey: path];
	
	[_lockForResourceDict unlock];
	
	_changed = YES;
	
	return retainCount;
}

- (NSInteger) retainFileName: (NSString *)name
{
	NSString		*path;
	
	if( self.standby == NO ) {
		return 0;
	}
	
	if( [name length] <= 0 ) {
		return 0;
	}
	
	if( (path = [self pathFromFileName: name]) == nil ) {
		return 0;
	}
	
	return [self retainFilePath: path];
}

- (NSInteger) retainUrlString: (NSString *)urlString
{
	return [self retainUrlString: urlString remakerName: ResourceManagerOriginalRemakerName remakerParameter: nil];
}

- (NSInteger) retainUrlString: (NSString *)urlString remakerName: (NSString *)remakerName remakerParameter: (id)remakerParameter
{
	NSString		*path;
	
	if( self.standby == NO ) {
		return 0;
	}
	
	if( ([urlString length] <= 0) || ([remakerName length] <= 0) ) {
		return 0;
	}
	
	if( (path = [self pathFromUrlString: urlString remakerName: remakerName remakerParameter: remakerParameter]) == nil ) {
		return 0;
	}
	
	return [self retainFilePath: path];
}

- (NSInteger) releaseFilePath: (NSString *)path
{
	NSInteger		retainCount;
	
	if( self.standby == NO ) {
		return 0;
	}
	
	if( [path length] <= 0 ) {
		return 0;
	}
	
	[_lockForResourceDict lock];
	
	retainCount = [[_retainCountDict objectForKey: path] integerValue];
	if( retainCount > 0 ) {
		-- retainCount;
		if( retainCount <= 0 ) {
			[_retainCountDict removeObjectForKey: path];
			if( [_removeMarkDict objectForKey: path] != nil ) {
				[_removeMarkDict removeObjectForKey: path];
				if( [[path lastPathComponent] isEqualToString: ResourceManagerOriginalRemakerName] == YES ) {
					path = [path stringByDeletingLastPathComponent];
				}
				[[NSFileManager defaultManager] removeItemAtPath: path error: nil];
			}
		} else {
			[_retainCountDict setObject: [NSNumber numberWithInteger: retainCount] forKey: path];
		}
	}
	
	[_lockForResourceDict unlock];
	
	_changed = YES;
	
	return retainCount;
}

- (NSInteger) releaseFileName: (NSString *)name
{
	NSString		*path;
	
	if( self.standby == NO ) {
		return 0;
	}
	
	if( [name length] <= 0 ) {
		return 0;
	}
	
	if( (path = [self pathFromFileName: name]) == nil ) {
		return 0;
	}
	
	return [self releaseFilePath: path];
}

- (NSInteger) releaseUrlString: (NSString *)urlString
{
	return [self releaseUrlString: urlString remakerName: ResourceManagerOriginalRemakerName remakerParameter: nil];
}

- (NSInteger) releaseUrlString: (NSString *)urlString remakerName: (NSString *)remakerName remakerParameter: (id)remakerParameter
{
	NSString		*path;
	
	if( self.standby == NO ) {
		return 0;
	}
	
	if( ([urlString length] <=0) || ([remakerName length] <= 0) ) {
		return 0;
	}
	
	if( (path = [self pathFromUrlString: urlString remakerName: remakerName remakerParameter: remakerParameter]) == nil ) {
		return 0;
	}
	
	return [self releaseFilePath: path];
}

- (void) pauseTransfering
{
	if( self.standby == NO ) {
		return;
	}
	
	[_lockForResourceDict lock];
	if( _paused == YES ) {
		[_lockForResourceDict unlock];
		return;
	}
	_paused = YES;
	[_lockForResourceDict unlock];
	
	[[Hydra defaultHydra] pauseAllQueriesForExecutorName: ResourceManagerRemoteJobExecutorName atWorkerName: [self employedWorkerNameForExecutorName: ResourceManagerRemoteJobExecutorName]];
}

- (void) resumeTransfering
{
	if( self.standby == NO ) {
		return;
	}
	
	[_lockForResourceDict lock];
	if( _paused == NO ) {
		[_lockForResourceDict unlock];
		return;
	}
	_paused = NO;
	[_lockForResourceDict unlock];
	
	[[Hydra defaultHydra] resumeAllQueriesForExecutorName: ResourceManagerRemoteJobExecutorName atWorkerName: [self employedWorkerNameForExecutorName: ResourceManagerRemoteJobExecutorName]];
}

- (void) cancelTransfering
{
	if( self.standby == NO ) {
		return;
	}
	
	[_lockForResourceDict lock];

	_paused = NO;
	
	[_lockForResourceDict unlock];
	
	[[Hydra defaultHydra] cancelAllQueriesForExecutorName: ResourceManagerRemoteJobExecutorName atWorkerName: [self employedWorkerNameForExecutorName: ResourceManagerRemoteJobExecutorName]];
}

- (NSData *) readDataFromPath: (NSString *)path cryptModuleName: (NSString *)cryptModuleName
{
	NSData		*readData;
	
	if( self.standby == NO ) {
		return nil;
	}
	
	if( [path length] <= 0 ) {
		return nil;
	}
	
	if( (readData = [[NSData alloc] initWithContentsOfFile: path]) == nil ) {
		return nil;
	}
	
	if( [cryptModuleName length] > 0 ) {
		return [self decryptData: readData forName: cryptModuleName];
	}
	
	return readData;
}

- (BOOL) writeData: (NSData *)data toPath: (NSString *)path cryptModuleName: (NSString *)cryptModuleName
{
	NSString		*folderPath;
	
	if( self.standby == NO ) {
		return NO;
	}
	
	if( ([data length] <= 0) || ([path length] <= 0) ) {
		return NO;
	}
	
	if( (folderPath = [path stringByDeletingLastPathComponent]) == nil ) {
		return NO;
	}
	
	[[NSFileManager defaultManager] createDirectoryAtPath: folderPath withIntermediateDirectories: YES attributes: nil error: nil];
	
	if( [cryptModuleName length] > 0 ) {
		return [self encryptData: data toFilePath: path forName: cryptModuleName];
	}
	
	return [data writeToFile: path atomically: YES];
}

- (UIImage *) imageWithColor: (UIColor *)color size: (CGSize)size
{
	CGRect			rect;
	CGContextRef	context;
	UIImage			*image;
	
	rect.origin = CGPointMake( 0.0f, 0.0f );
	rect.size = size;
	
	UIGraphicsBeginImageContext( rect.size );
	context = UIGraphicsGetCurrentContext();
	CGContextSetFillColorWithColor( context, color.CGColor );
	CGContextFillRect( context, rect );
	image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return image;
}

- (UIImage *) image: (UIImage *)image byRemakerName: (NSString *)remakerName remakerParameter: (id)remakerParameter
{
	NSData		*inputData;
	NSData		*outputData;
	CGFloat		scale;
	
	if( image == nil ) {
		return nil;
	}
	
	if( (inputData = UIImagePNGRepresentation(image)) == nil ) {
		return nil;
	}
	if( (outputData = [self remakeData: inputData withParameter: remakerParameter forName: remakerName]) == nil ) {
		return nil;
	}
	
	scale = [self isRetina] ? 2.0f : 1.0f;
	
	return [UIImage imageWithData: outputData scale:scale];
}

- (NSUInteger) limitSizeOfMemory
{
	if( self.standby == NO ) {
		return 0;
	}
	
	return _limitSizeOfMemory;
}

- (void) setLimitSizeOfMemory: (NSUInteger)limitSizeOfMemory
{
	if( self.standby == NO ) {
		return;
	}
	
	if( _limitSizeOfMemory == limitSizeOfMemory ) {
		return;
	}
	
	[_lockForResourceDict lock];
	
	_limitSizeOfMemory = limitSizeOfMemory;
	
	[self balancingWithLimitSizeOfMemory];
	
	[_lockForResourceDict unlock];
}

@end
